import React, { useEffect } from "react";
import { useRelatedOverview } from "../hooks/useRelatedOverview";
import Loading from "../components/LoadingUI";
import { useRouter } from "next/router";
import { useAppStateContext } from "../context/AppStateContext";
import { calculateDecimalToPercentage } from "../utulities/calculateDecimalToPercentage";
import { MilestoneOverViewType } from "../context/MilestonesContext";
import {
  SubProjectType,
  SubProjectTypeIdentical,
} from "../types/subProjectType";
import { CircledProgressBar } from "../components/CircleProgressContent";
import {
  HeaderTextContainer,
  ProgressBarContainer,
  ProjectOverviewContentBox,
  ProjectOverviewHeaderBox,
  ProjectOverviewWrapper,
} from "../components/ConstantUi";
import { ProjectOverviewContent } from "../components/ProjectOverviewContent";
import { MediumText, SmallText, YellowTextColor } from "../components/Text";
import Head from "next/head";
import { MilestoneType } from "../types/milestoneType";

interface LoadedOverViewComponentProps {
  milestones?: MilestoneType[];
  subProjects?: SubProjectType[];
  projectId: string;
  isTopLevel: boolean;
  projectGoal: string;
  projectCollectionPaths?: string[] | null;
  projectGoalAchievingProbability: number;
  handleProjectClick: (id: string) => void;
}

const LoadedOverViewComponent: React.FC<LoadedOverViewComponentProps> = ({
  milestones,
  subProjects,
  projectId,
  projectGoal,
  projectGoalAchievingProbability,
  handleProjectClick,
}) => {
  const [onMouse, setOnMouse] = React.useState<boolean>(false);
  return (
    <ProjectOverviewWrapper
      onMouseEnter={() => setOnMouse(true)}
      onMouseLeave={() => {
        setOnMouse(false);
      }}
      id={projectId}
      mouseOver={onMouse}
      onClick={() => handleProjectClick(projectId)}
    >
      <ProjectOverviewContentBox>
        <ProjectOverviewHeaderBox>
          <ProgressBarContainer>
            <CircledProgressBar
              isSmallSize
              isMediumSize
              progressValue={projectGoalAchievingProbability}
            />
          </ProgressBarContainer>
          <HeaderTextContainer>
            <SmallText color={YellowTextColor}>Goal</SmallText>
            <MediumText>
              {projectGoal ? projectGoal : "Project #" + projectId}
            </MediumText>
          </HeaderTextContainer>
        </ProjectOverviewHeaderBox>
        <ProjectOverviewContent
          milestones={milestones}
          subProjects={subProjects}
        />
      </ProjectOverviewContentBox>
    </ProjectOverviewWrapper>
  );
};

const Home: React.FC = () => {
  const history = useRouter();
  const { allRelatedOverviews } = useRelatedOverview();
  const { setActiveProjectData } = useAppStateContext();
  const handleProjectClick = (id: string) => {
    console.log("hey", history.pathname);
    const newPath = `projects/${id}`;
    history.push(newPath);
  };
  useEffect(() => {
    setActiveProjectData(null);
  }, [setActiveProjectData]);

  return allRelatedOverviews ? (
    <>
      {allRelatedOverviews.map(
        (
          {
            projectId,
            projectCollectionPaths,
            projectGoal,
            projectGoalAchievingProbability,
            milestones,
            subProjects,
          },
          index: number
        ) => {
          return (
            <>
              <Head>
                <title>Goal Rocket - Projects</title>
                <meta
                  property="og:title"
                  content="Goal Rocket - Projects"
                  key="title"
                />
              </Head>
              <LoadedOverViewComponent
                handleProjectClick={handleProjectClick}
                milestones={milestones}
                subProjects={subProjects}
                projectId={projectId}
                key={index}
                isTopLevel
                projectGoal={projectGoal}
                projectCollectionPaths={projectCollectionPaths}
                projectGoalAchievingProbability={calculateDecimalToPercentage(
                  projectGoalAchievingProbability
                )}
              />
            </>
          );
        }
      )}
    </>
  ) : (
    <Loading loading={true} />
  );
};

export default Home;
