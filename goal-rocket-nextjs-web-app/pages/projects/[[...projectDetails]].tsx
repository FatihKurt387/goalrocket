import React, { useEffect } from "react";
import Head from "next/head";

import Loading from "../../components/LoadingUI";
import TreeViewSvgIcon from "../../public/treeviewlogo.svg";
import { ProjectDetailScreenContentContainer } from "../../components/ProjectDetailContent";
import { LeftPanel } from "../../components/LeftPanel";
import { SwitchPanel } from "../../components/SwitchPanel";
import { TopPanel } from "../../components/TopPanel";
import { DetailBody } from "../../components/DetailBody";
import { MilestoneType } from "../../types/milestoneType";
import { useAppStateContext } from "../../context/AppStateContext";
import { firestore } from "../../firebase/firebase";
import { ProjectType } from "../../types/projectType";
import { usePathTrackingContext } from "../../context/PathTrackingContext";
import { decidePathLevel } from "../../utulities/decitePathLevel";
import { move } from "../../utulities/moveMilestone";
import { getIdFromPath } from "../../utulities/getIdFromPath";
import { useMilestoneContext } from "../../context/MilestonesContext";
import { useProjectContext } from "../../context/ProjectsContext";
import { calculateDecimalToPercentage } from "../../utulities/calculateDecimalToPercentage";
import { useRouter } from "next/router";
import { MilestoneTreeViewIcon } from "../../components/ConstantUi";
import { TreeViewsMainContent } from "../../components/TreeViewsMainContent";
import { SubProjectType } from "../../types/subProjectType";
import {
  matchTheOverview,
  TreeViewDataTypes,
  useRelatedOverview,
} from "../../hooks/useRelatedOverview";
import { useCallback } from "react";

const ProjectDetailPage: React.FC = () => {
  const {
    setActiveProjectData,
    activeProjectData,
    isMilestoneEditing,
    isMilestoneTab,
    setActiveProjectMilestonesData,
    activeProjectMilestonesData,
    setIsTreeView,
    activeProjectSubProjectsData,
    isTreeView,
    projectTreeViewData,
    setProjectTreeViewData,
  } = useAppStateContext();

  const { allRelatedOverviews } = useRelatedOverview();

  const history = useRouter();
  const { specificCurrentPath } = usePathTrackingContext();
  const { updateMilestoneDate } = useMilestoneContext();
  const { updateProjectIdentical } = useProjectContext();
  const activePath = specificCurrentPath.substring(1);

  useEffect(() => {
    const topLevelProjectId = activePath.split("/")[1];
    const id = getIdFromPath(activePath);
    allRelatedOverviews?.filter(
      (
        {
          isTopLevel,
          milestones,
          projectCollectionPaths,
          projectGoal,
          projectGoalAchievingProbability,
          projectId,
          subProjects,
        },
        index
      ) => {
        if (projectId === topLevelProjectId) {
          const value: TreeViewDataTypes[] = [
            {
              projectId,
              projectCollectionPaths,
              projectGoalAchievingProbability,
              projectGoal,
              isTopLevel,
              subProjects,
              milestones,
            },
          ];
          console.log("value", value);
          setProjectTreeViewData(value);
        }
      }
    );
  }, [
    activePath,
    activeProjectMilestonesData,
    allRelatedOverviews,
    setProjectTreeViewData,
  ]);

  useEffect(() => {
    if (!activePath) return;
    const id = getIdFromPath(activePath);
    const unsubscribe = firestore
      .collectionGroup("projects")
      .where("id", "==", id)
      .limit(1)
      .onSnapshot((doc) => {
        doc.docChanges().forEach(async (change) => {
          if (change.type === "added") {
            const data = (change.doc.data() as ProjectType) || null;
            console.log("path", activePath, data);
            setActiveProjectData(data);
          } else if (change.type === "modified") {
            const newdata = (change.doc.data() as ProjectType) || null;
            return setActiveProjectData((prevState) => {
              return { ...prevState, ...newdata };
            });
          }
        });
      });
    return () => unsubscribe();
  }, [
    activePath,
    activeProjectMilestonesData,
    setActiveProjectData,
    updateMilestoneDate,
  ]);
  useEffect(() => {
    if (!activePath) return;
    const id = getIdFromPath(activePath);
    const level = decidePathLevel(activePath);
    const unsubscribe = firestore
      .collectionGroup("milestones")
      .where("parent", "==", id)
      .where("level", "==", level)
      .orderBy("startDate", "asc")

      .onSnapshot((doc) => {
        doc.docChanges().forEach(async (change) => {
          if (change.type === "modified") {
            const newdata = change.doc.data() as MilestoneType;
            if (change.oldIndex !== change.newIndex) {
              // if the order changed after update first reorder and update it
              setActiveProjectMilestonesData((prevState) => {
                const reordered = move(
                  prevState!,
                  change.oldIndex,
                  change.newIndex
                ) as MilestoneType[];

                return reordered!.map((milestone) =>
                  milestone.id === newdata.id
                    ? { ...milestone, ...newdata }
                    : milestone
                );
              });
            } else {
              //if order not changed just update it
              return setActiveProjectMilestonesData((prevState) => {
                return prevState!.map((milestone) =>
                  milestone.id === newdata.id
                    ? { ...milestone, ...newdata }
                    : milestone
                );
              });
            }
          } else if (change.type === "added") {
            const newMilestoneData = [change.doc.data()] as MilestoneType[];
            setActiveProjectMilestonesData((prevState) => {
              if (!prevState) return [...newMilestoneData];
              const reordered = move(
                [...prevState, ...newMilestoneData],
                change.newIndex,
                change.newIndex
              );
              return reordered;
            });
          } else if (change.type === "removed") {
            const removedMilestone = change.doc.id as string;
            setActiveProjectMilestonesData((prevState) => {
              return prevState!
                .filter((milestone) => milestone.id !== removedMilestone)
                .map((milestone) => milestone);
            });
          }
        });
      });
    return () => unsubscribe();
  }, [activePath, setActiveProjectMilestonesData, updateProjectIdentical]);
  const LoadedTreeView = useCallback(() => {
    if (!projectTreeViewData || !activeProjectData) return null;
    console.log("projectreeviewdata", projectTreeViewData);
    return (
      <>
        <MilestoneTreeViewIcon
          onClick={() => {
            setIsTreeView(true);
          }}
          src={TreeViewSvgIcon}
          color={isTreeView ? "rgb(228, 220, 0)" : ""}
        />
        {projectTreeViewData.map(({ milestones, subProjects }, index) => {
          if (!milestones || !subProjects) return null;
          return (
            <TreeViewsMainContent
              key={index}
              isOpen={isTreeView}
              milestones={milestones}
              onClose={() => setIsTreeView(false)}
              projectGoal={activeProjectData.goal}
              projectProgressValue={Math.round(
                calculateDecimalToPercentage(
                  activeProjectData.goalAchievingProbability
                )
              )}
              subProjects={subProjects}
            />
          );
        })}
      </>
    );
  }, [activeProjectData, isTreeView, projectTreeViewData, setIsTreeView]);
  return !activeProjectData ? (
    <>
      <Head>
        <title>Project Details...</title>
      </Head>
      <meta
        name="description"
        content={`Goal Rocket 🚀  Here is the link of Project-Path-Of-${history.basePath}...`}
      />
      <Loading loading={true} />
    </>
  ) : activeProjectData ? (
    <>
      <Head>
        <title>
          GoalRocket/
          {activeProjectData.id}
        </title>
        <meta
          name="description"
          content={`Goal Rocket 🚀  Here is the link of Project-${history.basePath}...`}
        />
        <meta
          property="og:title"
          content={`GoalRocket-Project-${activeProjectData.id}`}
          key="title"
        />
      </Head>
      <ProjectDetailScreenContentContainer
        isMilestoneEditing={isMilestoneEditing}
      >
        <LoadedTreeView />
        <LeftPanel />
        <TopPanel
          endDate={activeProjectData.endDate}
          isTopLevel={activeProjectData.isTopLevel}
          milestones={activeProjectMilestonesData}
          progressOfProject={activeProjectData.progressOfProject}
          progressOfTime={activeProjectData.progressOfTime}
          startDate={activeProjectData.startDate}
          plannedEndDate={activeProjectData.plannedEndDate}
          goal={activeProjectData.goal}
          goalAchievingProbability={Math.round(
            calculateDecimalToPercentage(
              activeProjectData.goalAchievingProbability
            )
          )}
          activePath={activePath}
        />
        <SwitchPanel />
        <DetailBody
          activePath={activePath}
          isMilestoneEditing={isMilestoneEditing}
          isMilestoneTab={isMilestoneTab}
        />
      </ProjectDetailScreenContentContainer>
    </>
  ) : null;
};

export default ProjectDetailPage;
