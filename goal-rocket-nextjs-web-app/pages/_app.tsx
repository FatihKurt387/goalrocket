import type { AppProps } from "next/app";
import React from "react";
import { createGlobalStyle } from "styled-components";
import Layout from "../components/Layout";
import Navbar from "../components/Navbar";
import { AppState } from "../context/AppStateContext";
import { Milestones } from "../context/MilestonesContext";
import { PathTracking } from "../context/PathTrackingContext";
import { Projects } from "../context/ProjectsContext";
import { SubProjects } from "../context/SubProjectContext";

function MyApp({ Component, pageProps }: AppProps) {
  const GlobalStyle = createGlobalStyle`
* {
  font-family:  -apple-system,
    BlinkMacSystemFont, "Segoe UI", "Roboto", "Oxygen", "Ubuntu", "Cantarell",
    "Fira Sans", "Droid Sans", "Helvetica Neue" ;
}
body {
  margin: 0;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  background-color: #636380;
  overflow-y: hidden;
}

code {
  font-family: "aileron", sans-serif, source-code-pro, Menlo, Monaco, Consolas,
    "Courier New", monospace;
}

.MuiPickersToolbar-toolbar {
  background-color: rgba(50, 50, 77, 1) !important;
}

/* Chrome, Safari, Edge, Opera */
input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button {
  -webkit-appearance: none;
  margin: 0;
}

/* Firefox */
input[type="number"] {
  -moz-appearance: textfield;
}
::-webkit-scrollbar {
  width: 7px;
  margin-right: 8px;
  border-radius: 10px;
  margin-left: 8px;
}

/* Track */
::-webkit-scrollbar-track {
  border-radius: 10px;
  background-color: transparent;
}

/* Handle */
::-webkit-scrollbar-thumb {
  border-radius: 10px;
  background: rgba(240, 240, 255, 1);
}

`;
  return (
    <PathTracking>
      <Projects>
        <SubProjects>
          <Milestones>
            <AppState>
              <GlobalStyle />
              <Navbar />
              <Layout>
                <Component {...pageProps} />
              </Layout>
            </AppState>
          </Milestones>
        </SubProjects>
      </Projects>
    </PathTracking>
  );
}
export default MyApp;
