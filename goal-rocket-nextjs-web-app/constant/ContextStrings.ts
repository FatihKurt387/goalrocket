export const milestoneGoalPlaceholder = "Type your milestone goal here...";
export const projectGoalPlaceholder = "Type your project goal here...";
export const milestoneDescriptionPlaceHolder =
  "Type your milestone description here...";
export const projectDescriptionPlaceHolder =
  "Type your project description here...";
