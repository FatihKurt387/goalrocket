import { TextFieldProps } from "@material-ui/core";
import { DefaultWhiteTextColor } from "../components/Text";

export const EditGoalTextFieldProps: TextFieldProps = {
  fullWidth: true,
  type: "text",
  inputProps: {
    maxLength: 75,
  },
  InputLabelProps: {
    shrink: false,
    style: {
      color: "rgba(240, 240, 255, 1)",
      fontSize: "24px",
      fontStyle: "normal",
      fontWeight: "bold",
      lineHeight: "25px",
      letterSpacing: "0em",
      textAlign: "left",
    },
  },
  InputProps: {
    spellCheck: false,
    disableUnderline: true,
    style: {
      color: "rgba(240, 240, 255, 1)",
      fontSize: "24px",
      fontStyle: "normal",
      fontWeight: "bold",
      lineHeight: "29px",
      letterSpacing: "0em",
      textAlign: "left",
    },
  },
};

export const DescriptionTextFieldProps: TextFieldProps = {
  multiline: true,
  fullWidth: true,
  style: {
    fontFamily: "aileron",
    all: "inherit",
    flexBasis: "100%",
    cursor: "text",
  },
  InputProps: {
    spellCheck: false,
    disableUnderline: true,
    style: {
      all: "inherit",
      color: "rgba(240, 240, 255, 0.7)",
      fontSize: "15px",
    },
  },
  inputProps: {
    maxLength: 5000,
  },
  type: "text-area",
};

export const NumericFieldProps = (
  min?: number,
  max?: number
): TextFieldProps => {
  return {
    type: "number",
    InputLabelProps: {
      shrink: true,
    },
    InputProps: {
      disableUnderline: true,
      style: {
        color: DefaultWhiteTextColor,
        fontSize: "18px",
        fontStyle: "normal",
        fontWeight: 400,
        lineHeight: "14px",
        letterSpacing: "0em",
        textAlign: "center",
      },
    },
    inputProps: {
      style: { padding: 0, textAlign: "right" },
      min,
      max,
      inputMode: "numeric",
    },
    variant: "filled",
  };
};
export const DatePickerFormProps = (isReadOnly?: boolean) => {
  return {
    InputLabelProps: {
      style: {
        color: "rgba(228, 220, 0, 1)",
        fontSize: "18px",
        fontStyle: "normal",
        fontWeight: "bold",
        lineHeight: "25px",
        letterSpacing: "0em",
        textAlign: "left",
      },
    },
    InputProps: {
      style: {
        borderBottom: isReadOnly ? "" : "2px solid white",
        color: "rgba(240, 240, 255, 0.7)",
      },
    },
    format: "MM/dd/yyyy",
  };
};
