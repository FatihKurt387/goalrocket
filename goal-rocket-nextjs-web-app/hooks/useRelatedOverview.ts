import { useEffect, useState } from "react";

import { useMilestoneContext } from "../context/MilestonesContext";
import { useProjectContext } from "../context/ProjectsContext";
import { useSubProjectContext } from "../context/SubProjectContext";
import { MilestoneType } from "../types/milestoneType";
import { SubProjectType } from "../types/subProjectType";
import { ProjectType } from "../types/projectType";

export type TreeViewDataTypes = {
  projectId: string;
  projectCollectionPaths: string[] | null | undefined;
  projectGoalAchievingProbability: number;
  projectGoal: string;
  isTopLevel: boolean;
  subProjects: SubProjectType[] | undefined;
  milestones: MilestoneType[] | undefined;
};
export const matchTheOverview = (projects: {
  projects: ProjectType[] | null;
  milestones: MilestoneType[] | null;
  subProjects: SubProjectType[] | null;
}) => {
  const data = projects.projects?.map(
    ({ id, collectionPaths, goalAchievingProbability, goal }) => {
      const subProjects = projects.subProjects?.filter(
        (subproject) => subproject.topLevelProjectId === id
      );
      const milestones = projects.milestones?.filter(
        (milestone) => milestone.topLevelProjectId === id
      );
      const result: TreeViewDataTypes = {
        projectId: id,
        projectCollectionPaths: collectionPaths,
        projectGoalAchievingProbability: goalAchievingProbability,
        projectGoal: goal,
        isTopLevel: true,
        subProjects,
        milestones,
      };

      return result;
    }
  );
  return data;
};
export const useRelatedOverview = () => {
  const { allMilestonesOverViewData, isAllMilestonesOverviewLoading } =
    useMilestoneContext();
  const { allProjectsOverViewData, isAllProjectsOverviewLoading } =
    useProjectContext();
  const { allSubProjectsOverViewData, isAllSubProjectsOverviewLoading } =
    useSubProjectContext();

  const [allRelatedOverviews, setAllRelatedOverView] =
    useState<TreeViewDataTypes[]>();
  // console.log("yop", orderedByTopProjectId, orderedByLevel)
  useEffect(() => {
    const data = matchTheOverview({
      milestones: allMilestonesOverViewData,
      projects: allProjectsOverViewData,
      subProjects: allSubProjectsOverViewData,
    });

    setAllRelatedOverView(data);
  }, [
    allMilestonesOverViewData,
    allProjectsOverViewData,
    allSubProjectsOverViewData,
    isAllMilestonesOverviewLoading,
    isAllProjectsOverviewLoading,
    isAllSubProjectsOverviewLoading,
  ]);

  return { allRelatedOverviews };
};
