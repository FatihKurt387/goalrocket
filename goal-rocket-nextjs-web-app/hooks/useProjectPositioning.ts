import { Timestamp } from "../firebase/firebase";
import { MilestoneType } from "../types/milestoneType";
import { calculateDecimalToPercentage } from "../utulities/calculateDecimalToPercentage";
import { getCalculatedDateDifferance } from "../utulities/getDateDifferences";
export type PositionDataType = {
  progressOfProjectPosition: number;
  progressOfTimePositioning: number;
  isExpired: boolean;
  isTopLevel?: boolean;
  diffEndDateAndStartDate: number;
  diffPlannedAndEndDate: number;
  diffPlannedEndDateAndStartDate: number;
  grayLength: number;
  whiteLineLength: number;
  widthLength: number;
  yellowLineLength: number;
  milestonesPositions: { position: number; goalName: string }[];
};

export class ProjectPositioning {
  grayLength: number;
  widthLength: number;
  yellowLineLength: number;
  whiteLineLength: number;
  isExpired: boolean;
  isTopLevel: boolean;
  diffPlannedEndDateAndStartDate: number;
  diffPlannedAndEndDate: number;
  diffEndDateAndStartDate: number;
  progressOfTimePositioning: number;
  progressOfProjectPosition: number;
  milestonesPositions: { position: number; goalName: string }[];
  milestones?: MilestoneType[] | null;
  startDate?: Timestamp;
  constructor(
    progressOfProject: number,
    progressOfTime: number,
    isTopLevel: boolean,
    startDate: Timestamp,
    endDate: Timestamp,
    plannedEndDate?: Timestamp | null,
    milestones?: MilestoneType[] | null
  ) {
    this.grayLength = 0;
    this.widthLength = 0;
    this.yellowLineLength = 0;
    this.whiteLineLength = 0;
    this.progressOfProjectPosition =
      calculateDecimalToPercentage(progressOfProject);
    this.progressOfTimePositioning =
      calculateDecimalToPercentage(progressOfTime);
    this.isTopLevel = isTopLevel;
    this.startDate = startDate;
    this.diffPlannedEndDateAndStartDate = isTopLevel
      ? getCalculatedDateDifferance(
          new Date(plannedEndDate!.toDate().setHours(0, 0, 0, 0)),
          new Date(startDate!.toDate().setHours(0, 0, 0, 0))
        )
      : getCalculatedDateDifferance(
          new Date(endDate!.toDate().setHours(0, 0, 0, 0)),
          new Date(startDate!.toDate().setHours(0, 0, 0, 0))
        );
    this.diffPlannedAndEndDate = isTopLevel
      ? plannedEndDate && endDate
        ? getCalculatedDateDifferance(
            new Date(plannedEndDate.toDate().setHours(0, 0, 0, 0)),
            new Date(endDate.toDate().setHours(0, 0, 0, 0))
          )
        : 0
      : 0; //planned end date end date difference which come from backend
    this.diffEndDateAndStartDate =
      endDate && startDate
        ? getCalculatedDateDifferance(
            new Date(endDate.toDate().setHours(0, 0, 0, 0)),
            new Date(startDate.toDate().setHours(0, 0, 0, 0))
          )
        : 0; // end date and start date days difference
    this.isExpired =
      this.diffPlannedEndDateAndStartDate < this.diffEndDateAndStartDate
        ? true
        : false;
    this.milestonesPositions = [{ position: 0, goalName: "" }];
    this.milestones = milestones;
    this.calculateProjectPositioning();
    this.calculateMilestonesPositioning();
    // this.test();
  }

  // private test() {
  //   const testCases = {
  //     grayLength: this.grayLength,
  //     widthLength: this.widthLength,
  //     yellowLineLength: this.yellowLineLength,
  //     whiteLineLength: this.whiteLineLength,
  //     isExpired: this.isExpired,
  //     isTopLevel: this.isTopLevel,
  //     diffPlannedEndDateAndStartDate: this.diffPlannedEndDateAndStartDate,
  //     diffPlannedAndEndDate: this.diffPlannedAndEndDate,
  //     diffEndDateAndStartDate: this.diffEndDateAndStartDate,
  //     progressOfTimePositioning: this.progressOfTimePositioning,
  //     progressOfProjectPosition: this.progressOfProjectPosition,
  //   };
  // }

  private calculateMilestonesPositioning() {
    if (!this.milestones) return;
    this.milestonesPositions = this.milestones?.map((data) => {
      const milestoneStartDate = new Date(
        data.startDate.toDate().setHours(0, 0, 0, 0)
      );
      const projectStartDate = this.startDate
        ? new Date(this.startDate?.toDate().setHours(0, 0, 0, 0))
        : new Date();
      const differenceBetweenProjectStartDateAndMilestoneStartDate =
        getCalculatedDateDifferance(milestoneStartDate, projectStartDate);
      const currentMilestonePosition =
        (differenceBetweenProjectStartDateAndMilestoneStartDate /
          this.diffEndDateAndStartDate) *
        this.widthLength;
      return { position: currentMilestonePosition, goalName: data.goal };
    });
  }
  private calculateProjectPositioning() {
    if (this.isTopLevel) {
      if (this.isExpired) {
        this.diffPlannedEndDateAndStartDate =
          (this.diffPlannedEndDateAndStartDate / this.diffEndDateAndStartDate) *
          100;
        this.grayLength = this.diffPlannedEndDateAndStartDate; // old full width gray
        this.widthLength = 100; //* (old)full width end date position
        this.whiteLineLength = this.progressOfTimePositioning;
      } else {
        this.grayLength = 100;
        this.widthLength = calculateDecimalToPercentage(
          this.diffEndDateAndStartDate / this.diffPlannedEndDateAndStartDate
        );
        //* (old)full width end date position
        this.whiteLineLength =
          (this.widthLength / 100) * this.progressOfTimePositioning;
        this.progressOfProjectPosition =
          (this.widthLength / 100) * this.progressOfProjectPosition;
        this.diffPlannedEndDateAndStartDate = 99;
      }
    } else {
      this.grayLength = 100;
      this.widthLength = 100;
      this.whiteLineLength = this.progressOfTimePositioning;
    }
    this.yellowLineLength = this.widthLength - this.whiteLineLength; //* yellow line
  }
}
export const getLinearProgressBarPositions = (
  isTopLevel: boolean,
  startDate: Timestamp,
  endDate: Timestamp,
  progressOfProject: number,
  progressOfTime: number,
  plannedEndDate?: Timestamp | null,
  milestones?: MilestoneType[] | null
) => {
  const positions = new ProjectPositioning(
    progressOfProject,
    progressOfTime,
    isTopLevel,
    startDate,
    endDate,
    plannedEndDate,
    milestones
  );
  return { ...positions };
};
