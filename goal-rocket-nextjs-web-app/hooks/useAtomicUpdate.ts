import {
  UpdateMilestoneArgType,
  useMilestoneContext,
} from "../context/MilestonesContext";
import { firestore, Timestamp } from "../firebase/firebase";
import { MilestoneType } from "../types/milestoneType";
import { chainStartDateUpdate } from "../utulities/chainDateUpdate";

interface MiddleWareProps {
  topLevelProjectId: string;
  level: number;
  pathRef: string;
  parent: string;
  credentials: {
    newEndDate: Timestamp;
    newStartDate: Timestamp;
  };
  cb: ({
    pathRef,
    id,
    newEndDate,
    newStartDate,
    oldStartDate,
    oldEndDate,
    currentProgress,
    sizeOfMilestones,
  }: UpdateMilestoneArgType) => Promise<{
    startDate: Timestamp;
    endDate: Timestamp;
    totalRelativeProgress: number;
  }>;
}

export const applyAtomicUpdate = async ({
  parent,
  topLevelProjectId,
  level,
  credentials,
  pathRef,
  cb,
}: MiddleWareProps) => {
  let i = level - 1;
  let activeParent = parent;
  let text = "";

  try {
    console.log("docdata.id", pathRef, activeParent, i, topLevelProjectId);
    for (i; i >= 0; i--) {
      console.log("yes ı am inside for lood");
      const collectionGroup = firestore
        .collectionGroup("milestones")
        .where("level", "==", level)
        .orderBy("startDate", "asc");
      const activeProjectDataQueryFilterByTopLevelProjectId = firestore
        .collectionGroup("projects")
        .where("topLevelProjectId", "==", topLevelProjectId);
      const getCollectionGroup = await collectionGroup.get();
      console.log("size", getCollectionGroup.size);
      getCollectionGroup.docs.forEach(async (doc, index) => {
        const docData = doc.data() as MilestoneType;
        console.log("docdata.id", docData.id);
        if (docData.id === activeParent) {
          const isThatFirstMilestoneOfProject: boolean = index === 0 || false;
          // await chainStartDateUpdate({
          //   activeMilestonePath: doc.ref.path,
          //   id: docData.id,
          //   isThatFirstMilestoneOfProject,
          //   newEndDateForMilestone: credentials.newEndDate,
          //   newStartDateForMilestone: credentials.newStartDate,
          //   oldEndDate: docData.endDate,
          //   oldStartDate: docData.startDate,
          //   progress: docData.progress,
          //   sizeOfMilestones: getCollectionGroup.size,
          //   activeProjectData:

          // });
          await cb({
            currentProgress: docData.progress,
            id: docData.id,
            newEndDate: credentials.newEndDate,
            newStartDate: credentials.newStartDate,
            oldEndDate: docData.endDate,
            oldStartDate: docData.startDate,
            pathRef: doc.ref.path,
            sizeOfMilestones: getCollectionGroup.size,
          });
        }
      });
    }
  } catch (e) {
    console.error("breaking error ", e);
  }
};

// ->  start date 01.01.2020 end date 01.08.2020  level 0
// sub project ->  start date 01.01.2020 end date 01.08.2020  level 1
// sub project milestone -> start date 01.01.2020k
