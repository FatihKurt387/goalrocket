import React from "react";
import {
  ControlPanel,
  ControlPanelItemsContainer,
  MilestoneSearchTabIcon,
  MilestoneTabIcon,
} from "./ConstantUi";
import SearchButtonSvg from "../public/searchbutton.svg";
import MilestoneTabSvg from "../public/milestonetab.svg";
import { useAppStateContext } from "../context/AppStateContext";
interface SwitchPanelProps {}

export const SwitchPanel: React.FC<SwitchPanelProps> = () => {
  const {
    isMilestoneEditing,
    isMilestoneTab,
    setIsMilestoneTab,
    setIsMilestoneEditing,
  } = useAppStateContext();
  return (
    <ControlPanel id="Control-Panel">
      <ControlPanelItemsContainer>
        <MilestoneSearchTabIcon
          color={!isMilestoneTab ? "rgb(228, 220, 0)" : "rgb(240, 240, 255)"}
          onClick={() => {
            if (!isMilestoneTab) return;
            setIsMilestoneTab(false);
            if (!isMilestoneEditing) return;
            setIsMilestoneEditing(false);
          }}
          src={SearchButtonSvg}
        ></MilestoneSearchTabIcon>
        <MilestoneTabIcon
          color={isMilestoneTab ? "rgb(228, 220, 0)" : "rgb(240, 240, 255)"}
          onClick={() => {
            if (isMilestoneTab) return;
            setIsMilestoneTab(true);
            if (isMilestoneEditing) setIsMilestoneEditing(false);
          }}
          src={MilestoneTabSvg}
        ></MilestoneTabIcon>
      </ControlPanelItemsContainer>
    </ControlPanel>
  );
};
