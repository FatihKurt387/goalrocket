import { BodyContainer } from "./ConstantUi";
import React from "react";

// import Loading from "./ui/LoadingUI"
// import ErrorBox from "./ui/ErrorBox"
export interface BodyComponentProps {}

const Layout: React.FC<BodyComponentProps> = ({ children }) => {
  return (
    <BodyContainer id="body">
      <link
        href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap"
        rel="stylesheet"
      />
      {/* <Loading></Loading>
      {error && <ErrorBox error={error}></ErrorBox>} */}
      {children}
    </BodyContainer>
  );
};

export default Layout;
