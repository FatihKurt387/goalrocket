import { Dialog, makeStyles } from "@material-ui/core";

import FrameIcon from "../public/FrameIcon.svg";

import { FrameIconElement } from "./ConstantUi";
export interface LoadingProps {
  loading: boolean;
}

const Loading: React.FC<LoadingProps> = () => {
  const useStyles = makeStyles(() => ({
    paper: {
      background: "transparent",
      overflowX: "hidden",
      overflowY: "hidden",
      margin: "0",
      padding: "0",
      width: "100%",
      justifyContent: "center",
      alignItems: "center",
      height: "100%",
      boxShadow: "none",
    },
  }));
  const classes = useStyles();
  return (
    <Dialog
      fullWidth={true}
      maxWidth={false}
      classes={{ paper: classes.paper }}
      open={true}
    >
      <FrameIconElement Spin={true} src={FrameIcon}></FrameIconElement>
    </Dialog>
  );
};

export default Loading;
