import React from "react";
import ClockIcon from "../public/clock.svg";
import {
  ClockIconContainer,
  ClockIconImage,
  MilestoneGoalStaticsContainer,
  MilestoneLeftSide,
} from "./ConstantUi";
import { LargeText, SmallText, YellowTextColor } from "./Text";

interface DragWrapperLeftSideProps {
  goal: string | undefined;
  isMilestoneEditing: boolean;
}

export const DragWrapperLeftSide: React.FC<DragWrapperLeftSideProps> = ({
  goal,
  isMilestoneEditing,
}) => {
  return (
    <MilestoneLeftSide isMilestoneEditing={isMilestoneEditing}>
      <ClockIconContainer style={{ marginRight: "23px" }}>
        <ClockIconImage src={ClockIcon}></ClockIconImage>
      </ClockIconContainer>
      <MilestoneGoalStaticsContainer>
        <SmallText color={YellowTextColor}>Goal</SmallText>
        <LargeText>{goal}</LargeText>
      </MilestoneGoalStaticsContainer>
    </MilestoneLeftSide>
  );
};
