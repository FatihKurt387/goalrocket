import React, { createRef, useEffect, useState } from "react";
import { useRouter } from "next/router";
import FrameIcon from "../public/FrameIcon.svg";
import SettingsIcon from "../public/SettingIcon.svg";
import DeleteIcon from "../public/delete.svg";
import {
  NavbarContainer,
  NavbarItem,
  NavbarLinksContainer,
  NavbarLinksContainerItems,
  NavbarRoot,
  NavbarUserContainer,
  CreateNewProjectButtonContainer,
  DropDownCreateMenuContainer,
  DropDownHeaderContainer,
  DropDownMenuBody,
  FrameIconElement,
  SettingsIconElement,
  DropDownMenuHeaderTextContainer,
  DropDownHintTextContainer,
  MilestoneDeleteIcon,
} from "./ConstantUi";

import { CreateProjectForm } from "./form/CreateProjectForm";
import { useAppStateContext } from "../context/AppStateContext";
import { MediumText, SmallText, YellowTextColor } from "./Text";
import { IconButtonComponent } from "./IconButton";
import { useProjectContext } from "../context/ProjectsContext";
import { useSubProjectContext } from "../context/SubProjectContext";
const Navbar: React.FC = () => {
  const history = useRouter();

  const { deleteProject } = useProjectContext();
  const { deleteSubProject } = useSubProjectContext();
  const dropDownMenuRef = createRef<HTMLDivElement>();
  const { activeProjectData, setActiveProjectMilestonesData } =
    useAppStateContext();

  const [openDropDownMenu, setDropDownMenu] = useState<boolean>(false);
  const handleOpenDropDownMenu = async () => {
    if (activeProjectData) {
      history.push("/");
      setActiveProjectMilestonesData(null);
    } else {
      setDropDownMenu(!openDropDownMenu);
    }
  };
  useEffect(() => {
    if (!activeProjectData) return;
    setDropDownMenu(false);
  }, [activeProjectData]);
  const handleDeleteProject = async () => {
    if (activeProjectData) {
      if (activeProjectData.isTopLevel) {
        // is top level
        deleteProject(activeProjectData.id).finally(() => {
          history.back();
        });
      } else {
        // is sub project
        deleteSubProject(activeProjectData.id).finally(() => {
          history.back();
        });
      }
    }
  };
  return (
    <NavbarRoot style={{ justifyContent: "center" }}>
      <NavbarContainer>
        <NavbarLinksContainer>
          <NavbarLinksContainerItems>
            <NavbarItem>
              <FrameIconElement src={FrameIcon}></FrameIconElement>
            </NavbarItem>
          </NavbarLinksContainerItems>
        </NavbarLinksContainer>
        <NavbarUserContainer>
          <NavbarUserContainer>
            <CreateNewProjectButtonContainer
              onClick={handleOpenDropDownMenu}
              style={{ alignSelf: "center" }}
            >
              <MediumText>
                {activeProjectData ? "Projects" : "Create Project"}
              </MediumText>
            </CreateNewProjectButtonContainer>

            {activeProjectData && (
              <IconButtonComponent
                onClick={handleDeleteProject}
                // eslint-disable-next-line react/no-children-prop
                children={<MilestoneDeleteIcon src={DeleteIcon} />}
              />
            )}
            <DropDownCreateMenuContainer
              ref={dropDownMenuRef}
              opened={openDropDownMenu}
              style={{ justifyContent: "center" }}
            >
              <DropDownHeaderContainer style={{ marginBottom: "50px" }}>
                <DropDownMenuHeaderTextContainer>
                  <SmallText color={YellowTextColor}>
                    Create your project
                  </SmallText>
                </DropDownMenuHeaderTextContainer>
                <DropDownHintTextContainer>
                  <MediumText>
                    Select one of three possible project types
                  </MediumText>
                </DropDownHintTextContainer>
              </DropDownHeaderContainer>
              <DropDownMenuBody>
                <CreateProjectForm isSubProject={false} />
              </DropDownMenuBody>
            </DropDownCreateMenuContainer>
            <SettingsIconElement src={SettingsIcon}></SettingsIconElement>
          </NavbarUserContainer>
        </NavbarUserContainer>
      </NavbarContainer>
    </NavbarRoot>
  );
};

export default Navbar;
