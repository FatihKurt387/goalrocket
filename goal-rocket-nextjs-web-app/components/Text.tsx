import React from "react";
import { Text } from "./ConstantUi";
interface SmallTextProps {
  color?: string;
}

export const DefaultWhiteTextColor = " rgba(240, 240, 255, 1)";

export const YellowTextColor = " rgba(228, 220, 0, 1)";

export const SmallText: React.FC<SmallTextProps> = ({
  color = DefaultWhiteTextColor,
  children,
}) => {
  return (
    <Text
      fontSize="12px"
      fontStyle="normal"
      fontWeight={400}
      lineHeight="14px"
      letterSpacing="0em"
      textAlign="left"
      color={color}
    >
      {children}
    </Text>
  );
};
interface MediumTextProps {
  color?: string;
  isMargin?: boolean;
  children;
}

export const MediumText: React.FC<MediumTextProps> = ({
  color = DefaultWhiteTextColor,
  isMargin,
  children,
}) => {
  return (
    <Text
      margin={isMargin}
      fontSize="18px"
      fontStyle="normal"
      fontWeight={400}
      lineHeight="22px"
      letterSpacing="0em"
      textAlign="left"
      color={color}
    >
      {" "}
      {children}{" "}
    </Text>
  );
};
interface LargeTextProps {
  color?: string;
}

export const LargeText: React.FC<LargeTextProps> = ({
  color = DefaultWhiteTextColor,
  children,
}) => {
  return (
    <Text
      fontSize="24px"
      fontStyle="normal"
      fontWeight={400}
      lineHeight="29px"
      letterSpacing="0em"
      textAlign="left"
      color={color}
    >
      {children}{" "}
    </Text>
  );
};
