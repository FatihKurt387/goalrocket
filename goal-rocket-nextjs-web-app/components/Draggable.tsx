import React from "react";
import { DragDropContext, Droppable, DropResult } from "react-beautiful-dnd";
import { useAppStateContext } from "../context/AppStateContext";
import { MilestoneType } from "../types/milestoneType";
import { DragComponentBodyRoot } from "./ConstantUi";
import { DragWrapper } from "./DragWrapper";

interface DragsComponentProps {
  orderObjects: MilestoneType[];
  dragsId: string;
  handleOnDragEnd(result: DropResult): Promise<void>;
  handleDragClick(y: any): void;
}
export const DragsComponent: React.FC<DragsComponentProps> = ({
  orderObjects,
  dragsId,
  handleDragClick,
  handleOnDragEnd,
}) => {
  const { isMilestoneEditing } = useAppStateContext();

  return (
    <DragDropContext onDragEnd={handleOnDragEnd}>
      <Droppable droppableId={dragsId}>
        {(provided) => (
          <DragComponentBodyRoot
            className={dragsId}
            {...provided.droppableProps}
            ref={provided.innerRef}
          >
            {orderObjects.map((obj, index) => {
              return (
                <DragWrapper
                  key={`wrapper${obj.id}`}
                  index={index}
                  milestone={obj}
                  isMilestoneEditing={isMilestoneEditing}
                  onClick={handleDragClick}
                />
              );
            })}
            {provided.placeholder}
          </DragComponentBodyRoot>
        )}
      </Droppable>
    </DragDropContext>
  );
};
