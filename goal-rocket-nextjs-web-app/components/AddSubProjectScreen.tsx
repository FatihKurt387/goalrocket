import { Dialog, makeStyles } from "@material-ui/core";
import AddIcon from "../public/addicon.svg";
import { CreateProjectForm } from "../components/form/CreateProjectForm";
import {
  SubProjectRoot,
  ProjectGoalText,
  SubProjectHeader,
  SubProjectBody,
  MilestoneAddButtonIcon,
  MilestoneAddButtonContainer,
} from "../components/ConstantUi";
import React from "react";
import { MediumText, YellowTextColor } from "../components/Text";

export interface AddSubProjectScreenProps {
  opened: boolean;
  milestoneid: string;
  projectid: number;
  closeCreateScreen: () => void;
}

const AddSubProjectScreen: React.FC<AddSubProjectScreenProps> = (props) => {
  const useStyles = makeStyles(() => ({
    paper: {
      background: "rgba(50, 50, 77, 0.95)",
      overflow: "hidden",
      margin: "0",
      padding: "0",
    },
  }));

  const classes = useStyles();
  // const [opened, setOpend] = React.useState<boolean>(props.opened)

  return (
    <Dialog classes={{ paper: classes.paper }} fullScreen open={props.opened}>
      <SubProjectRoot>
        <MilestoneAddButtonContainer
          onClick={() => {
            // setOpend(false)
          }}
          style={{ cursor: "pointer", position: "absolute", right: "2%" }}
        >
          <MilestoneAddButtonIcon
            onClick={() => {
              props.closeCreateScreen();
            }}
            style={{
              transform: "rotate(135deg)",
            }}
            src={AddIcon}
          ></MilestoneAddButtonIcon>
        </MilestoneAddButtonContainer>

        <SubProjectHeader>
          <MediumText color={YellowTextColor}>Create New Project</MediumText>
          <ProjectGoalText>
            Select one of three possible project types
          </ProjectGoalText>
        </SubProjectHeader>
        <SubProjectBody>
          <CreateProjectForm isFullWith isSubProject />
        </SubProjectBody>
      </SubProjectRoot>
    </Dialog>
  );
};

export default AddSubProjectScreen;
