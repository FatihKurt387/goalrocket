import { SVGProps } from "react";
import styled, { keyframes, css } from "styled-components";
import SVG from "react-inlinesvg";
//ICONS

export const MilestoneDeleteIcon = styled(SVG)<SVGProps<any>>`
  width: 25px;
  height: 22px;
  & path {
    fill: ${({ color }) => color};
  }
  cursor: pointer;
`;
export const UserLogoImage = styled.img``;

export const SearchButtonIcon = styled.img`
  width: 42px;
  cursor: pointer;
  height: 32px;
`;
export const LeaftAsideContent2Image = styled.img``;

export const MilestonesTabIcon = styled.img`
  width: 33px;
  height: 33px;
  cursor: pointer;
  color: green;
  ::&svg {
    fill: green;
  }
`;

export const TopHeaderLeftBıgProgressIcon = styled.img<{
  small?: boolean;
  medium?: boolean;
}>`
  ${(p) =>
    p.small
      ? p.medium
        ? "height:70px;width:70px;"
        : "height:48px;width:48px;"
      : "height:auto;width:auto;"}
`;

export const MilestoneCloseEditTabIcon = styled.img`
  position: absolute;
  width: 100%;
  height: 100%;
  max-height: 40px;
  max-width: 26px;
  left: 0;
  top: 60px;
  cursor: pointer;
`;

export const MilestoneAddButtonIcon = styled.img<{ small?: boolean }>`
  height: ${(p) => (p.small ? "24px" : "31px")};
  cursor: pointer;
  width: ${(p) => (p.small ? "24px" : "31px")};
`;

export const MilestoneTabIcon = styled(SVG)<SVGProps<any>>`
  width: 33px;
  height: 33px;
  cursor: pointer;
  & path {
    fill: ${({ color }) => color};
  }
`;
export const ClockIconImage = styled.img`
  height: 42px;
  width: 42px;
`;

export const MilestoneSearchTabIcon = styled(SVG)<SVGProps<any>>`
  width: 42px;
  height: 32px;
  cursor: pointer;
  & path {
    fill: ${({ color }) => color};
  }
`;
export const MilestoneTreeViewIcon = styled(SVG)<SVGProps<any>>`
  cursor: pointer;
  max-width: 121px;
  position: absolute;
  right: 0;
  top: 65%;
  max-height: 186px;
  & path {
    fill: ${({ color }) => color};
  }
`;
export const ProgressBarImage = styled.img`
  width: 70px;
  height: 70px;
`;

const rotate = keyframes`
from {
  transform: rotate(0deg);
}

to {
  transform: rotate(360deg);
}
`;
export const FrameIconElement = styled.img<{ Spin?: boolean }>`
  width: 45px;
  height: 45px;
  animation: ${(props) =>
    props.Spin
      ? css`
          ${rotate} 2s linear infinite
        `
      : ""};
`;
export const SettingsIconElement = styled.img`
  width: 45px;
  height: 45px;
`;
export const SaveIcon = styled.img`
  cursor: pointer;
`;
export const DiscardIcon = styled.img`
  cursor: pointer;
`;

// Body
export const BodyContainer = styled.div`
  display: flex;
  position: relative;
  height: 90vh;
  width: 100%;
  flex: 1;
  flex-wrap: wrap;
  overflow-x: hidden;
  overflow-y: auto;
`;
export const UnderLine = styled.span`
  display: flex;
  box-sizing: border-box;
  fill: rgba(240, 240, 255, 1);
  max-height: 1px;
  min-height: 1px;
  border-radius: 5px;
  background-color: rgba(240, 240, 255, 1);
`;
//Navbar

export const NavbarRoot = styled.div`
  display: flex;
  flex: 1;
  width: 100%;
  background: rgb(50, 50, 77, 1);
  box-sizing: border-box;
  min-width: 100%;
  min-height: 60px;
  border-bottom: 5px solid #636380;
  padding: 20px 25px 20px 25px;
`;

export const NavbarContainer = styled.div`
  display: flex;
  flex: 4;
`;
export const NavbarLinksContainer = styled.div`
  display: flex;
  justify-content: flex-start;

  flex: 4;
  align-items: center;
  text-align: center;
  flex-direction: row;
`;
export const NavbarLinksContainerItems = styled.div`
  width: 100%;
  justify-content: flex-start;
  flex-direction: row;
`;
export const NavbarUserContainer = styled.div`
  display: flex;
  flex: 1;
  justify-content: space-evenly;
  align-items: center;
  position: relative;
`;

export const NavbarItem = styled.div`
  display: flex;
  position: relative;
`;
export const CreateNewProjectButtonContainer = styled.div`
  display: flex;
  max-width: 163px;
  width: 100%;
  cursor: pointer;
  max-height: 43px;
  height: 100%;
  border-radius: 5px;
  border: 3px solid #484862;
  text-align: center;
  justify-content: center;
  padding: 6px 2px 5px 2px;
`;
export const CreateProjectRadioInputSubmitButton = styled.div`
  display: flex;
  justify-content: center;
  background: rgba(99, 99, 128, 0.1);
  align-self: flex-end;
  width: 163px;
  height: 43px;
  border-radius: 5px;
  font-weight: 400;
  border: 3px solid #484862;
`;
export const DropDownCreateMenuContainer = styled.div<{ opened: boolean }>`
  min-width: 364px;
  display: flex;
  opacity: ${(p) => (p.opened ? "1" : "0")};
  transition: opacity 250ms cubic-bezier(0.4, 0, 0.2, 1),
    z-index 250ms cubic-bezier(0.4, 0, 0.2, 1);
  position: absolute;
  min-height: 389px;
  top: 47px;
  z-index: ${(p) => (p.opened ? "9999" : "-5555")};
  right: 145px;
  background-color: rgba(50, 50, 77, 0.95);
  width: 100%;
  height: 100%;
  border: 3px solid rgba(240, 240, 255, 0.1);
  border-radius: 5px;
  padding-left: 25px;
  padding-top: 34px;
  padding-bottom: 20px;
  padding-right: 21px;
  box-sizing: border-box;
  flex-direction: column;
`;
export const DropDownHeaderContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  height: 20%;
`;
export const DropDownMenuHeaderTextContainer = styled.div`
  display: flex;
  min-width: 99px;
  max-height: 14px;
  min-height: 14px;
  width: 100%;
  height: 100%;
`;
export const DropDownHintTextContainer = styled.div`
  width: 100%;
  display: flex;
  height: 100%;
  min-width: 318px;
  min-height: 22px;
  max-width: 318px;
  max-height: 22px;
`;
export const DropDownMenuBody = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
`;
// Project Details
export const ProjectDetailsContainer = styled.div<{ isEditing: boolean }>`
  display: inline-grid;
  width: 100%;
  grid-template-columns: ${(p) =>
    p.isEditing ? "0.3fr 0.2fr 2.5fr 2.5fr" : "0.3fr 0.2fr 2.5fr 1fr"};
  grid-template-rows: 0.8fr 1.2fr 1fr;
  gap: 4px 5px;
  grid-template-areas: ${(p) =>
    p.isEditing
      ? `${'"LeftAside TopHeader TopHeader TopHeader"\n"LeftAside controlpanel Detail-Content  Detail-Content-2"\n"LeftAside controlpanel Detail-Content Detail-Content-2"'}`
      : `${'"LeftAside TopHeader TopHeader TopHeader"\n"LeftAside controlpanel Detail-Content Detail-Content"\n"LeftAside controlpanel Detail-Content Detail-Content"'}`};
`;
export const Detail2 = styled.div`
  grid-area: Detail-Content-2;
  border-radius: 5px;
  position: relative;
  min-height: 728px;
  padding: 40px 69px 25px 44px;
  background: rgba(50, 50, 77, 1);
`;
export const Detail2ContentContainer = styled.div`
  display: flex;
  height: 100%;
  width: 100%;
  flex-direction: column;
`;
export const LeftAside = styled.aside`
  grid-area: LeftAside;
  border-radius: 5px;
  display: flex;
  justify-content: center;
  align-items: flex-start;
  object-fit: cover;
  background: rgba(50, 50, 77, 1);
`;
export const LeftAsideContentBox = styled.div`
  display: flex;
  width: 100%;
  height: 40%;
  flex-direction: column;
  justify-content: space-around;
`;
export const LeftAsideUserContent = styled.div`
  display: flex;
  flex-direction: column;
  height: 35%;
`;
export const LeftAsideContent2 = styled.div`
  display: flex;
  width: 100%;
  justify-content: space-evenly;
  align-items: center;
  flex-direction: row;
`;
export const LeftAsideContent2TextBox = styled.div`
  display: flex;
  position: relative;
`;

export const UserNameContainer = styled.div`
  display: flex;
  max-width: 116px;
  align-self: center;
  color: rgba(240, 240, 255, 1);
  width: 100%;
  justify-content: center;
  margin-top: 12px;
`;
export const TopHeader = styled.div`
  grid-area: TopHeader;
  border-radius: 5px;
  justify-content: space-between;
  display: flex;
  flex-direction: row;
  padding: 27px 90px 44px 51px;
  background: rgba(50, 50, 77, 1);
`;
export const TopHeaderLeft = styled.div`
  display: flex;
`;
export const TopHeaderRight = styled.div`
  width: 80%;
  display: flex;
  justify-content: space-between;
  height: 100%;
  flex-direction: column;
  max-height: 188px;
`;
export const TopHeaderTitleBox = styled.div`
  display: flex;
  height: 20%;
  flex-direction: column;
  max-width: 449px;
`;
export const ProjectGoalText = styled.div`
  color: rgba(240, 240, 255, 0.7);
`;
export const ProjectNameInputContainer = styled.div`
  display: flex;
  flex-direction: column;
  color: rgba(240, 240, 255, 0.7);
  justify-content: space-between;
`;
export const EditPencilIcon = styled.img`
  height: 50%;
  z-index: 9999;
  cursor: pointer;
`;
export const TopHeaderStatusBarContainer = styled.div<{ small?: boolean }>`
  display: flex;
  height: 35%;
  flex-direction: column;
  width: 100%;
`;
export const TopHeaderStatusBar = styled.img<{ small?: boolean }>`
  ${(p) =>
    p.small ? "height:2.76px;width:106.94px;" : "height:auto;width:auto;"}
`;
export const ControlPanel = styled.div`
  grid-area: controlpanel;
  border-radius: 5px;
  padding: 48px 18px;
  background: rgba(50, 50, 77, 1);
`;
export const ControlPanelItemsContainer = styled.div`
  display: flex;
  flex-direction: column;
  min-height: 96px;
  justify-content: space-between;
`;

export const DetailContent = styled.div`
  grid-area: Detail-Content;
  border-radius: 5px;
  padding: 45px 97px 55px 35px;
  background: rgba(50, 50, 77, 1);
`;
export const DetailContentContainer = styled.div`
  display: flex;
  flex: 1;
  height: 100%;
  position: relative;
  justify-content: space-between;
  flex-direction: column;
`;

export const DescriptionBoxContainer = styled.div`
  display: flex;
  border: 3px solid rgba(240, 240, 255, 0.1);
  height: 100%;
  border-radius: 5px;
`;
export const SaveDiscardContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-end;
  width: 100%;
`;
export const DetailMilestonesRoot = styled.div`
  display: flex;
  height: 100%;
  width: 100%;
  max-height: 728px;
  flex-direction: row;
  justify-content: space-between;
`;
export const DetailMilestoneContainer = styled.div<{ width: number }>`
  display: flex;
  width: ${(p) => p.width}%;
  height: 100%;
  flex-direction: column;
`;

export const MilestonesHeader = styled.div`
  display: flex;
  justify-content: space-between;
  flex-direction: column;
`;
export const MilestoneAddButtonContainer = styled.div`
  display: flex;
  width: 100%;
  padding: 20px 0;
  justify-content: flex-end;
`;
export const DragComponentBodyRoot = styled.div`
  display: flex;
  width: 100%;
  height: 80%;
  flex-direction: column;
  overflow-y: auto;
  overflow-x: hidden;
  &::-webkit-scrollbar {
    width: 7px;
    margin-right: 8px;
    margin-left: 8px;
  }

  /* Track */
  &::-webkit-scrollbar-track {
    box-shadow: inset 0 0 5px grey;
    border-radius: 10px;
    background-color: transparent;
  }

  /* Handle */
  &::-webkit-scrollbar-thumb {
    background: rgba(240, 240, 255, 1);

    width: 100%;
    height: 100%;
    max-width: 7px;
    max-height: 11px;
    border-radius: 5px;
  }
`;
export const MilestonesWrapper = styled.div<{
  mouseOver?: boolean;
  isMilestoneEditing?: boolean;
}>`
  display: flex;
  padding: 12px 25px 12px 25px;
  height: 100%;
  max-height: 50px;
  min-height: 50px;
  border-radius: 5px;
  border: ${(p) =>
    p.mouseOver
      ? "3px solid rgba(228, 220, 0, 1)"
      : "3px solid rgba(240, 240, 255, 0.1)"};

  max-width: ${(p) => (p.isMilestoneEditing ? "100%" : "100%")};
  flex-basis: 100%;
  justify-content: space-between;
  background-color: #373752;
  flex-direction: row;
  flex-grow: 0;
  position: relative;
  margin-bottom: 11px;
  align-items: center;
`;
export const MilestoneLeftSide = styled.div<{ isMilestoneEditing: boolean }>`
  display: flex;
  align-items: center;
  flex-grow: 0;
  width: 100%;
  max-width: ${(p) => (p.isMilestoneEditing ? "200px" : "304px")};
  min-width: ${(p) => (p.isMilestoneEditing ? "200px" : "304px")};
  flex-direction: row;
`;
export const ClockIconContainer = styled.div`
  display: flex;
`;

export const MilestoneGoalStaticsContainer = styled.div`
  display: flex;
  flex-direction: column;
`;
export const MilestonesDateContentRoot = styled.div<{
  isEditTapOpened: boolean;
}>`
  display: flex;
  flex-grow: 0;
  width: 100%;
  max-width: 342px;
  min-height: 50px;
  justify-content: space-between;
  flex-direction: row;
  border-right: ${(p) =>
    p.isEditTapOpened ? "" : "4px solid rgba(240, 240, 255, 0.5)"};
  border-radius: 1px;
`;
export const MilestoneDateContentContanier = styled.div`
  display: flex;
  justify-content: space-evenly;
  text-align: center;
  flex-direction: column;
`;
export const MilestoneStatusBarsContentContainer = styled.div`
  display: flex;
  width: 100%;
  justify-content: center;
  flex-direction: row;
`;
export const MilestoneStatusBarContentItems = styled.div`
  display: flex;
  justify-content: center;
  flex-basis: 10%;
  max-width: 10%;
`;
export const MilestoneEndThreeDotContainer = styled.div`
  display: flex;
  flex-grow: 0;
  flex-basis: 3%;
  border-left: 4px solid rgba(240, 240, 255, 0.5);
  min-height: 50px;
  border-radius: 1px;
  cursor: pointer;
  max-width: 3%;
  align-items: center;
  justify-content: flex-end;
`;
export const MilestoneEndThreeDotImage = styled.img`
  height: 30px;
  width: 8px;
`;

//

// Overview
export const ProjectOverviewWrapper = styled.div<{ mouseOver?: boolean }>`
  display: flex;
  flex-direction: row;
  flex: 1;
  height: 100%;
  max-height: 80%;
  margin-bottom: 5px;
  cursor: pointer;
  margin-right: 5px;
  overflow-y: hidden;
  max-width: 48%;
  min-width: 48%;
  border: ${(p) => (p.mouseOver ? "3px solid rgba(228, 220, 0, 1)" : "0px")};
  border-radius: 5px;
  background: rgba(50, 50, 77, 1);
  &::-webkit-scrollbar {
    width: 7px;
    margin-right: 8px;
    margin-left: 8px;
  }

  /* Track */
  &::-webkit-scrollbar-track {
    border-radius: 10px;
    background-color: transparent;
  }

  /* Handle */
  &::-webkit-scrollbar-thumb {
    background: rgba(240, 240, 255, 1);

    width: 100%;
    height: 100%;
    max-width: 7px;
    max-height: 11px;
    border-radius: 5px;
  }
`;
export const ProjectOverviewContentBox = styled.div`
  display: flex;
  flex: 1;
  width: 100%;
  height: 100%;
  padding: 27px 41px 69px 48px;
  flex-direction: column;
  justify-content: space-between;
`;
export const ProjectOverviewHeaderBox = styled.div`
  display: flex;
  flex-direction: row;
  width: 100%;
  max-width: 440px;
  justify-content: space-between;
  flex: 1;
`;
export const ProjectContentBox = styled.div`
  display: flex;
  flex-direction: column;
  flex: 4;
`;
export const ProgressBarContainer = styled.div`
  display: flex;
`;
export const SubProjectContainer = styled.div<{ level: number }>`
  display: flex;
  flex-direction: row;
  min-width: 200px;
  max-height: 52px;
  max-width: 858px;
  justify-content: flex-start;
  align-items: center;
  margin-top: 10px;
`;

export const SubSubProjectBox = styled.div<{ level: string }>`
  display: flex;
  background: rgba(99, 99, 128, 0.1);
  border: 3px solid rgba(240, 240, 255, 0.1);
  box-sizing: border-box;
  border-radius: 5px;
  padding: 5px;
  justify-content: space-evenly;
  width: 100%;
`;
export const SubProjectGroup = styled.div`
  display: flex;
  justify-content: space-between;
  min-width: 30px;
`;
export const SubSubProjectItem = styled.img`
  width: 28px;
  height: 28px;
`;
export const HeaderTextContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
`;

// SubProject

export const SubProjectRoot = styled.div`
  display: flex;
  height: 100%;
  width: 100%;
  flex-direction: column;
  justify-content: space-between;
  padding: 34px 21px 21px 25px;
  background-color: #3b3b55;
  overflow: hidden;
`;
export const SubProjectHeader = styled.div`
  display: flex;
  height: fit-content;

  width: 100%;
  flex-direction: column;
  justify-content: space-between;
`;
export const SubProjectBody = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
  max-width: 95%;
`;
export const SubProjectAction = styled.div`
  display: flex;
  width: 100%;
  justify-content: flex-end;
  flex-direction: row;
`;
// Linear progress bar
export const MainContainerBody = styled.div<{
  isSmall?: boolean;
  isMedium?: boolean;
}>`
  display: flex;
  flex-basis: 100%;
  min-width: ${(p) => (p.isSmall ? "20%" : "100%")};
  max-height: ${(p) => (p.isSmall ? "12px" : p.isMedium ? "13.18px" : "100%")};
  flex-basis: ${(p) => (p.isSmall ? "20%" : "100%")};
  min-height: ${(p) => (p.isSmall ? "12px" : p.isMedium ? "13.18px" : "23px")};
  flex-direction: row;
  flex-grow: 0;
  border-radius: 5px;
`;
export const MainContainerGrayArea = styled.div<{ width: number }>`
  background: ${(p) =>
    `linear-gradient(to right, #636380 0%, #636380 ${p.width}%, transparent ${p.width}%, transparent 100%)`};
  display: flex;
  border-radius: 5px;
  width: 100%;
  padding: 4.19px 7px 4.27px 7px;
`;
export const MainContainerTop = styled.div`
  display: flex;
  align-items: flex-end;
  flex-direction: row;
  width: 100%;
  position: relative;
  flex-basis: 100%;
  min-width: 100%;
  flex-grow: 0;
  border-radius: 5px;
  padding: 4.19px 0 4.27px 0;
`;
export const MainContainerBottom = styled.div`
  display: flex;
  align-items: flex-start;
  flex-direction: row;
  position: relative;
  flex-basis: 100%;
  min-width: 100%;
  flex-grow: 0;
  max-height: 23px;
  min-height: 23px;
  border-radius: 5px;
  padding: 4.19px 7px 4.27px 7px;
`;
export const MilestonesIcon = styled.img<{ position: number }>`
  position: absolute;
  cursor: pointer;
  left: ${(p) => p.position + "%"};
`;
export const FirstLine = styled.div<{ width: any }>`
  display: flex;
  flex-grow: 0;
  cursor: pointer;

  background: rgba(240, 240, 255, 0.7);
  border-radius: 5px;
  flex-basis: ${(p) => p.width + "%"};
  min-width: ${(p) => p.width + "%"};
`;
export const SecondLine = styled.div<{ width: any }>`
  display: flex;
  cursor: pointer;
  flex-grow: 0;
  background: rgba(228, 220, 0, 1);
  border-radius: 5px;
  flex-basis: ${(p) => p.width + "%"};
  min-width: ${(p) => p.width + "%"};
`;
export const SmallPointerIcon = styled.img<{ position?: number }>`
  position: absolute;
  cursor: pointer;
  z-index: 999;
  left: ${(p) => p.position + "%"};
`;
export const BigPointerIcon = styled.img<{
  position?: number;
  isSmall?: boolean;
  isMedium?: boolean;
}>`
  position: absolute;
  cursor: pointer;
  z-index: 0px;
  left: ${(p) => p.position + "%"};
  ${(p) => p.isSmall && "width:11px;height:11px;"}
  ${(p) => p.isMedium && "width:31px;height:31px;"}
`;
export const PlannedAndDatePointer = styled.img<{ position?: number }>`
  position: absolute;
  cursor: pointer;
  left: ${(p) => p.position + "%"};
`;
// Date Picker

export const DatePickerContainer = styled.div<{ width: number }>`
  display: flex;
  width: ${(p) => p.width}%;
  justify-content: space-between;
`;
export const DetailContentHeader = styled.div`
  display: flex;
  flex-direction: row;
  width: 100%;
  flex-wrap: wrap;
`;

export const ProjectTypeContainer = styled.div`
  display: flex;
  justify-content: flex-end;
  flex-direction: column;
  width: 65%;
`;
export const CalendarIconComp = styled.img``;
// Milestone Edit Tab

export const MilestoneEditContainer = styled.div`
  display: flex;
  flex-basis: 50%;
  flex-grow: 0;
  margin-left: 2px;
  border-radius: 10px;
  transition: opacity 5s linear;
  z-index: 999;
  min-height: 100%;
  flex-direction: column;
  justify-content: space-between;
`;
export const MilestoneEditHeader = styled.div`
  display: flex;
  width: 50%;
  flex-direction: column;
`;
export const MilestoneEditBody = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-evenly;
  min-height: 334px;
`;
export const MilestoneEditBottom = styled.div`
  display: flex;
  flex-direction: column;
  min-height: 334px;
  justify-content: space-between;
`;
export const MilestoneEditStatusContainer = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
`;
export const MilestoneEditCircleProgressContainer = styled.div`
  display: flex;
`;
export const MilestoneEditLinearProgressContanier = styled.div`
  display: flex;
`;
export const MilestoneEditDatasContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
`;
export const MilestoneEditDatasTop = styled.div`
  display: flex;
  width: 50%;
  flex-direction: row;
  justify-content: space-between;
`;
export const MilestoneEditDatasBottom = styled.div`
  display: flex;
  width: 80%;
  flex-direction: row;
  justify-content: space-between;
`;
export const MilestoneEditDataTopItem = styled.div`
  display: flex;
  flex-direction: column;
`;
// Texts

export const Text = styled.text<{
  fontSize: string;
  margin?: boolean;
  fontStyle: string;
  fontWeight: number;
  lineHeight: string;
  textAlign: string;
  color: string;
}>`
  font-family: Aileron;
  font-size: ${(p) => p.fontSize};
  font-style: ${(p) => p.fontStyle};
  font-weight: ${(p) => p.fontWeight};
  line-height: ${(p) => p.lineHeight};
  letter-spacing: ${(p) => p.letterSpacing};
  text-align: ${(p) => p.textAlign};
  color: ${(p) => p.color};
  margin-left: ${(p) => p.margin && "5px"};
`;
