import React from "react";
import styled from "styled-components";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormControl from "@material-ui/core/FormControl";
import { FirstLogo, SecondLogo } from "./RadioButtons";
import { FieldProps, getIn } from "formik";
import { FormHelperText } from "@material-ui/core";
// Inspired by blueprintjs
export const LabelText = styled.span`
  width: 98px;
  color: rgba(240, 240, 255, 1);
  height: 22px;
  font-family: Aileron;
  font-size: 18px;
  font-style: normal;
  font-weight: 400;
  line-height: 22px;
  letter-spacing: 0em;
  text-align: left;
`;

export const CheckBoxFormField: React.FC<
  FieldProps & {
    label?: string;
    options: Array<{ label: string; value: string }>;
  }
> = ({ field, form, label, options, children, ...props }) => {
  const errorText =
    getIn(form.touched, field.name) && getIn(form.errors, field.name);
  return (
    <FormControl
      component="fieldset"
      style={{ height: "100%", maxHeight: "fit-content" }}
      required
    >
      <RadioGroup
        {...field}
        {...props}
        style={{ maxHeight: "100%", flexWrap: "nowrap" }}
      >
        {options?.map((option, index) => {
          return (
            <>
              <FormControlLabel
                key={index}
                value={option.value}
                control={
                  <Radio
                    defaultChecked
                    icon={<FirstLogo />}
                    checkedIcon={<SecondLogo />}
                    color="default"
                    inputProps={{
                      style: { visibility: "hidden", display: "none" },
                    }}
                  />
                }
                label={<LabelText>{option.label}</LabelText>}
              />
              {option.value === "S" && children}
            </>
          );
        })}
      </RadioGroup>
      <FormHelperText>{errorText}</FormHelperText>
    </FormControl>
  );
};
