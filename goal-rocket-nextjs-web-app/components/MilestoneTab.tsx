import React, { useCallback } from "react";
import AddIcon from "../public/addicon.svg";
import {
  DetailMilestonesRoot,
  DetailMilestoneContainer,
  MilestonesHeader,
  MilestoneAddButtonContainer,
  MilestoneAddButtonIcon,
} from "./ConstantUi";
import { DragsComponent } from "./Draggable";
import { useMilestoneContext } from "../context/MilestonesContext";
import { useAppStateContext } from "../context/AppStateContext";
import { IconButtonComponent } from "./IconButton";
import { MilestoneType } from "../types/milestoneType";
import Loading from "./LoadingUI";
import { LargeText, YellowTextColor } from "./Text";
import { createTimeStamp, firestore, Timestamp } from "../firebase/firebase";
import Router, { useRouter } from "next/router";
import { DropResult } from "react-beautiful-dnd";
import { useProjectContext } from "../context/ProjectsContext";
interface LoadedDragComponentProps {
  activeProjectMilestonesData: MilestoneType[];
  activePath: string;
}

const reorder = (
  list: MilestoneType[],
  startIndex: number,
  endIndex: number
) => {
  const result = Array.from(list);
  const [removed] = result.splice(startIndex, 1);
  result.splice(endIndex, 0, removed);
  return result;
};

const LoadedDragComponentMain: React.FC<LoadedDragComponentProps> = ({
  activePath,
  activeProjectMilestonesData,
}) => {
  const history = useRouter();
  const { updateMilestoneDate } = useMilestoneContext();
  const { setActiveOnEditMilestoneData, setIsMilestoneEditing } =
    useAppStateContext();
  const handleOnDragEnd = useCallback(
    async (result: DropResult) => {
      if (
        !result.destination ||
        result.source.index === result.destination.index
      ) {
        return;
      }

      const items = reorder(
        activeProjectMilestonesData,
        result.source.index,
        result.destination.index
      );
      const willBeEffectedPart = activeProjectMilestonesData.slice(
        result.source.index < result.destination.index
          ? result.source.index
          : result.destination.index,
        result.source.index < result.destination.index
          ? result.destination.index + 1
          : result.source.index + 1
      );
      const newWillEffectedPart = items.slice(
        result.source.index < result.destination.index
          ? result.source.index
          : result.destination.index,
        result.source.index < result.destination.index
          ? result.destination.index + 1
          : result.source.index + 1
      );
      const lastEndDate: Timestamp[] = [];
      willBeEffectedPart.forEach(async (oldPositionedMilestone, index) => {
        if (oldPositionedMilestone === newWillEffectedPart[index]) return;
        if (oldPositionedMilestone !== newWillEffectedPart[index]) {
          if (index === 0) {
            const pathRef =
              activePath + "/milestones/" + newWillEffectedPart[index].id;
            const id = newWillEffectedPart[index].id;
            const duration = newWillEffectedPart[index].duration;
            const progress = newWillEffectedPart[index].progress;
            const newStartDate = oldPositionedMilestone.startDate;
            const newEndDate = createTimeStamp.fromMillis(
              newStartDate.toMillis() + duration * 86400000
            );
            lastEndDate.push(newEndDate);
            const oldEndDate = newWillEffectedPart[index].endDate;
            const oldStartDate = newWillEffectedPart[index].startDate;
            return await updateMilestoneDate({
              pathRef,
              id,
              newEndDate,
              newStartDate,
              oldEndDate,
              oldStartDate,
              sizeOfMilestones: 1,
              currentProgress: progress,
            });
          } else {
            const pathRef =
              activePath + "/milestones/" + newWillEffectedPart[index].id;
            const id = newWillEffectedPart[index].id;
            const progress = newWillEffectedPart[index].progress;
            const duration = newWillEffectedPart[index].duration;
            const newStartDate = lastEndDate[lastEndDate.length - 1];
            const newEndDate = createTimeStamp.fromMillis(
              newStartDate.toMillis() + duration * 86400000
            );
            lastEndDate.push(newEndDate);
            const oldEndDate = newWillEffectedPart[index].endDate;
            const oldStartDate = newWillEffectedPart[index].startDate;
            return await updateMilestoneDate({
              pathRef,
              id,
              newEndDate,
              newStartDate,
              oldEndDate,
              oldStartDate,
              sizeOfMilestones: 1,
              currentProgress: progress,
            });
          }
        }
      });
    },
    [activePath, activeProjectMilestonesData, updateMilestoneDate]
  );

  const handleDragClick = useCallback(
    async (milestone: MilestoneType) => {
      const {
        id,
        goal,
        duration,
        startDate,
        endDate,
        progress,
        relativeProgress,
        description,
        goalAchievingProbability,
        hasProjects,
        level,
        parent,
        progressOfTime,
        topLevelProjectId,
      } = milestone;
      if (!milestone.hasProjects) {
        const milestonePath = activePath + "/milestones/" + id;
        setIsMilestoneEditing(true);
        setActiveOnEditMilestoneData({
          id,
          goal,
          duration,
          startDate,
          endDate,
          progress,
          relativeProgress,
          description,
          goalAchievingProbability,
          hasProjects,
          level,
          parent,
          progressOfTime,
          topLevelProjectId,
          path: milestonePath,
        });
      } else {
        const subProjectRef = await firestore
          .collectionGroup("projects")
          .where("parent", "==", id)
          .limit(1)
          .get();
        console.log("active path ", activePath);
        subProjectRef.docs.map((doc) => {
          history.push(
            `/${activePath}`,
            `/${activePath}/milestones/${id}/projects/${doc.id}`,
            { shallow: true }
          );
        });
      }
    },
    [activePath, history, setActiveOnEditMilestoneData, setIsMilestoneEditing]
  );
  const LoadedDragComponent = useCallback(() => {
    return (
      <DragsComponent
        handleOnDragEnd={handleOnDragEnd}
        dragsId="milestones"
        handleDragClick={handleDragClick}
        orderObjects={activeProjectMilestonesData}
      />
    );
  }, [activeProjectMilestonesData, handleDragClick, handleOnDragEnd]);
  return <LoadedDragComponent />;
};

interface MilestoneTabProps {
  activePath: string;
}
export const MilestoneTab: React.FC<MilestoneTabProps> = ({ activePath }) => {
  const { updateMilestoneDate, createMilestone } = useMilestoneContext();
  const { updateProjectDate } = useProjectContext();
  const { activeProjectMilestonesData, activeProjectData } =
    useAppStateContext();
  const handleAddMilestone = async () => {
    if (!activeProjectMilestonesData) return;
    const lastOneEndDate =
      activeProjectMilestonesData[activeProjectMilestonesData.length - 1]
        .endDate;
    const createdMilestone = await createMilestone(
      activePath + "/milestones",
      lastOneEndDate
    );
    const currentMilestonePath =
      activePath + "/milestones/" + createdMilestone.id;
    if (createdMilestone) {
      await updateMilestoneDate({
        currentProgress: createdMilestone.progress,
        id: createdMilestone.id,
        newEndDate: createdMilestone.endDate,
        newStartDate: createdMilestone.startDate,
        oldEndDate: createdMilestone.endDate,
        oldStartDate: createdMilestone.startDate,
        pathRef: currentMilestonePath,
        sizeOfMilestones: 2,
      }).then(async ({ totalRelativeProgress }) => {
        await updateProjectDate({
          pathRef: activePath,
          startDate: activeProjectData!.startDate,
          progressOfProject: totalRelativeProgress,
          endDate: createdMilestone.endDate,
        });
      });
    }
  };
  return (
    <DetailMilestonesRoot id="milestone-tab-main">
      <DetailMilestoneContainer width={100}>
        <MilestonesHeader>
          <LargeText color={YellowTextColor}>Milestones</LargeText>
          <LargeText>
            In this section you can manage the milestones of your project
          </LargeText>
        </MilestonesHeader>
        <MilestoneAddButtonContainer>
          <IconButtonComponent
            // eslint-disable-next-line react/no-children-prop
            children={<MilestoneAddButtonIcon src={AddIcon} />}
            onClick={handleAddMilestone}
          />
        </MilestoneAddButtonContainer>
        {activeProjectMilestonesData ? (
          <LoadedDragComponentMain
            activePath={activePath}
            activeProjectMilestonesData={activeProjectMilestonesData}
          />
        ) : (
          <>
            <div>There is no milestone under that project</div>
            <Loading loading={true} />
          </>
        )}
      </DetailMilestoneContainer>
    </DetailMilestonesRoot>
  );
};
