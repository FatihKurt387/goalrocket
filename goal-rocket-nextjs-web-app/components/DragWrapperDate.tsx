import React from "react";

import {
  MilestonesDateContentRoot,
  MilestoneDateContentContanier,
} from "./ConstantUi";
import { MediumText, SmallText, YellowTextColor } from "./Text";

interface DragWrapperDateProps {
  startDate: string;
  endDate: string;
  duration: number;
  isEditTabOpened: boolean;
}

export const DragWrapperDate: React.FC<DragWrapperDateProps> = ({
  startDate,
  endDate,
  duration,
  isEditTabOpened,
}) => {
  return (
    <MilestonesDateContentRoot isEditTapOpened={isEditTabOpened}>
      <MilestoneDateContentContanier>
        <SmallText color={YellowTextColor}>Start Date</SmallText>
        <MediumText>{startDate}</MediumText>
      </MilestoneDateContentContanier>
      <MilestoneDateContentContanier>
        <SmallText color={YellowTextColor}>Due Date</SmallText>
        <MediumText>{endDate}</MediumText>
      </MilestoneDateContentContanier>
      <MilestoneDateContentContanier
        style={{ marginRight: isEditTabOpened ? "0px" : "5px" }}
      >
        <SmallText color={YellowTextColor}>Duration</SmallText>
        <MediumText>{duration}</MediumText>
      </MilestoneDateContentContanier>
    </MilestonesDateContentRoot>
  );
};
