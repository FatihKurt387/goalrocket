import React, { useCallback } from "react";
import {
  Detail2,
  DetailContentContainer,
  MilestoneCloseEditTabIcon,
} from "./ConstantUi";
import CloseMilestoneEditTabIcon from "../public/closemilestonetab.svg";
import { MilestoneEditComponent } from "./MilestoneEditComponent";
import { useAppStateContext } from "../context/AppStateContext";
import { calculateDecimalToPercentage } from "../utulities/calculateDecimalToPercentage";
interface MilestoneEditTabProps {}

const MilestoneLoadedEditComponent = () => {
  const { activeOnEditMilestoneData } = useAppStateContext();
  const {
    id,
    duration,
    endDate,
    goal,
    progress,
    relativeProgress,
    startDate,
    description,
    goalAchievingProbability,
    progressOfTime,
    path,
  } = activeOnEditMilestoneData;
  const LoadedEditComponent = useCallback(() => {
    return (
      <>
        <MilestoneEditComponent
          id={id}
          duration={duration}
          endDate={endDate}
          goal={goal}
          description={description}
          progress={progress}
          relativeProgress={relativeProgress}
          progressOfTime={progressOfTime}
          startDate={startDate}
          goalAchievingProbability={Math.round(
            calculateDecimalToPercentage(goalAchievingProbability)
          )}
          path={path}
        />
      </>
    );
  }, [
    description,
    duration,
    endDate,
    goal,
    goalAchievingProbability,
    id,
    path,
    progress,
    progressOfTime,
    relativeProgress,
    startDate,
  ]);
  return <LoadedEditComponent />;
};

export const MilestoneEditTab: React.FC<MilestoneEditTabProps> = () => {
  const { setIsMilestoneEditing } = useAppStateContext();

  return (
    <Detail2 id="Detail-Content-2">
      <MilestoneCloseEditTabIcon
        src={CloseMilestoneEditTabIcon}
        onClick={() => {
          setIsMilestoneEditing(false);
        }}
      />

      <DetailContentContainer id="milestone-edit-tab-container">
        <MilestoneLoadedEditComponent />
      </DetailContentContainer>
    </Detail2>
  );
};
