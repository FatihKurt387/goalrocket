import React, { MouseEvent, useCallback, useState } from "react";
import { useAppStateContext } from "../context/AppStateContext";
import { Timestamp } from "../firebase/firebase";
import threeDotImage from "../public/threedot.svg";
import AddOrDeletePopUp from "./AddorDeletePopUp";
import {
  MilestoneEndThreeDotContainer,
  MilestoneEndThreeDotImage,
} from "./ConstantUi";

interface DragWrapperEndSideProps {
  hasProjects: boolean;
  id: string;
  startDate: Timestamp;
  endDate: Timestamp;
  progress: number;
}

export const DragWrapperEndSide: React.FC<DragWrapperEndSideProps> = ({
  hasProjects,
  id,
  startDate,
  endDate,
  progress,
}) => {
  const [openAddOrDeletePopUp, setOpenAddOrDeletePopUp] =
    useState<boolean>(false);
  const handleThreeDotClick = useCallback(
    (e: MouseEvent<any>) => {
      e.stopPropagation();
      setOpenAddOrDeletePopUp(!openAddOrDeletePopUp);
    },
    [openAddOrDeletePopUp]
  );
  const { setActiveOnEditMilestoneData } = useAppStateContext();

  return (
    <>
      <MilestoneEndThreeDotContainer
        onClick={(e) => {
          handleThreeDotClick(e);
          setActiveOnEditMilestoneData((prevState) => {
            return { ...prevState, id, startDate };
          });
        }}
      >
        <MilestoneEndThreeDotImage src={threeDotImage} />
      </MilestoneEndThreeDotContainer>
      <AddOrDeletePopUp
        hasProjects={hasProjects}
        open={openAddOrDeletePopUp}
        milestoneid={id}
        projectid={1}
        startDate={startDate}
        endDate={endDate}
        progress={progress}
      />
    </>
  );
};
