import React from "react";

import { MilestoneOverViewType } from "../context/MilestonesContext";
import { SubProjectOverViewType } from "../context/SubProjectContext";
import { MilestoneType } from "../types/milestoneType";
import { SubProjectType } from "../types/subProjectType";
import { TreeViewComponent } from "./TreeViewComponent";

interface ProjectOverviewContentProps {
  subProjects?: SubProjectType[];
  milestones?: MilestoneType[];
}

export const ProjectOverviewContent: React.FC<ProjectOverviewContentProps> = ({
  subProjects,
  milestones,
}) => {
  // console.log("result,", result)
  return (
    <TreeViewComponent milestones={milestones} subProjects={subProjects} />
  );
};
