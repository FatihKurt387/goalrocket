import { Formik, Form, Field } from "formik";
import React from "react";
import { milestoneGoalPlaceholder } from "../constant/ContextStrings";
import { EditGoalTextFieldProps } from "../constant/TextFieldsProps";
import { getValidationSchema } from "../utulities/getValidationFormSchema";
import { MilestoneEditHeader, UnderLine } from "./ConstantUi";
import { SubmitButton } from "./FormikFormWithTextField";
import { SmallText, YellowTextColor } from "./Text";
import { TextFormField } from "./TextFormField";

interface MilestoneEditComponentTopProps {
  goal: string;
  activeMilestonePath: string;
  updateMilestoneIdentical: ({
    goal,
    pathRef,
    description,
    hasProjects,
    duration,
  }: {
    goal?: string;
    pathRef: string;
    description?: string;
    hasProjects?: boolean;
    duration?: number;
  }) => Promise<boolean | undefined>;
}

export const MilestoneEditComponentTop: React.FC<MilestoneEditComponentTopProps> =
  ({ activeMilestonePath, goal, updateMilestoneIdentical }) => {
    return (
      <MilestoneEditHeader id="milestone-edit-tab-header">
        <SmallText color={YellowTextColor}>Goal</SmallText>
        <Formik
          validationSchema={getValidationSchema(goal, "goal", 75)}
          initialValues={{ goal }}
          onSubmit={async (values) => {
            const obj = {
              pathRef: activeMilestonePath,
              ...values,
            };
            return await updateMilestoneIdentical(obj);
          }}
        >
          {(formik) => {
            return (
              <Form>
                <Field
                  component={TextFormField}
                  {...EditGoalTextFieldProps}
                  name="goal"
                  placeholder={milestoneGoalPlaceholder}
                />
                <UnderLine />
                <SubmitButton
                  type="submit"
                  onClick={() => formik.handleSubmit()}
                />
              </Form>
            );
          }}
        </Formik>
      </MilestoneEditHeader>
    );
  };
