import React from "react";
import { Timestamp } from "../firebase/firebase";
import { getLinearProgressBarPositions } from "../hooks/useProjectPositioning";
import { LoadedLinearProgressBar } from "./TopPanel";
import { calculateDecimalToPercentage } from "../utulities/calculateDecimalToPercentage";
import { CircledProgressBar } from "./CircleProgressContent";
import { MilestoneStatusBarsContentContainer } from "./ConstantUi";
// import { LinearProgressBar } from "./LinearProgressBar";

interface DragWrapperStatusProps {
  goalAchievingProbability: number;
  startDate: Timestamp;
  endDate: Timestamp;
  progressOfProject: number;
  progressOfTime: number;
}

export const DragWrapperStatus: React.FC<DragWrapperStatusProps> = ({
  goalAchievingProbability,
  startDate,
  endDate,
  progressOfProject,
  progressOfTime,
}) => {
  const linearBarData = getLinearProgressBarPositions(
    false,
    startDate,
    endDate,
    progressOfProject,
    progressOfTime
  );
  return (
    <MilestoneStatusBarsContentContainer id="wrapper-status-bar-container">
      <CircledProgressBar
        progressValue={goalAchievingProbability}
        isSmallSize
      />
      <div
        style={{
          width: "70%",
          padding: "5px",
          marginTop: "5px",
          marginLeft: "5px",
        }}
      >
        <LoadedLinearProgressBar
          startDate={startDate}
          endDate={endDate}
          progressOfProject={Math.round(
            calculateDecimalToPercentage(progressOfProject)
          )}
          isSmallSize
          plannedEndDate={null}
          {...linearBarData}
        />
      </div>
    </MilestoneStatusBarsContentContainer>
  );
};
