import React from "react";
import { DetailContent } from "./ConstantUi";
import { MilestoneEditTab } from "./MilestoneEditTab";
import { MilestoneTab } from "./MilestoneTab";
import { ProjectTab } from "./ProjectTab";

interface DetailBodyProps {
  isMilestoneEditing: boolean;
  isMilestoneTab: boolean;
  activePath: string;
}

export const DetailBody: React.FC<DetailBodyProps> = ({
  isMilestoneEditing,
  isMilestoneTab,
  activePath,
}) => {
  return (
    <>
      <DetailContent id="Detail-Content">
        {isMilestoneTab ? (
          <MilestoneTab activePath={activePath} />
        ) : (
          <ProjectTab activePath={activePath} />
        )}
      </DetailContent>
      {isMilestoneEditing && <MilestoneEditTab />}
    </>
  );
};
