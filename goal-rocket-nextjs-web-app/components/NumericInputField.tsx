import { Field } from "formik";
import React from "react";
import { NumericFieldProps } from "../constant/TextFieldsProps";
import { TextFormField } from "./TextFormField";

interface NumericInputFieldProps {
  name: string;
  id: string;
  min?: number;
  max?: number;
  width?: number;
  onChange?: (e: any) => void;
}

export const NumericInputField: React.FC<NumericInputFieldProps> = ({
  id,
  name,
  min,
  max,
  onChange,
  width,
}) => {
  return (
    <Field
      style={{ width: width + "%" }}
      onChange={onChange}
      name={name}
      id={id}
      {...NumericFieldProps(min, max)}
      component={TextFormField}
    />
  );
};
