import { IconButton, IconButtonProps } from "@material-ui/core";
import React from "react";

export const IconButtonComponent: React.FC<IconButtonProps> = ({
  ...props
}) => {
  return <IconButton {...props}>{props.children}</IconButton>;
};
