import React, { useCallback } from "react";
import DiscardSvgIcon from "../public/discard.svg";
import AcceptIcon from "../public/accept.svg";
import {
  DescriptionBoxContainer,
  DiscardIcon,
  MilestoneEditBottom,
  SaveDiscardContainer,
  SaveIcon,
} from "./ConstantUi";
import { Field, Form, Formik } from "formik";
import { getValidationSchema } from "../utulities/getValidationFormSchema";
import { useMilestoneContext } from "../context/MilestonesContext";
import { IconButton } from "@material-ui/core";
import { DescriptionTextFieldProps } from "../constant/TextFieldsProps";
import { TextFormField } from "./TextFormField";
import { SmallText, YellowTextColor } from "./Text";
import { milestoneDescriptionPlaceHolder } from "../constant/ContextStrings";
import { MilestoneEditComponentTop } from "./MilestoneEditComponentTop";
import { MilestoneEditComponentBody } from "./MilestoneEditComponentBody";
import { Timestamp } from "../firebase/firebase";

export interface MilestoneEditComponentProps {
  id: string;
  goal: string;
  description: string;
  duration: number;
  startDate: Timestamp;
  endDate: Timestamp;
  progress: number;
  relativeProgress: number;
  progressOfTime: number;
  goalAchievingProbability: number;
  path: string;
}

export const MilestoneEditComponent: React.FC<MilestoneEditComponentProps> = ({
  id,
  goal,
  description,
  duration,
  startDate,
  endDate,
  progress,
  progressOfTime,
  relativeProgress,
  goalAchievingProbability,
  path,
}) => {
  const { updateMilestoneIdentical } = useMilestoneContext();
  const LoadedEditComponentTop = useCallback(
    () => (
      <MilestoneEditComponentTop
        activeMilestonePath={path}
        goal={goal}
        updateMilestoneIdentical={updateMilestoneIdentical}
      />
    ),
    [goal, path, updateMilestoneIdentical]
  );
  const LoadedMilestoneEditComponentBody = useCallback(
    () => (
      <MilestoneEditComponentBody
        activeMilestonePath={path}
        id={id}
        startDate={startDate}
        endDate={endDate}
        relativeProgress={relativeProgress}
        goalAchievingProbability={goalAchievingProbability}
        progress={progress}
        progressOfTime={progressOfTime}
        duration={duration}
      />
    ),
    [
      duration,
      endDate,
      goalAchievingProbability,
      id,
      path,
      progress,
      progressOfTime,
      relativeProgress,
      startDate,
    ]
  );
  const LoadedMilestoneEditButtom = useCallback(
    () => (
      <MilestoneEditBottom
        style={{ marginTop: "55px" }}
        id="milestone-edit-tab-bottom"
      >
        <SmallText color={YellowTextColor}>Comment</SmallText>
        <Formik
          validationSchema={getValidationSchema(
            description,
            "description",
            5000
          )}
          initialValues={{ description: description }}
          onSubmit={async (values) => {
            const obj = {
              pathRef: path,
              ...values,
            };
            return await updateMilestoneIdentical(obj);
          }}
        >
          {(formik) => {
            return (
              <DescriptionBoxContainer>
                <Form
                  style={{
                    display: "flex",
                    width: "100%",
                    flexDirection: "column",
                  }}
                >
                  <Field
                    component={TextFormField}
                    name="description"
                    {...DescriptionTextFieldProps}
                    placeholder={milestoneDescriptionPlaceHolder}
                  />
                  <SaveDiscardContainer>
                    <IconButton
                      onSubmit={() => formik.handleSubmit()}
                      type="submit"
                    >
                      <SaveIcon src={AcceptIcon}></SaveIcon>
                    </IconButton>
                    <IconButton
                      onClick={() => {
                        formik.handleReset();
                      }}
                      key="discardbutton"
                      id="discardbutton"
                    >
                      <DiscardIcon src={DiscardSvgIcon}></DiscardIcon>
                    </IconButton>
                  </SaveDiscardContainer>
                </Form>
              </DescriptionBoxContainer>
            );
          }}
        </Formik>
      </MilestoneEditBottom>
    ),
    [description, path, updateMilestoneIdentical]
  );
  return (
    <>
      <LoadedEditComponentTop />
      <LoadedMilestoneEditComponentBody />
      <LoadedMilestoneEditButtom />
    </>
  );
};
