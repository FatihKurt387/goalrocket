import React from "react";
import UserLogo from "../public/userDefault.svg";
import IfLogo from "../public/iflogo.svg";
import {
  LeaftAsideContent2Image,
  LeftAside,
  LeftAsideContent2,
  LeftAsideContent2TextBox,
  LeftAsideContentBox,
  LeftAsideUserContent,
  UserLogoImage,
  UserNameContainer,
} from "./ConstantUi";
import { MediumText, YellowTextColor } from "./Text";

interface LeftPanelProps {}

export const LeftPanel: React.FC<LeftPanelProps> = () => {
  return (
    <LeftAside id="LeftAside">
      <LeftAsideContentBox>
        <LeftAsideUserContent>
          <UserLogoImage src={UserLogo} />
          <UserNameContainer>
            <MediumText>Fatih</MediumText>
          </UserNameContainer>
        </LeftAsideUserContent>
        <LeftAsideContent2>
          <LeaftAsideContent2Image src={IfLogo} />
          <LeftAsideContent2TextBox>
            <MediumText color={YellowTextColor}>Project</MediumText>
          </LeftAsideContent2TextBox>
        </LeftAsideContent2>
      </LeftAsideContentBox>
    </LeftAside>
  );
};
