import { CheckBoxFormField } from "../Checkbox";
import React, { Dispatch, useEffect, useState } from "react";
import styled from "styled-components";
import { Button, FormControl, FormGroup } from "@material-ui/core";
import { useRouter } from "next/router";
import DeleteIcon from "../../public/delete.svg";
import AddIcon from "../../public/addicon.svg";
import {
  MilestoneAddButtonContainer,
  MilestoneAddButtonIcon,
  MilestoneDeleteIcon,
  MilestoneEndThreeDotContainer,
  MilestonesWrapper,
  UnderLine,
} from "../ConstantUi";
import { ProjectIdenticalType } from "../../types/projectType";
import { Field, Formik } from "formik";
import uniqid from "uniqid";
import { EditGoalTextFieldProps } from "../../constant/TextFieldsProps";
import { SubmitButton } from "../FormikFormWithTextField";
import { TextFormField } from "../TextFormField";
import { useProjectContext } from "../../context/ProjectsContext";
import { useMilestoneContext } from "../../context/MilestonesContext";
import { usePathTrackingContext } from "../../context/PathTrackingContext";
import { MediumText, SmallText, YellowTextColor } from "../Text";
import { NumericInputField } from "../NumericInputField";
import { useAppStateContext } from "../../context/AppStateContext";

const Form = styled.form`
  display: flex;
  height: 100%;
  width: 100%;
  flex-direction: column;
  justify-content: space-between;
`;
const CreateProjectRadioInputSubmitButton = styled.div`
  display: flex;
  justify-content: center;
  background: rgba(99, 99, 128, 0.1);
  align-self: flex-end;
  width: 163px;
  height: 43px;
  border-radius: 5px;
  font-weight: 400;
  border: 3px solid #484862;
`;

interface MilestoneWrapperForCreateSubProjectProps {
  duration: number;
  goal: string;
  id: string;
  activeSubProjectLength: number;
  onDuration: (duration: number, id: string) => void;
  onGoal: (goal: string, id: string) => void;
  deleteSubProject: (id: string) => void;
  addSubProject: () => void;
}

const MilestoneWrapperForCreateSubProject: React.FC<MilestoneWrapperForCreateSubProjectProps> =
  ({
    duration,
    goal,
    id,
    activeSubProjectLength,
    onGoal,
    onDuration,
    deleteSubProject,
  }) => {
    const [onMouse, setOnMouse] = React.useState<boolean>(false);
    const onMouseOverWrapper = () => {
      setOnMouse(true);
    };
    const onMouseLeaveWrapper = () => {
      setOnMouse(false);
    };
    return (
      <>
        <MilestonesWrapper
          onMouseEnter={onMouseOverWrapper}
          onMouseLeave={onMouseLeaveWrapper}
          mouseOver={onMouse}
        >
          <div
            style={{
              display: "flex",
              flexDirection: "column",
              width: "70%",
              maxWidth: "80%",
            }}
          >
            <SmallText color={YellowTextColor}>Goal</SmallText>

            <Formik
              initialValues={{ goal: goal }}
              onSubmit={async (values) => {
                alert(JSON.stringify(values));
              }}
            >
              {({ handleChange }) => {
                return (
                  <>
                    <Field
                      name="goal"
                      onChange={(e: any) => {
                        handleChange(e);
                        onGoal(e.target.value, id);
                      }}
                      {...EditGoalTextFieldProps}
                      component={TextFormField}
                    />
                    <UnderLine />
                  </>
                );
              }}
            </Formik>
          </div>
          <div
            style={{
              display: "flex",
              flexDirection: "column",
              height: "100%",
              justifyContent: "space-between",
            }}
          >
            <SmallText color={YellowTextColor}>Duration</SmallText>

            <Formik onSubmit={() => {}} initialValues={{ duration }}>
              {({ values, handleSubmit, handleChange }) => {
                return (
                  <div style={{ display: "flex", flexDirection: "row" }}>
                    <NumericInputField
                      name="duration"
                      onChange={(e) => {
                        handleChange(e);
                        onDuration(e.target.value, id);
                      }}
                      id="duration"
                      width={30}
                      min={1}
                    />
                    <SubmitButton
                      onClick={(e) => {
                        e.preventDefault();
                        handleSubmit();
                      }}
                      type="submit"
                    />
                    <MediumText isMargin>
                      {values.duration > 1 ? "Days" : "Day"}
                    </MediumText>
                  </div>
                );
              }}
            </Formik>
            <UnderLine
              style={{ display: "flex", width: "30%", marginTop: "15px" }}
            />
          </div>
          <MilestoneEndThreeDotContainer
            onClick={() => {
              if (activeSubProjectLength > 1) {
                //do here
                // deleteSubProject(activeSubProject.id);
              } else {
                //do not
              }
            }}
            style={{
              alignItems: "center",
              justifyContent: "flex-end",
              justifySelf: "flex-end",
              cursor: activeSubProjectLength <= 1 ? "none" : "pointer",
            }}
          >
            <MilestoneDeleteIcon
              style={{
                cursor: activeSubProjectLength <= 1 ? "none" : "pointer",
              }}
              color={
                activeSubProjectLength <= 1
                  ? "rgb(240, 240, 255,0.3)"
                  : "rgb(240, 240, 255)"
              }
              onClick={() => deleteSubProject(id)}
              src={DeleteIcon}
            ></MilestoneDeleteIcon>
          </MilestoneEndThreeDotContainer>
        </MilestonesWrapper>
      </>
    );
  };
interface CreateStepByStepSubProejctI {
  setActiveProjects: Dispatch<any>;
}
export const CreateStepByStepSubProjectComponent: React.FC<CreateStepByStepSubProejctI> =
  ({ setActiveProjects }) => {
    const [activeSubProjects, setActiveSubProjects] = useState<
      ProjectIdenticalType[]
    >([
      {
        description: "",
        goal: "",
        goalAchievingProbability: 0,
        id: uniqid(),
        isTopLevel: false,
        level: 0,
        collectionPaths: null,
        duration: 7,
      },
    ]);
    useEffect(() => {
      setActiveProjects(activeSubProjects);
    }, [activeSubProjects, setActiveProjects]);
    const handleGoalChange = (goal: string, id: string) => {
      let newArr = [...activeSubProjects];
      let updateArr = newArr.filter((subProject) => {
        if (subProject.id === id) {
          subProject.goal = goal;
        }
        return subProject;
      });
      setActiveSubProjects(updateArr);
    };
    const handleDurationChange = (duration: number, id: string) => {
      let newArr = [...activeSubProjects];
      let updateArr = newArr.filter((subProject) => {
        if (subProject.id === id) {
          subProject.duration = Number(duration);
        }
        return subProject;
      });
      setActiveSubProjects(updateArr);
    };

    const handleAddSubProject = () => {
      setActiveSubProjects([
        ...activeSubProjects,
        {
          description: "",
          goal: "",
          goalAchievingProbability: 0,
          id: uniqid(),
          isTopLevel: false,
          level: 0,
          collectionPaths: null,
          duration: 7,
        },
      ]);
    };
    const handleDeleteSubProjectOnList = (id: string) => {
      if (activeSubProjects.length > 1) {
        const newArr = [...activeSubProjects];
        const deletedItems = newArr.filter((project) => project.id !== id);
        setActiveSubProjects(deletedItems);
      }
    };

    return (
      <>
        <MilestoneAddButtonContainer>
          <MilestoneAddButtonIcon
            onClick={handleAddSubProject}
            small
            src={AddIcon}
          ></MilestoneAddButtonIcon>
        </MilestoneAddButtonContainer>
        {activeSubProjects.map(({ goal, duration, id }) => {
          return (
            <>
              <MilestoneWrapperForCreateSubProject
                onGoal={handleGoalChange}
                key={id}
                onDuration={handleDurationChange}
                addSubProject={handleAddSubProject}
                deleteSubProject={handleDeleteSubProjectOnList}
                activeSubProjectLength={activeSubProjects.length}
                goal={goal}
                id={id}
                duration={duration}
              />
              ;
            </>
          );
        })}
      </>
    );
  };

const createProjectTypesOptions = [
  { label: "Step By Step", value: "S" },
  { label: "All At Once", value: "A" },
  { label: "Cycle Up", value: "C" },
];

export interface CreateProjectFormProps {
  isFullWith?: boolean;
  isSubProject: boolean;
}

export const CreateProjectForm: React.FC<CreateProjectFormProps> = ({
  isSubProject,
}) => {
  const { createProject } = useProjectContext();
  const { setIsCreatingSubProjectScreenOpened, activeOnEditMilestoneData } =
    useAppStateContext();
  const { specificCurrentPath } = usePathTrackingContext();
  const activePath = specificCurrentPath.substring(1);
  const { updateMilestoneIdentical } = useMilestoneContext();
  const [activeSubProject, setActiveProject] =
    useState<ProjectIdenticalType[]>();
  const history = useRouter();
  const handleCreateProjectSubmit = async (values: { value: string }) => {
    if (!isSubProject && values.value === "S") {
      await createProject().then((res) => history.push(`/projects/${res}`));
    } else {
      const subMilestonePath =
        activePath + "/milestones/" + activeOnEditMilestoneData.id;

      if (values.value === "S")
        await createProject(
          subMilestonePath,
          activeSubProject,
          activeOnEditMilestoneData.startDate
        );
      await updateMilestoneIdentical({
        hasProjects: true,
        pathRef: subMilestonePath,
      });
    }
    setIsCreatingSubProjectScreenOpened(false);
  };
  return (
    <Formik
      initialValues={{ value: "S" }}
      onSubmit={(values) => handleCreateProjectSubmit(values)}
    >
      {({ handleSubmit, values }) => (
        <Form
          onSubmit={(e) => {
            e.preventDefault();
            handleSubmit(e);
          }}
        >
          <FormControl
            component="fieldset"
            style={{ height: "100%", maxHeight: "fit-content" }}
            required
          >
            <FormGroup>
              <Field
                name="value"
                options={createProjectTypesOptions}
                children={
                  isSubProject && values.value === "S" ? (
                    <CreateStepByStepSubProjectComponent
                      setActiveProjects={setActiveProject}
                    />
                  ) : null
                }
                component={CheckBoxFormField}
              />
            </FormGroup>
            <CreateProjectRadioInputSubmitButton>
              <Button
                type="submit"
                style={{
                  color: "white",
                  width: "100%",
                  height: "100%",
                  fontWeight: "inherit",
                  fontFamily: "Aileron",
                }}
              >
                Create Project
              </Button>
            </CreateProjectRadioInputSubmitButton>
          </FormControl>
        </Form>
      )}
    </Formik>
  );
};
