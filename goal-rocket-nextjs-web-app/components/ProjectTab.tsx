import { projectDescriptionPlaceHolder } from "../constant/ContextStrings";
import { DescriptionTextFieldProps } from "../constant/TextFieldsProps";
import { useAppStateContext } from "../context/AppStateContext";
import { useMilestoneContext } from "../context/MilestonesContext";
import { useProjectContext } from "../context/ProjectsContext";
import { useSubProjectContext } from "../context/SubProjectContext";
import { createTimeStamp } from "../firebase/firebase";
import AcceptIcon from "../public/accept.svg";
import DiscardSvgIcon from "../public/discard.svg";
import { getValidationSchema } from "../utulities/getValidationFormSchema";
import {
  DescriptionBoxContainer,
  DetailContentContainer,
  DiscardIcon,
  SaveDiscardContainer,
  SaveIcon,
} from "./ConstantUi";
import { DateInputField } from "./DateInputField";
import { MediumText, SmallText, YellowTextColor } from "./Text";
import { TextFormField } from "./TextFormField";
import { IconButton } from "@material-ui/core";
import { Field, Form, Formik } from "formik";
import React, { useCallback } from "react";

interface ProjectTabProps {
  activePath: string;
}

export const ProjectTab: React.FC<ProjectTabProps> = ({ activePath }) => {
  const {
    setActiveProjectData,
    activeProjectData,
    activeProjectMilestonesData,
  } = useAppStateContext();
  const { isTopLevel, startDate, description, plannedEndDate, endDate } =
    activeProjectData!;
  const { updateProjectDate, updateProjectIdentical } = useProjectContext();
  const { updateMilestoneDate } = useMilestoneContext();
  const { updateSubProjectIdentical } = useSubProjectContext();
  const ProjectTabBottom = useCallback(() => {
    return (
      <>
        <SmallText color={YellowTextColor}>Description</SmallText>
        <Formik
          validationSchema={getValidationSchema(
            description ?? "",
            "description",
            5000
          )}
          initialValues={{ description: description ?? "" }}
          onSubmit={async ({ description }) => {
            if (isTopLevel) {
              return await updateProjectIdentical({
                pathRef: activePath,
                description,
              });
            } else {
              const obj = { pathRef: activePath, description };
              return await updateSubProjectIdentical(obj);
            }
          }}
        >
          {(formik) => {
            return (
              <DescriptionBoxContainer
                style={{
                  padding: "5px",
                  maxHeight: "400px",
                  marginTop: "13px",
                }}
              >
                <Form
                  style={{
                    display: "flex",
                    width: "100%",
                    flexDirection: "column",
                  }}
                >
                  <Field
                    component={TextFormField}
                    name="description"
                    {...DescriptionTextFieldProps}
                    placeholder={projectDescriptionPlaceHolder}
                  />
                  <SaveDiscardContainer>
                    <IconButton
                      onSubmit={() => formik.handleSubmit()}
                      type="submit"
                    >
                      <SaveIcon src={AcceptIcon}></SaveIcon>
                    </IconButton>
                    <IconButton
                      onClick={() => {
                        formik.handleReset();
                      }}
                      key="discardbutton"
                      id="discardbutton"
                    >
                      <DiscardIcon src={DiscardSvgIcon}></DiscardIcon>
                    </IconButton>
                  </SaveDiscardContainer>
                </Form>
              </DescriptionBoxContainer>
            );
          }}
        </Formik>
      </>
    );
  }, [
    activePath,
    description,
    isTopLevel,
    updateProjectIdentical,
    updateSubProjectIdentical,
  ]);
  const ProjectTabTop = useCallback(() => {
    return (
      <div
        style={{
          display: "flex",
          flexDirection: "row",
          marginBottom: "50px",
          flex: 1,
        }}
      >
        <div
          style={{
            display: "flex",
            flexDirection: "row",
            flex: 2,
            justifyContent: "space-between",
          }}
        >
          <Formik
            initialValues={{
              startDate: startDate.toDate(),
            }}
            onSubmit={async ({ startDate }) => {
              const milestoneNewStartDate = createTimeStamp.fromDate(startDate);
              if (activeProjectMilestonesData) {
                const currentFirstMilestone = activeProjectMilestonesData[0];
                const milestonePathRef =
                  activePath + "/milestones/" + currentFirstMilestone.id;
                const newEndDateForMilestone = createTimeStamp.fromMillis(
                  milestoneNewStartDate.toMillis() +
                    currentFirstMilestone.duration * 86400000
                );
                setActiveProjectData((prevState) => {
                  return prevState
                    ? { ...prevState, startDate: milestoneNewStartDate }
                    : null;
                });
                await updateMilestoneDate({
                  pathRef: milestonePathRef,
                  currentProgress: currentFirstMilestone.progress,
                  id: currentFirstMilestone.id,
                  sizeOfMilestones: activeProjectMilestonesData.length,
                  newEndDate: newEndDateForMilestone,
                  newStartDate: milestoneNewStartDate,
                  oldEndDate: currentFirstMilestone.endDate,
                  oldStartDate: currentFirstMilestone.startDate,
                }).then(async ({ endDate, totalRelativeProgress }) => {
                  await updateProjectDate({
                    pathRef: activePath,
                    startDate: createTimeStamp.fromDate(startDate),
                    endDate: endDate,
                    progressOfProject: totalRelativeProgress,
                  });
                });
              }
            }}
          >
            {({ handleSubmit, handleChange }) => {
              return (
                <DateInputField
                  onChange={handleChange}
                  isJustReadOnly={!isTopLevel}
                  name={"startDate"}
                  label={"Start Date"}
                  isShowTodayButton
                  handleAccept={handleSubmit}
                  id={"startDate"}
                />
              );
            }}
          </Formik>
          <Formik
            initialValues={
              isTopLevel
                ? { plannedEndDate: plannedEndDate?.toDate() }
                : { endDate: endDate.toDate() }
            }
            onSubmit={async (values) => {
              return isTopLevel
                ? values.plannedEndDate
                  ? await updateProjectIdentical({
                      pathRef: activePath,
                      plannedEndDate: createTimeStamp.fromDate(
                        values.plannedEndDate
                      ),
                    })
                  : null
                : null;
            }}
          >
            {({ handleSubmit, handleChange }) => {
              return (
                <DateInputField
                  onChange={handleChange}
                  name={isTopLevel ? "plannedEndDate" : "endDate"}
                  isJustReadOnly={!isTopLevel}
                  label={isTopLevel ? "Planned End Date" : "End Date"}
                  handleAccept={handleSubmit}
                  id={isTopLevel ? "plannedEndDate" : "endDate"}
                  minDate={startDate.toDate()}
                />
              );
            }}
          </Formik>
        </div>
        <div
          style={{
            display: "flex",
            flex: 3,
            justifyContent: "flex-end",
            alignItems: "flex-end",
            flexDirection: "column",
          }}
        >
          <SmallText color={YellowTextColor}>Project Type</SmallText>
          <MediumText>Step By Step</MediumText>
        </div>
      </div>
    );
  }, [
    activePath,
    activeProjectMilestonesData,
    endDate,
    isTopLevel,
    plannedEndDate,
    setActiveProjectData,
    startDate,
    updateMilestoneDate,
    updateProjectDate,
    updateProjectIdentical,
  ]);
  return (
    <DetailContentContainer id="project-tab-main">
      <ProjectTabTop />
      <ProjectTabBottom />
    </DetailContentContainer>
  );
};
