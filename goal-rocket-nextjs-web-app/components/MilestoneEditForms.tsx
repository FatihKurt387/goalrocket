import { Formik, Form } from "formik";
import React from "react";

import { useAppStateContext } from "../context/AppStateContext";
import { useMilestoneContext } from "../context/MilestonesContext";
import { useProjectContext } from "../context/ProjectsContext";
import { createTimeStamp, Timestamp } from "../firebase/firebase";
import { calculateDecimalToPercentage } from "../utulities/calculateDecimalToPercentage";
import { calculateGoalAchievingProbability } from "../utulities/calculateGoalAchievingProbability";
import { calculatePercentageToDecimal } from "../utulities/calculatePercentageToDecimal";
import { calculateProgressOfTime } from "../utulities/calculateProgressOfTime";
import { chainStartDateUpdate } from "../utulities/chainDateUpdate";
import { getCalculatedDateDifferance } from "../utulities/getDateDifferences";
import { UnderLine } from "./ConstantUi";
import { DateInputField } from "./DateInputField";
import { NumericInputField } from "./NumericInputField";
import { SmallText, YellowTextColor, MediumText } from "./Text";

interface MilestoneEditFormsProps {
  startDate: Timestamp;
  endDate: Timestamp;
  duration: number;
  relativeProgress: number;
  progress: number;
  id: string;
  activeMilestonePath: string;
}
const StartDateForm: React.FC<{
  initialStartDate: Date;
  handleStartDateSubmit: (newStartDate: Date) => Promise<void>;
}> = ({ initialStartDate, handleStartDateSubmit }) => (
  <Formik
    initialValues={{ startDate: initialStartDate }}
    onSubmit={({ startDate }) => {
      handleStartDateSubmit(startDate);
    }}
  >
    {({ handleSubmit, handleChange }) => {
      return (
        <DateInputField
          handleAccept={handleSubmit}
          name="startDate"
          isShowTodayButton
          label="Start Date"
          id="startDate"
          onChange={handleChange}
        />
      );
    }}
  </Formik>
);
interface EndDateFormProps {
  minDate: Date;
  initialEndDate: Date;
  handleEndDateSubmit: (newEndDate: Date) => Promise<void>;
}
const EndDateForm: React.FC<EndDateFormProps> = ({
  handleEndDateSubmit,
  minDate,
  initialEndDate,
}) => (
  <Formik
    initialValues={{ endDate: initialEndDate }}
    onSubmit={({ endDate }) => {
      handleEndDateSubmit(endDate);
    }}
  >
    {({ handleSubmit, handleChange }) => {
      return (
        <DateInputField
          onChange={handleChange}
          name="endDate"
          label="End Date"
          handleAccept={handleSubmit}
          id="endDate"
          minDate={minDate}
        />
      );
    }}
  </Formik>
);
const DurationForm: React.FC<{
  initialDuration: number;
  handleDurationSubmit: (newDuration: number) => Promise<void>;
}> = ({ initialDuration, handleDurationSubmit }) => (
  <Formik
    initialValues={{ duration: initialDuration }}
    onSubmit={({ duration }) => {
      handleDurationSubmit(duration);
    }}
  >
    {({ values, handleChange }) => {
      return (
        <Form
          style={{
            display: "flex",
            flexDirection: "column",
          }}
        >
          <SmallText color={YellowTextColor}>Duration</SmallText>
          <div
            style={{
              display: "flex",
              flexDirection: "row",
            }}
          >
            <NumericInputField
              name="duration"
              onChange={(e) => {
                handleChange(e);
              }}
              id="duration"
              width={30}
              min={1}
            />
            <MediumText isMargin>
              {values.duration > 1 ? "Days" : "Day"}
            </MediumText>
          </div>
          <UnderLine style={{ width: "30%" }} />
        </Form>
      );
    }}
  </Formik>
);
const ProgressForm: React.FC<{
  progress: number;
  handleProgressSubmit: (newProgress: number) => Promise<void>;
}> = ({ progress, handleProgressSubmit }) => (
  <Formik
    initialValues={{ progress: calculateDecimalToPercentage(progress) }}
    onSubmit={({ progress }) => {
      handleProgressSubmit(calculatePercentageToDecimal(progress));
    }}
  >
    {({ handleChange }) => {
      return (
        <Form
          style={{
            display: "flex",
            flexDirection: "column",
          }}
        >
          <SmallText color={YellowTextColor}>Progress</SmallText>
          <div style={{ display: "flex", flexDirection: "row" }}>
            <NumericInputField
              name="progress"
              onChange={(e) => {
                handleChange(e);
              }}
              id="progress"
              min={0}
              max={100}
            />
            <MediumText>%</MediumText>
          </div>
          <UnderLine style={{ width: "30%" }} />
        </Form>
      );
    }}
  </Formik>
);
export const MilestoneEditForms: React.FC<MilestoneEditFormsProps> = ({
  endDate,
  startDate,
  duration,
  progress,
  relativeProgress,
  activeMilestonePath,
  id,
}) => {
  const {
    updateMilestoneDate,
    updateMilestoneIdentical,
    calculateMilestoneRelativeProgress,
  } = useMilestoneContext();
  const { updateProjectDate } = useProjectContext();
  const {
    activeProjectData,
    activeProjectMilestonesData,
    setActiveOnEditMilestoneData,
  } = useAppStateContext();
  if (!activeProjectMilestonesData) return null;
  const totalDurations = activeProjectMilestonesData.reduce(
    (accumulator, currentValue) => accumulator + currentValue.duration,
    0
  );
  const totalRelativeProgress = activeProjectMilestonesData.reduce(
    (accumulator, currentValue) => accumulator + currentValue.relativeProgress,
    0
  );
  const activeProjectPath = activeMilestonePath
    .split("/")
    .slice(0, activeMilestonePath.split("/").length - 2)
    .join("/");
  const handleStartDateSubmit = async (newStartDate: Date) => {
    const newStartDateForMilestone = createTimeStamp.fromDate(newStartDate);
    const newEndDateForMilestone = createTimeStamp.fromMillis(
      newStartDateForMilestone.toMillis() + duration * 86400000
    );
    const isThatFirstMilestoneOfProject: boolean =
      activeProjectMilestonesData.filter((milestone) => milestone.id === id)[0]
        .id === id;
    const res = await chainStartDateUpdate({
      activeMilestonePath,
      id,
      isThatFirstMilestoneOfProject,
      newEndDateForMilestone,
      newStartDateForMilestone,
      oldEndDate: endDate,
      oldStartDate: startDate,
      progress,
      sizeOfMilestones: activeProjectMilestonesData.length,
      activeProjectData,
      updateMilestoneDate,
      updateMilestoneIdentical,
      updateProjectDate,
    });
    if (res) {
      setActiveOnEditMilestoneData((prev) => {
        return {
          ...prev,
          endDate: res.endDate,
          startDate: res.startDate,
          duration: res.duration,
          goalAchievingProbability: res.goalAchievingProbability,
          progressOfTime: res.progressOfTime,
        };
      });
    }
  };
  const handleEndDateSubmit = async (newEndDate: Date) => {
    try {
      const newEndDatee = createTimeStamp.fromDate(newEndDate);
      // actula chain updates
      await updateMilestoneDate({
        pathRef: activeMilestonePath,
        id: id,
        newStartDate: startDate,
        newEndDate: newEndDatee,
        sizeOfMilestones: activeProjectMilestonesData
          ? activeProjectMilestonesData.length
          : 0,
        oldEndDate: endDate,
        oldStartDate: startDate,
        currentProgress: progress,
      })
        .then(async ({ totalRelativeProgress, endDate }) => {
          // database chain updates
          const newDuration = getCalculatedDateDifferance(
            newEndDatee.toDate(),
            startDate.toDate()
          );
          await updateMilestoneIdentical({
            pathRef: activeMilestonePath,
            duration: newDuration,
          });
          await updateProjectDate({
            pathRef: activeProjectPath,
            endDate: endDate,
            startDate: activeProjectData!.startDate,
            progressOfProject: totalRelativeProgress,
          });
          const progressOfTime = calculateProgressOfTime(
            startDate,
            newEndDatee
          );
          const goalAchievingProbability = calculateGoalAchievingProbability(
            progress,
            progressOfTime
          );
          return {
            endDate: newEndDatee,
            duration: newDuration,
            progressOfTime: progressOfTime,
            goalAchievingProbability,
          };
        })
        .then(
          ({ progressOfTime, endDate, goalAchievingProbability, duration }) => {
            //locally
            setActiveOnEditMilestoneData((prev) => {
              return {
                ...prev,
                endDate,
                duration,
                goalAchievingProbability,
                progressOfTime,
              };
            });
          }
        );
    } catch (e) {
      console.log(e);
    }
  };
  const handleProgressUpdate = async (newProgress: number) => {
    try {
      // console.log("newprogress", newProgress);
      const newRelativeProgress = calculateMilestoneRelativeProgress(
        newProgress,
        duration,
        totalDurations
      );
      const currentProgressOfTime = calculateProgressOfTime(startDate, endDate);
      const newGoalAchievingProbability = calculateGoalAchievingProbability(
        newProgress,
        currentProgressOfTime
      );
      await updateMilestoneIdentical({
        pathRef: activeMilestonePath,
        progress: newProgress,
        relativeProgress: newRelativeProgress,
        goalAchievingProbability: newGoalAchievingProbability,
      });
      const newTotalRelativeProgress =
        totalRelativeProgress - relativeProgress + newRelativeProgress;
      // console.log("new relative progress", newTotalRelativeProgress);
      await updateProjectDate({
        pathRef: activeProjectPath,
        startDate: activeProjectData!.startDate,
        endDate: activeProjectData!.endDate,
        progressOfProject: newTotalRelativeProgress,
      });
      setActiveOnEditMilestoneData((prev) => {
        return {
          ...prev,
          progress: newProgress,
          relativeProgress: newRelativeProgress,
          goalAchievingProbability: newGoalAchievingProbability,
        };
      });
    } catch (e) {
      alert(e);
    }
  };
  const handleDurationUpdate = async (newDuration: number) => {
    try {
      await updateMilestoneIdentical({
        pathRef: activeMilestonePath,
        duration: newDuration,
      });

      const newEndDate = createTimeStamp.fromMillis(
        startDate.toMillis() + newDuration * 86400000
      );

      await updateMilestoneDate({
        pathRef: activeMilestonePath,
        id: id,
        newStartDate: startDate,
        newEndDate,
        sizeOfMilestones: activeProjectMilestonesData
          ? activeProjectMilestonesData.length
          : 0,
        oldEndDate: endDate,
        oldStartDate: startDate,
        currentProgress: progress,
      })
        .then(async ({ endDate, totalRelativeProgress }) => {
          const newTotalDuration = totalDurations + newDuration - duration;
          const newRelativeProgress = calculateMilestoneRelativeProgress(
            progress,
            newDuration,
            newTotalDuration
          );
          // const newGoalAchievingProbability = calculateGoalAchievingProbability(progress,)
          // await updateMilestoneIdentical({
          //   pathRef: activeMilestonePath,
          //   progress: progress,
          //   relativeProgress: newRelativeProgress,
          //   goalAchievingProbability:
          // });

          await updateProjectDate({
            pathRef: activeProjectPath,
            endDate: endDate,
            startDate: activeProjectData!.startDate,
            progressOfProject: totalRelativeProgress,
          });

          const progressOfTime = calculateProgressOfTime(startDate, newEndDate);
          const goalAchievingProbability = calculateGoalAchievingProbability(
            progress,
            progressOfTime
          );
          return {
            duration: newDuration,
            endDate,
            relativeProgress: newRelativeProgress,
            goalAchievingProbability,
            progressOfTime: progressOfTime,
          };
        })
        .then(
          ({
            duration,
            relativeProgress,
            endDate,
            goalAchievingProbability,
            progressOfTime,
          }) => {
            // console.log("relativeProgress", relativeProgress);
            setActiveOnEditMilestoneData((prev) => {
              return {
                ...prev,
                duration,
                endDate,
                relativeProgress,
                goalAchievingProbability,
                progressOfTime,
              };
            });
          }
        );
    } catch (e) {
      console.log(e);
    }
  };
  return (
    <>
      <div
        id="date-container "
        style={{
          display: "flex",
          flexDirection: "column",
          justifyContent: "space-around",
        }}
      >
        <StartDateForm
          handleStartDateSubmit={handleStartDateSubmit}
          initialStartDate={startDate.toDate()}
        />
        <EndDateForm
          handleEndDateSubmit={handleEndDateSubmit}
          minDate={createTimeStamp
            .fromMillis(startDate.toMillis() + 86400000)
            .toDate()}
          initialEndDate={endDate.toDate()}
        />
      </div>

      <div
        style={{
          display: "flex",
          flexDirection: "column",
          justifyContent: "space-around",
        }}
      >
        <DurationForm
          handleDurationSubmit={handleDurationUpdate}
          initialDuration={duration}
        />
        <ProgressForm
          progress={progress}
          handleProgressSubmit={handleProgressUpdate}
        />
      </div>
    </>
  );
};
