import React from "react";
import { Timestamp } from "../firebase/firebase";
import { getLinearProgressBarPositions } from "../hooks/useProjectPositioning";
import { LoadedLinearProgressBar } from "./TopPanel";
import { calculateDecimalToPercentage } from "../utulities/calculateDecimalToPercentage";
import { CircledProgressBar } from "./CircleProgressContent";
import { MilestoneEditBody, MilestoneEditStatusContainer } from "./ConstantUi";
import { MilestoneEditForms } from "./MilestoneEditForms";
import { SmallText, YellowTextColor, MediumText } from "./Text";

interface MilestoneEditComponentBodyProps {
  startDate: Timestamp;
  id: string;
  activeMilestonePath: string;
  endDate: Timestamp;
  progress: number;
  relativeProgress: number;
  progressOfTime: number;
  goalAchievingProbability: number;
  duration: number;
}

export const MilestoneEditComponentBody: React.FC<MilestoneEditComponentBodyProps> =
  ({
    duration,
    startDate,
    progress,
    endDate,
    activeMilestonePath,
    id,
    relativeProgress,
    goalAchievingProbability,
    progressOfTime,
  }) => {
    const linearBarData = getLinearProgressBarPositions(
      false,
      startDate,
      endDate,
      progress,
      progressOfTime
    );
    // useEffect(() => {
    //   console.log("its changed1 ", endDate.toDate(), startDate.toDate());
    // }, [endDate, startDate]);
    return (
      <MilestoneEditBody>
        <MilestoneEditStatusContainer id="milestone-edit-status-container">
          <CircledProgressBar
            progressValue={goalAchievingProbability}
            isMediumSize
            isSmallSize
          />

          <div
            style={{
              width: "60%",
              padding: "5px",
              marginTop: "5px",
              marginLeft: "5px",
            }}
          >
            <LoadedLinearProgressBar
              startDate={startDate}
              endDate={endDate}
              progressOfProject={Math.round(
                calculateDecimalToPercentage(progress)
              )}
              isMediumSize
              plannedEndDate={null}
              {...linearBarData}
            />
          </div>
        </MilestoneEditStatusContainer>
        <div
          style={{
            display: "flex",
            flexDirection: "row",
            height: "60%",
            width: "100%",
            justifyContent: "space-around",
          }}
        >
          <MilestoneEditForms
            id={id}
            activeMilestonePath={activeMilestonePath}
            progress={progress}
            duration={duration}
            startDate={startDate}
            endDate={endDate}
            relativeProgress={relativeProgress}
          />
          <div
            style={{
              display: "flex",
              flexDirection: "column",
              height: "85%",
              justifyContent: "flex-end",
            }}
          >
            <div style={{ display: "flex", flexDirection: "column" }}>
              <SmallText color={YellowTextColor}>Relative Progress</SmallText>
              <MediumText>
                {Math.round(calculateDecimalToPercentage(relativeProgress))}%
              </MediumText>
            </div>
          </div>
        </div>
        {/*TODO
          <div
            style={{ display: "flex", width: "100%", flexDirection: "column" }}
          >
            {/* <LinearProgressBar
              diffStartDateandPlannedEndDate={diffPlannedEndDateAndStartDate}
              endDate={endDate}
              isTopLevel={false}
              grayLength={grayLength}
              isExpired={isExpired}
              plannedEndDate={""}
              progressOfProject={progressOfProject}
              startDate={startDate}
              widthLength={widthLength}
              yellowLineLength={yellowLineLength}
              isMediumSize
            /> */}
        {/* </div> */}
      </MilestoneEditBody>
    );
  };
