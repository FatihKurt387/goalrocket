import React from "react";
import { ProjectDetailsContainer } from "./ConstantUi";

interface ProjectDetailScreenContentContainerProps {
  isMilestoneEditing: boolean;
}
export const ProjectDetailScreenContentContainer: React.FC<ProjectDetailScreenContentContainerProps> =
  ({ isMilestoneEditing, children }) => {
    return (
      <>
        <ProjectDetailsContainer
          id="Project-Details-Container"
          isEditing={isMilestoneEditing}
        >
          {children}
        </ProjectDetailsContainer>
      </>
    );
  };
