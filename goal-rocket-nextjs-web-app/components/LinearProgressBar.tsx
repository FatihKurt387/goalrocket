import React from "react";
import SmallPointer from "../public/smallpointer.svg";
import BigPointer from "../public/bigpointer.svg";
import { Tooltip } from "@material-ui/core";
import {
  MainContainerTop,
  SmallPointerIcon,
  BigPointerIcon,
  PlannedAndDatePointer,
  MainContainerBody,
  MainContainerGrayArea,
  FirstLine,
  SecondLine,
  MainContainerBottom,
} from "./ConstantUi";

export interface LinearProgressBarProps {
  isSmallSize?: boolean;
  isMediumSize?: boolean;
  isShowMilestones?: boolean;
  isShowDatePointers?: boolean;
  isExpired: boolean;
  widthLength: number;
  grayLength: number;
  yellowLineLength: number;
  whiteLineLength: number;
  isTopLevel?: boolean;
  startDate: string;
  endDate: string;
  plannedEndDate: string | null;
  progressOfProjectValue?: number;
  progressOfProjectPosition?: number;
  progressOfTimePositioning?: number;
  diffStartDateandPlannedEndDate: number;
}
const LinearProgressBar: React.FC<LinearProgressBarProps> = ({
  isMediumSize,
  isSmallSize,
  progressOfProjectPosition,
  progressOfProjectValue,
  isTopLevel,
  isExpired,
  isShowMilestones,
  isShowDatePointers,
  startDate,
  plannedEndDate,
  endDate,
  widthLength,
  yellowLineLength,
  diffStartDateandPlannedEndDate,
  grayLength,
  whiteLineLength,
  children,
}) => {
  return (
    <>
      <MainContainerTop id="linear-progress-top">
        {isShowDatePointers && (
          <>
            <Tooltip title={"Start Date " + startDate}>
              <SmallPointerIcon
                id="startdate"
                position={-1}
                src={SmallPointer}
              />
            </Tooltip>
            <Tooltip title={"End Date " + endDate}>
              <SmallPointerIcon
                src={SmallPointer}
                position={isExpired ? widthLength - 1.5 : widthLength - 0.8}
                id="enddate"
              />
            </Tooltip>
            {isTopLevel && plannedEndDate && (
              <Tooltip title={"Planned End Date " + plannedEndDate}>
                <PlannedAndDatePointer
                  src={SmallPointer}
                  position={diffStartDateandPlannedEndDate}
                  id="plannedenddate"
                ></PlannedAndDatePointer>
              </Tooltip>
            )}
          </>
        )}
        <Tooltip
          id="progress-tooltip"
          title={`Progress ${Math.round(progressOfProjectValue ?? 0)}%`}
        >
          <BigPointerIcon
            isSmall={isSmallSize}
            isMedium={isMediumSize}
            id="progressofproject"
            position={
              Math.round(progressOfProjectPosition ?? 0) === 100
                ? 97.5
                : isMediumSize
                ? Math.round(progressOfProjectPosition ?? 0) - 2.5
                : Math.round(progressOfProjectPosition ?? 0) - 1.5
            }
            src={BigPointer}
          />
        </Tooltip>
      </MainContainerTop>
      <Tooltip title="100%">
        <MainContainerBody
          id="linear-progress-body"
          isMedium={isMediumSize}
          isSmall={isSmallSize}
        >
          <MainContainerGrayArea
            width={isExpired ? grayLength + 1 : grayLength}
          >
            <Tooltip title={`${Math.round(whiteLineLength)}%`}>
              <FirstLine width={whiteLineLength} />
            </Tooltip>
            <Tooltip title={`${Math.round(yellowLineLength)}%`}>
              <SecondLine width={yellowLineLength} />
            </Tooltip>
          </MainContainerGrayArea>
        </MainContainerBody>
      </Tooltip>
      {isShowMilestones && children ? (
        <MainContainerBottom id="linear-progress-bar-bottom">
          {children}
        </MainContainerBottom>
      ) : null}
    </>
  );
};

export default LinearProgressBar;
