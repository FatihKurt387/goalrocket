import { FieldProps, getIn } from "formik";
import React from "react";
import { TextField, TextFieldProps } from "@material-ui/core";
type TextFormFieldProps = FieldProps & TextFieldProps;

export const TextFormField: React.FC<TextFormFieldProps> = ({
  field,
  form,
  ...props
}) => {
  const errorText =
    getIn(form.touched, field.name) && getIn(form.errors, field.name);

  return (
    <TextField
      helperText={errorText}
      error={!!errorText}
      {...field}
      {...props}
    />
  );
};
