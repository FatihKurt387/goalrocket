import styled from "styled-components";
import { MilestoneAddButtonIcon, MilestoneDeleteIcon } from "./ConstantUi";
import AddIcon from "../public/addicon.svg";
import DeleteIcon from "../public/delete.svg";
import AddSubProjectScreen from "./AddSubProjectScreen";
import React, { useCallback } from "react";
import { useMilestoneContext } from "../context/MilestonesContext";
import { usePathTrackingContext } from "../context/PathTrackingContext";
import { IconButtonComponent } from "./IconButton";
import { useAppStateContext } from "../context/AppStateContext";
import { Timestamp } from "../firebase/firebase";
import { useProjectContext } from "../context/ProjectsContext";

export interface AddOrDeletePopUpInterface {
  open: boolean;
  hasProjects: boolean;
  milestoneid: string;
  projectid: number;
  startDate: Timestamp;
  endDate: Timestamp;
  progress: number;
}
const AddOrDeletePopUpContainer = styled.div<{ opened: boolean }>`
  display: flex;
  max-width: 107px;
  max-height: 66px;
  border: 3px solid rgba(240, 240, 255, 0.1);
  box-sizing: border-box;
  width: 100%;
  position: absolute;
  flex-direction: row;
  background: rgba(50, 50, 77, 0.95);
  height: ${(p) => (p.opened ? "100%" : "0")};
  border: 3px solid rgba(240, 240, 255, 0.1);
  opacity: ${(p) => (p.opened ? "1" : "0")};
  visibility: ${(p) => (p.opened ? "visible" : "hidden")};
  justify-content: space-around;
  justify-items: center;
  align-items: center;
  transition: opacity 225ms cubic-bezier(0.4, 0, 0.2, 1);
  bottom: ${(p) => (p.opened ? "-70px" : "0")};
  right: 0;
  z-index: 9999;
`;

const AddOrDeletePopUp: React.FC<AddOrDeletePopUpInterface> = ({
  open,
  hasProjects,
  milestoneid,
  projectid,
  startDate,
  endDate,
  progress,
}) => {
  const { deleteMilestone, updateMilestoneDate } = useMilestoneContext();
  const { updateProjectDate } = useProjectContext();
  const {
    setIsCreatingSubProjectScreenOpened,
    isCreatingSubProjectScreenOpened,
    activeProjectData,
  } = useAppStateContext();
  const closeAddSubProjectScreen = () => {
    setIsCreatingSubProjectScreenOpened(false);
  };
  const { specificCurrentPath } = usePathTrackingContext();
  const activePath = specificCurrentPath.substring(1);
  const activeMilestonePath = activePath + "/milestones/" + milestoneid;
  const handleDeleteMilestone = useCallback(async () => {
    try {
      const isDeleted = await deleteMilestone(
        specificCurrentPath + "/milestones",
        hasProjects,
        milestoneid
      );
      if (!isDeleted) return;

      await updateMilestoneDate({
        pathRef: activeMilestonePath,
        id: milestoneid,
        newEndDate: startDate,
        newStartDate: startDate,
        oldEndDate: endDate,
        oldStartDate: startDate,
        sizeOfMilestones: 2,
        currentProgress: progress,
      }).then(async ({ endDate, totalRelativeProgress }) => {
        await updateProjectDate({
          pathRef: activePath,
          startDate: activeProjectData!.startDate,
          endDate: endDate,
          progressOfProject: totalRelativeProgress,
        });
      });
    } catch (e) {
      console.log(e);
    }
  }, [
    activeMilestonePath,
    activePath,
    activeProjectData,
    deleteMilestone,
    endDate,
    hasProjects,
    milestoneid,
    progress,
    specificCurrentPath,
    startDate,
    updateMilestoneDate,
    updateProjectDate,
  ]);

  return (
    <AddOrDeletePopUpContainer
      opened={!isCreatingSubProjectScreenOpened && open}
      onClick={(e) => {
        e.stopPropagation();
      }}
    >
      <IconButtonComponent
        children={<MilestoneDeleteIcon src={DeleteIcon} />}
        onClick={handleDeleteMilestone}
      />
      <IconButtonComponent
        children={<MilestoneAddButtonIcon small src={AddIcon} />}
        size="small"
        onClick={() => setIsCreatingSubProjectScreenOpened(true)}
      />

      <AddSubProjectScreen
        opened={isCreatingSubProjectScreenOpened}
        projectid={projectid}
        milestoneid={milestoneid}
        closeCreateScreen={closeAddSubProjectScreen}
      />
    </AddOrDeletePopUpContainer>
  );
};

export default AddOrDeletePopUp;
