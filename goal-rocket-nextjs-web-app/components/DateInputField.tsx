import { Field } from "formik";
import React from "react";
import { DatePickerFormProps } from "../constant/TextFieldsProps";
import { DatePickerComponent } from "./DatePicker";

interface DateInputProps {
  handleAccept: (e?: React.FormEvent<HTMLFormElement> | undefined) => void;
  name: string;
  id: string;
  label: string;
  minDate?: Date;
  maxDate?: Date;
  isShowTodayButton?: boolean;
  isJustReadOnly?: boolean;
  onChange: {
    (e: React.ChangeEvent<any>): void;
    <T = string | React.ChangeEvent<any>>(
      field: T
    ): T extends React.ChangeEvent<any>
      ? void
      : (e: string | React.ChangeEvent<any>) => void;
  };
}
export const DateInputField: React.FC<DateInputProps> = ({
  handleAccept,
  name,
  id,
  label,
  minDate,
  maxDate,
  onChange,
  isShowTodayButton,
  isJustReadOnly,
}) => {
  return (
    <Field
      readOnly={isJustReadOnly}
      onChange={onChange}
      onAccept={handleAccept}
      name={name}
      label={label}
      id={id}
      minDate={minDate}
      maxDate={maxDate}
      showTodayButton={isShowTodayButton}
      {...DatePickerFormProps(isJustReadOnly)}
      component={DatePickerComponent}
    />
  );
};
