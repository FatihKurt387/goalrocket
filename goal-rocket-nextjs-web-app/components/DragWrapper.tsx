import { truncate } from "lodash";
import React, { FC } from "react";
import { Draggable } from "react-beautiful-dnd";
import { MilestoneType } from "../types/milestoneType";
import { MilestonesWrapper } from "./ConstantUi";
import { DragWrapperLeftSide } from "./DragWrapperLeftSide";
import { DragWrapperDate } from "./DragWrapperDate";
import { DragWrapperEndSide } from "./DragWrapperEndSide";
import { DragWrapperStatus } from "./DragWrapperStatus";
import { calculateDecimalToPercentage } from "../utulities/calculateDecimalToPercentage";

interface WrapperComponentI {
  milestone: MilestoneType;
  isMilestoneEditing: boolean;
  onClick: (x: any) => void;
  index: number;
}
export const DragWrapper: FC<WrapperComponentI> = ({
  onClick,
  milestone,
  index,
  isMilestoneEditing,
}) => {
  const {
    id,
    goal,
    hasProjects,
    startDate,
    endDate,
    duration,
    progress,
    goalAchievingProbability,
    progressOfTime,
  } = milestone;
  const [onMouse, setOnMouse] = React.useState<boolean>(false);
  const onMouseOverWrapper = () => {
    setOnMouse(true);
  };
  const onMouseLeaveWrapper = () => {
    setOnMouse(false);
  };
  return (
    <React.Fragment>
      <Draggable key={id} index={index} draggableId={id}>
        {(provided) => (
          <>
            <MilestonesWrapper
              onMouseEnter={onMouseOverWrapper}
              onMouseLeave={onMouseLeaveWrapper}
              isMilestoneEditing={isMilestoneEditing}
              mouseOver={onMouse}
              id={`${"milestonewrapper" + id}`}
              onClick={() => {
                onClick(milestone);
              }}
              ref={provided.innerRef}
              {...provided.draggableProps}
              {...provided.dragHandleProps}
              key={id}
            >
              <DragWrapperLeftSide
                isMilestoneEditing={isMilestoneEditing}
                goal={
                  goal
                    ? truncate(goal, {
                        length: isMilestoneEditing ? 10 : 20,
                      })
                    : truncate("Type Milestone goal here", {
                        length: isMilestoneEditing ? 10 : 20,
                      })
                }
              />
              <DragWrapperDate
                isEditTabOpened={isMilestoneEditing}
                startDate={startDate.toDate().toLocaleDateString("en-US")}
                endDate={endDate.toDate().toLocaleDateString("en-US")}
                duration={duration}
              />
              {!isMilestoneEditing && (
                <>
                  <DragWrapperStatus
                    goalAchievingProbability={calculateDecimalToPercentage(
                      goalAchievingProbability
                    )}
                    startDate={startDate}
                    endDate={endDate}
                    progressOfProject={progress}
                    progressOfTime={progressOfTime}
                  />
                  <DragWrapperEndSide
                    id={id}
                    hasProjects={hasProjects}
                    startDate={startDate}
                    endDate={endDate}
                    progress={progress}
                  />
                </>
              )}
            </MilestonesWrapper>
          </>
        )}
      </Draggable>
    </React.Fragment>
  );
};
