import { Tooltip } from "@material-ui/core";
import React, { useCallback } from "react";
import MilestonePointer from "../public/milestonepointer.svg";
import { CircledProgressBar } from "./CircleProgressContent";
import {
  TopHeader,
  TopHeaderLeft,
  TopHeaderRight,
  TopHeaderTitleBox,
  ProjectNameInputContainer,
  MilestonesIcon,
  TopHeaderStatusBarContainer,
  UnderLine,
} from "./ConstantUi";
import { SubmitButton } from "./FormikFormWithTextField";
import LinearProgressBar from "./LinearProgressBar";
import { useAppStateContext } from "../context/AppStateContext";
import { useProjectContext } from "../context/ProjectsContext";
import { useSubProjectContext } from "../context/SubProjectContext";
import { Field, Form, Formik } from "formik";
import { getValidationSchema } from "../utulities/getValidationFormSchema";
import { MediumText, YellowTextColor } from "./Text";
import { TextFormField } from "./TextFormField";
import { EditGoalTextFieldProps } from "../constant/TextFieldsProps";
import { projectGoalPlaceholder } from "../constant/ContextStrings";
import {
  getLinearProgressBarPositions,
  PositionDataType,
} from "../hooks/useProjectPositioning";
import { MilestoneType } from "../types/milestoneType";
import { Timestamp } from "../firebase/firebase";
import { calculateDecimalToPercentage } from "../utulities/calculateDecimalToPercentage";

interface TopPanelProps {
  activePath: string;
  goal: string;
  goalAchievingProbability: number;
  isTopLevel: boolean;
  progressOfProject: number;
  progressOfTime: number;
  milestones: MilestoneType[] | null | undefined;
  startDate: Timestamp;
  endDate: Timestamp;
  plannedEndDate?: Timestamp | null;
}
interface LoadedProjectGoalComponentProps {
  goal: string;
  activePath: string;
  updateProjectIdentical: ({
    goal,
    pathRef,
    description,
  }: {
    goal?: string | undefined;
    pathRef: string;
    description?: string | undefined;
  }) => Promise<false | undefined>;
  updateSubProjectIdentical: ({
    goal,
    pathRef,
    description,
  }: {
    goal?: string | undefined;
    pathRef: string;
    description?: string | undefined;
  }) => Promise<boolean>;
}

const LoadedProjectGoalComponent: React.FC<LoadedProjectGoalComponentProps> = ({
  goal,
  activePath,
  updateProjectIdentical,
  updateSubProjectIdentical,
}) => {
  const { activeProjectData } = useAppStateContext();

  const LoadedEditProjectGoalForm = useCallback(() => {
    return (
      <>
        <Formik
          validationSchema={getValidationSchema(goal ?? "", "goal", 75)}
          initialValues={{ goal: goal ?? "" }}
          onSubmit={async ({ goal }) => {
            if (activeProjectData?.isTopLevel) {
              const obj = { pathRef: activePath, goal };
              return await updateProjectIdentical(obj);
            } else {
              const obj = { pathRef: activePath, goal };
              return await updateSubProjectIdentical(obj);
            }
          }}
        >
          {(formik) => {
            return (
              <>
                <Form autoComplete="off">
                  <Field
                    component={TextFormField}
                    name="goal"
                    {...EditGoalTextFieldProps}
                    placeholder={projectGoalPlaceholder}
                  />
                  <UnderLine />
                  <SubmitButton
                    type="submit"
                    onClick={() => formik.handleSubmit()}
                  />
                </Form>
              </>
            );
          }}
        </Formik>
      </>
    );
  }, [
    activePath,
    activeProjectData?.isTopLevel,
    goal,
    updateProjectIdentical,
    updateSubProjectIdentical,
  ]);
  return <LoadedEditProjectGoalForm />;
};

export const LoadedLinearProgressBar: React.FC<
  PositionDataType & {
    startDate: Timestamp;
    endDate: Timestamp;
    plannedEndDate?: Timestamp | null;
    progressOfProject?: number;
    isSmallSize?: boolean;
    isMediumSize?: boolean;
    isShowDatePointers?: boolean;
    isShowMilestones?: boolean;
  }
> = ({
  diffPlannedEndDateAndStartDate,
  grayLength,
  isExpired,
  whiteLineLength,
  widthLength,
  yellowLineLength,
  isTopLevel,
  milestonesPositions,
  progressOfProjectPosition,
  progressOfTimePositioning,
  endDate,
  plannedEndDate,
  startDate,
  progressOfProject,
  isShowDatePointers,
  isShowMilestones,
  isSmallSize,
  isMediumSize,
}) => {
  const LoadedLinearProgressBarComponent = useCallback(() => {
    return (
      <LinearProgressBar
        diffStartDateandPlannedEndDate={diffPlannedEndDateAndStartDate}
        endDate={endDate.toDate().toLocaleDateString("en-US") ?? ""}
        grayLength={grayLength}
        isExpired={isExpired}
        isTopLevel={isTopLevel}
        plannedEndDate={
          plannedEndDate?.toDate().toLocaleDateString("en-US") ?? ""
        }
        progressOfProjectValue={progressOfProject}
        progressOfProjectPosition={progressOfProjectPosition}
        progressOfTimePositioning={progressOfTimePositioning}
        startDate={startDate.toDate().toLocaleDateString("en-US") ?? ""}
        widthLength={widthLength}
        yellowLineLength={yellowLineLength}
        isMediumSize={isMediumSize}
        isSmallSize={isSmallSize}
        whiteLineLength={whiteLineLength}
        isShowDatePointers={isShowDatePointers}
        isShowMilestones={isShowMilestones}
      >
        {milestonesPositions?.map(({ position, goalName }, index) => {
          return (
            <Tooltip
              key={index}
              title={`${goalName ? goalName : "Milestone " + index}`}
            >
              <MilestonesIcon
                position={position > 99 ? position - 2 : position}
                src={MilestonePointer}
              />
            </Tooltip>
          );
        })}
      </LinearProgressBar>
    );
  }, [
    diffPlannedEndDateAndStartDate,
    endDate,
    grayLength,
    isExpired,
    isMediumSize,
    isShowDatePointers,
    isShowMilestones,
    isSmallSize,
    isTopLevel,
    milestonesPositions,
    plannedEndDate,
    progressOfProject,
    progressOfProjectPosition,
    progressOfTimePositioning,
    startDate,
    whiteLineLength,
    widthLength,
    yellowLineLength,
  ]);
  return <LoadedLinearProgressBarComponent />;
};
export const TopPanel: React.FC<TopPanelProps> = ({
  goalAchievingProbability,
  goal,
  activePath,
  endDate,
  isTopLevel,
  milestones,
  progressOfProject,
  progressOfTime,
  startDate,
  plannedEndDate,
}) => {
  const { updateProjectIdentical } = useProjectContext();
  const { updateSubProjectIdentical } = useSubProjectContext();
  const linearBarData = getLinearProgressBarPositions(
    isTopLevel,
    startDate,
    endDate,
    progressOfProject,
    progressOfTime,
    plannedEndDate,
    milestones
  );

  return (
    <TopHeader id="TopHeader">
      <TopHeaderLeft>
        <CircledProgressBar
          isShowNumber
          isMediumSize
          progressValue={goalAchievingProbability}
        />
      </TopHeaderLeft>
      <TopHeaderRight>
        <TopHeaderTitleBox>
          <ProjectNameInputContainer style={{ marginTop: "5px" }}>
            <MediumText color={YellowTextColor}>Project Goal</MediumText>
            <LoadedProjectGoalComponent
              goal={goal}
              updateProjectIdentical={updateProjectIdentical}
              updateSubProjectIdentical={updateSubProjectIdentical}
              activePath={activePath}
            />
          </ProjectNameInputContainer>
        </TopHeaderTitleBox>
        <TopHeaderStatusBarContainer>
          <LoadedLinearProgressBar
            startDate={startDate}
            endDate={endDate}
            progressOfProject={Math.round(
              calculateDecimalToPercentage(progressOfProject)
            )}
            isShowDatePointers
            isShowMilestones
            plannedEndDate={plannedEndDate}
            {...linearBarData}
          />
        </TopHeaderStatusBarContainer>
      </TopHeaderRight>
    </TopHeader>
  );
};
