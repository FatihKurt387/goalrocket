import React, { useCallback } from "react";
import { chain } from "lodash";

// import { SubProjectOverViewType } from "../context/SubProjectContext";
// import { MilestoneOverViewType } from "../context/MilestonesContext";
import {
  ProjectContentBox,
  SubProjectContainer,
  SubProjectGroup,
  SubSubProjectBox,
  SubSubProjectItem,
} from "./ConstantUi";
import FrameIcon from "../public/treesquare.svg";
import { MilestoneType } from "../types/milestoneType";
import { SubProjectType } from "../types/subProjectType";
import { getIdFromPath } from "../utulities/getIdFromPath";
import { usePathTrackingContext } from "../context/PathTrackingContext";
import { useAppStateContext } from "../context/AppStateContext";

interface TreeViewComponentProps {
  subProjects?: SubProjectType[];
  milestones?: MilestoneType[];
}

export const TreeViewComponent: React.FC<TreeViewComponentProps> = ({
  subProjects,
  milestones,
}) => {
  const { specificCurrentPath } = usePathTrackingContext();
  const { activeProjectMilestonesData } = useAppStateContext();
  const activePath = specificCurrentPath.substring(1);
  const matchAndRenderMilestoneAndItsSubproject = useCallback(() => {
    let result: JSX.Element[] = [];
    if (subProjects) {
      console.log("subprojects", subProjects);
      const subProjectsGroupByLevel = chain(subProjects)
        .groupBy("level")
        .map((data) => data)
        .value();

      subProjectsGroupByLevel.map((groups, index) => {
        const body = (
          <>
            <SubProjectContainer
              level={index}
              style={{
                maxWidth: index === 0 ? 100 + "%" : 25 + "%",
              }}
              id="main-project-container"
              key={index}
            >
              {groups.map((subproject) => {
                return (
                  <>
                    <SubSubProjectBox style={{ marginLeft: "5px" }} level={"0"}>
                      <SubProjectGroup style={{ marginLeft: "5px" }}>
                        {subproject.id === getIdFromPath(activePath)
                          ? activeProjectMilestonesData?.map(
                              (_Element, index) => {
                                return (
                                  <SubSubProjectItem
                                    key={index}
                                    src={FrameIcon}
                                    style={{ marginLeft: "5px" }}
                                  />
                                );
                              }
                            )
                          : milestones
                              ?.filter(
                                (milestone) =>
                                  milestone.parent === subproject?.id
                              )
                              .map((_Element, index) => {
                                return (
                                  <SubSubProjectItem
                                    key={index}
                                    src={FrameIcon}
                                    style={{ marginLeft: "5px" }}
                                  />
                                );
                              })}
                      </SubProjectGroup>
                    </SubSubProjectBox>
                  </>
                );
              })}
            </SubProjectContainer>
          </>
        );

        return result.push(body);
      });
      return result;
    }
    return null;
  }, [milestones, subProjects]);
  return matchAndRenderMilestoneAndItsSubproject ? (
    <ProjectContentBox id="content-box">
      {matchAndRenderMilestoneAndItsSubproject()?.map((Element) => {
        return Element;
      })}
    </ProjectContentBox>
  ) : null;
};
