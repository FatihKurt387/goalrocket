import { Box, CircularProgress } from "@material-ui/core";
import React from "react";
import { TopHeaderLeftBıgProgressIcon } from "./ConstantUi";
import BıgProgressIcon from "../public/bigprogressicon.svg";

export interface CircledProgressBarProps {
  isSmallShowNumber?: boolean;
  isSmallSize?: boolean;
  isMediumSize?: boolean;
  isShowNumber?: boolean;
  progressValue: number;
}

export const CircledProgressBar: React.FC<CircledProgressBarProps> = ({
  isSmallSize,
  isMediumSize,
  progressValue,
  isShowNumber,
  isSmallShowNumber,
}) => {
  return (
    <Box position="relative" display="inline-flex" visibility={"visible"}>
      <CircularProgress
        style={{
          height: isSmallSize ? (isMediumSize ? "70px" : "48px") : "188px",
          width: isSmallSize ? (isMediumSize ? "70px" : "48px") : "188px",
          color: "yellow",
          zIndex: 999,
        }}
        thickness={5}
        variant="determinate"
        value={progressValue >= 100 ? 100 : progressValue}
      />
      <Box
        style={{
          height: isSmallSize ? (isMediumSize ? "70px" : "48px") : "188px",
          width: isSmallSize ? (isMediumSize ? "70px" : "48px") : "188px",
        }}
        top={0}
        left={isSmallSize ? -1.5 : 0}
        bottom={isSmallSize ? -3 : 0}
        right={0}
        position="absolute"
        display="flex"
        alignItems="center"
        justifyContent="center"
      >
        <TopHeaderLeftBıgProgressIcon
          small={isSmallSize}
          medium={isMediumSize}
          src={BıgProgressIcon}
        />
        <div
          style={{
            position: "absolute",
            textAlign: "center",
            height: "80%",
            width: "80%",
            minHeight: "80%",
            minWidth: "80%",
            justifyContent: "center",
            alignItems: "center",
            backgroundColor:
              isSmallSize || isMediumSize ? "#373752" : "#32324D",
            borderRadius: isSmallSize || isMediumSize ? "80%" : "50%",
            display: "inline-flex",
            color: "rgba(240, 240, 255, 1)",
          }}
        >
          {isShowNumber && (
            <div
              style={{
                display: "flex",
                fontFamily: "aileron",
                fontSize: isSmallShowNumber ? "25px" : "50px",
                fontStyle: "normal",
                fontWeight: 400,
                lineHeight: "86px",
                letterSpacing: "0em",
                textAlign: "left",
              }}
            >
              {progressValue > 100
                ? ">100"
                : progressValue === 0
                ? " "
                : progressValue}
              <div
                style={{
                  display: "inline-flex",
                  fontFamily: "aileron",
                  fontSize: isSmallShowNumber ? "10px" : "36px",
                  fontStyle: "normal",
                  fontWeight: 400,
                  lineHeight: "43px",
                  letterSpacing: "0em",
                  textAlign: "left",
                  alignItems: "center",
                }}
              >
                {progressValue === 0 ? " " : "%"}
              </div>
            </div>
          )}
        </div>
      </Box>
    </Box>
  );
};
