import React from "react";
import {
  DatePicker,
  DatePickerProps,
  MuiPickersUtilsProvider,
} from "@material-ui/pickers";
import CalendarIcon from "../public/calendar.svg";
import { CalendarIconComp } from "./ConstantUi";
import { FieldProps, getIn } from "formik";
// import { CleanTypeOfData } from "../../functions/cleaningData"
import DateFnsUtils from "@date-io/date-fns";

type DatePickerFormFieldWithFormik = FieldProps & DatePickerProps;
export const DatePickerComponent: React.FC<DatePickerFormFieldWithFormik> = ({
  field,
  form,
  ...props
}) => {
  const errorText =
    getIn(form.touched, field.name) && getIn(form.errors, field.name);
  return (
    <MuiPickersUtilsProvider utils={DateFnsUtils}>
      <label style={{ position: "relative", display: "flex" }}>
        <DatePicker
          {...props}
          {...field}
          helperText={errorText}
          error={!!errorText}
          allowKeyboardControl={false}
          onChange={(val: any) => {
            form.setFieldValue(field.name, val);
          }}
        />
        {!props.readOnly && (
          <CalendarIconComp
            style={{ left: "-15px", position: "relative", cursor: "pointer" }}
            src={CalendarIcon}
          />
        )}
      </label>
    </MuiPickersUtilsProvider>
  );
};

// {
/* <KeyboardDatePicker
        margin={isMilestoneEditing ? "none" : "normal"}
        id="date-picker-dialog"
        label={isMilestoneEditing ? "" : "Planning Date"}
        format="MM/dd/yyyy"
        value=""
        onChange={(e) => {
          console.log("e", e);
        }}
        // minDate={
        //   props.ismilestoneedit
        //     ? CannotBeEqual.setDate(CannotBeEqual.getDate() + 1)
        //     : ActiveStartDate
        // }
        // value={ActivePlannedDate}
        allowKeyboardControl={false}
        InputLabelProps={{
          style: {
            color: "rgba(228, 220, 0, 1)",
            fontSize: "18px",
            fontStyle: "normal",
            fontWeight: "bold",
            lineHeight: "25px",
            letterSpacing: "0em",
            textAlign: "left",
          },
        }}
        keyboardIcon={<CalendarIconComp src={CalendarIcon}></CalendarIconComp>}
        InputProps={{
          readOnly: true,
          style: {
            borderBottom: "2px solid white",
            color: isMilestoneEditing
              ? "rgba(240, 240, 255, 1)"
              : "rgba(240, 240, 255, 0.7)",
            fontSize: isMilestoneEditing ? "18px" : "auto",
            fontStyle: isMilestoneEditing ? "normal" : "auto",
            lineHeight: isMilestoneEditing ? "22px" : "auto",
            letterSpacing: isMilestoneEditing ? "0em" : "auto",
            fontWeight: "-moz-initial",
          },
        }}
        // DialogProps={{
        //   onFocus: () => {
        //     setOnFocus(true);
        //   },
        //   onBlur: () => {
        //     setOnFocus(false);
        //   },
        // }}
        // onChange={handlePlanningEndDateChange}
        KeyboardButtonProps={{
          "aria-label": "change date",
        }}
      /> */
// }
