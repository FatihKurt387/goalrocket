import { TextField, TextFieldProps } from "@material-ui/core";
import React from "react";
import styled from "styled-components";
import { getIn, FormikProps } from "formik";
import { FormValues } from "../utulities/getValidationFormSchema";

export const SubmitButton = styled.button`
  display: none;
  width: 0;
  opacity: 0;
  height: 0;
  cursor: none;
  z-index: -9999;
`;

interface FormikFormI {
  initialProperty: string;
  id: string;
  name: string;
  formStyle?: React.CSSProperties;
  textFieldProps?: TextFieldProps;
  formik: FormikProps<FormValues>;
}
export const FormikFormWithTextField: React.FC<FormikFormI> = ({
  id,
  name,
  initialProperty,
  formStyle,
  textFieldProps,
  formik,
}) => {
  const errorText =
    getIn(formik.touched, initialProperty) &&
    getIn(formik.errors, initialProperty);
  return (
    <form autoComplete="off" id={name} style={formStyle}>
      <TextField
        id={id}
        {...textFieldProps}
        placeholder={`Type your ${name} here...`}
        value={getIn(formik.values, initialProperty)}
        onChange={formik.handleChange}
        helperText={errorText}
        error={!!errorText}
      />
    </form>
  );
};
