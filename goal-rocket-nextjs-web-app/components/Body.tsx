import React from "react";
import { BodyContainer } from "./ConstantUi";
// import Loading from "./ui/LoadingUI"
// import ErrorBox from "./ui/ErrorBox"
export interface BodyComponentProps {}

const BodyComponent: React.FC<BodyComponentProps> = (props) => {
  return (
    <BodyContainer id="body">
      {/* <Loading></Loading>
      {error && <ErrorBox error={error}></ErrorBox>} */}
      {props.children}
    </BodyContainer>
  );
};

export default BodyComponent;
