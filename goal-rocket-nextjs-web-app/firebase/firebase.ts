import firebase from "firebase";
const firebaseConfig = {
  apiKey: "AIzaSyDV1LrNSYtSUHyTw1te1WcMQv3Nhxcyfa0",
  authDomain: "goalrocket-1b4b2.firebaseapp.com",
  databaseURL: "https://goalrocket-1b4b2-default-rtdb.firebaseio.com",
  projectId: "goalrocket-1b4b2",
  storageBucket: "goalrocket-1b4b2.appspot.com",
  messagingSenderId: "660694150005",
  appId: "1:660694150005:web:7367fa230ffda3506ac13c",
  measurementId: "G-3P5ZEVSP48",
};

export type Timestamp = firebase.firestore.Timestamp;
const firebaseApp = !firebase.apps.length
  ? firebase.initializeApp(firebaseConfig)
  : firebase.app(); // if already initialized, use that one

const firestore = firebaseApp.firestore();
const createTimeStamp = firebase.firestore.Timestamp;
export { firestore, createTimeStamp };
