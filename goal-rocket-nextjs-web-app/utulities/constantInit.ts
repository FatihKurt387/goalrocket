import { createTimeStamp, Timestamp } from "../firebase/firebase";
import { MilestoneType } from "../types/milestoneType";
import { ProjectType } from "../types/projectType";

export const getMilestoneInitialState = (
  id: string,
  topLevelProjectId: string,
  parent: string | null = null,
  level: number = 0,
  duration: number = 7,
  goal: string = "",
  startDate: Timestamp = createTimeStamp.now(),
  endDate: Timestamp = createTimeStamp.fromMillis(
    createTimeStamp.now().toMillis() + 7 * 86400000
  )
): MilestoneType => {
  return {
    id,
    description: "",
    goal,
    topLevelProjectId: topLevelProjectId,
    startDate,
    endDate,
    progressOfTime: 0,
    progress: 0,
    relativeProgress: 0,
    goalAchievingProbability: 0,
    hasProjects: false,
    duration,
    parent,
    level,
  };
};

export const getProjectInitialState = (
  id: string,
  initialMilestonesId: string | null,
  level: number = 0,
  collectionPaths: string[] | null = [
    `projects/${id}/milestones/${initialMilestonesId}`,
  ],
  parent: string | null = null,
  topLevelProjectId: string | null = null,
  timeDifference: number = 7,
  typeofproject: string = "S",
  isTopLevel: boolean = true,
  goal: string = "",
  startDate: Timestamp = createTimeStamp.now(),
  plannedEndDate: Timestamp = createTimeStamp.fromMillis(
    createTimeStamp.now().toMillis() + 7 * 86400000
  ),
  endDate: Timestamp = createTimeStamp.fromMillis(
    createTimeStamp.now().toMillis() + 7 * 86400000
  ),
  progressOfTime: number = 0,
  progressOfProject: number = 0,
  goalAchievingProbability: number = 0
): ProjectType => {
  return {
    id,
    timeDifference,
    user: "",
    typeofproject,
    isTopLevel,
    startDate,
    plannedEndDate,
    endDate,
    goal,
    description: "",
    progressOfTime,
    progressOfProject,
    goalAchievingProbability,
    collectionPaths,
    level,
    topLevelProjectId,
    parent,
  };
};
