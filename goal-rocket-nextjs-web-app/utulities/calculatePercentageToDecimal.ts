export const calculatePercentageToDecimal = (percentageNumber: number) => {
  return percentageNumber / 100;
};
