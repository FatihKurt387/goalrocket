import * as yup from "yup";

export interface FormValues {
  [x: string]: any;
}
export const getValidationSchemaForNumeric = (
  number: number | undefined,
  initialProperty: string,
  max: number,
  min: number
) => {
  const validationSchema = yup.object({
    [initialProperty]: yup
      .number()
      .required("Cannot be less than 1 character")
      .notOneOf([number], `Value cannot be equal to initial value : ${number}`)
      .max(max, `Cannot be more than ${max} chutulitieter`)
      .min(min, `Cannot be more than ${min} character`),
  });
  return validationSchema;
};
export const getValidationSchema = (
  string: string | undefined,
  initialProperty: string,
  maxLength: number
) => {
  const validationSchema = yup.object({
    [initialProperty]: yup
      .string()
      .required("Cannot be less than 1 character")
      .notOneOf([string], `Value cannot be equal to initial value : ${string}`)
      .max(maxLength, `Cannot be more than ${maxLength} character`),
  });
  return validationSchema;
};
// const useFormikForm = (
//   initialProperty: string,
//   initialValue: string,
//   maxLength: number
// ) => {
//   let obj = { [initialProperty]: initialValue };
//   const formik = useFormik({
//     initialValues: obj,
//     validationSchema: getValidationSchema(
//       initialValue,
//       initialProperty,
//       maxLength
//     ),

//     onSubmit: (values) => {},
//   });
//   return { formik };
// };
// const useNumericFormikForm = (
//   initialProperty: string,
//   initialValue: number,
//   max: number,
//   min: number
// ) => {
//   let obj = { [initialProperty]: initialValue };
//   const formik = useFormik({
//     initialValues: obj,
//     validationSchema: getValidationSchemaForNumeric(
//       initialValue,
//       initialProperty,
//       max,
//       min
//     ),

//     onSubmit: (values) => {
//       alert(JSON.stringify(values, null, 2));
//     },
//   });
//   return { formik };
// };
// export { useFormikForm, useNumericFormikForm };
