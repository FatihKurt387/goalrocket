import { createTimeStamp, Timestamp } from "../firebase/firebase";
import { getCalculatedDateDifferance } from "./getDateDifferences";

export const calculateProgressOfTime = (
  currentStartDate: Timestamp,
  currentEndDate: Timestamp
) => {
  const today = createTimeStamp.now().toDate().setHours(0, 0, 0, 0);
  const startDate = currentStartDate.toDate().setHours(0, 0, 0, 0);
  const endDate = currentEndDate.toDate().setHours(0, 0, 0, 0);
  const startDateAndEndDateDifferences = getCalculatedDateDifferance(
    new Date(currentEndDate.toDate().setHours(0, 0, 0, 0)),
    new Date(currentStartDate.toDate().setHours(0, 0, 0, 0))
  );
  let calculatedProgressOfTime = 0.0;
  if (today < startDate) {
    calculatedProgressOfTime = 0.0;
  } else if (today > endDate) {
    calculatedProgressOfTime = 1.0;
  } else {
    const todayAndStartDateDifferences = getCalculatedDateDifferance(
      new Date(today),
      new Date(currentStartDate.toDate().setHours(0, 0, 0, 0))
    );
    if (startDateAndEndDateDifferences === 0) {
      calculatedProgressOfTime = todayAndStartDateDifferences;
    } else {
      calculatedProgressOfTime =
        todayAndStartDateDifferences / startDateAndEndDateDifferences;
    }
  }
  return calculatedProgressOfTime;
};
