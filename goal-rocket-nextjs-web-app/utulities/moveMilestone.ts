import { MilestoneType } from "../types/milestoneType";

export function move(arr: any[], from: number, to: number) {
  return arr.reduce(
    (prev: MilestoneType[], current: MilestoneType, idx, self) => {
      if (from === to) {
        prev.splice(from, 0, current);
      }
      if (idx === from) {
        return prev;
      }
      if (from < to) {
        prev.push(current);
      }
      if (idx === to) {
        prev.push(self[from]);
      }
      if (from > to) {
        prev.push(current);
      }
      return prev;
    },
    []
  );
}
