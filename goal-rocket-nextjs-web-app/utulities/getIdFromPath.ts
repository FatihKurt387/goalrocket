export const getIdFromPath = (path: string) => {
  const id = path.split("/")[path.split("/").length - 1];
  return id;
};
