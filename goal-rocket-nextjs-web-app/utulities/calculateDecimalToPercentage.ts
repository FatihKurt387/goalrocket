export const calculateDecimalToPercentage = (decimalNumber: number) => {
  return decimalNumber * 100;
};
