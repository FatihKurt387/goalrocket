export const calculateGoalAchievingProbability = (
  progressNumber: number,
  progressOfTime: number
) => {
  let goalAchievingProbabiliy = progressNumber / progressOfTime;
  if (goalAchievingProbabiliy === Infinity) {
    return 0;
  }
  if (Number.isNaN(goalAchievingProbabiliy)) {
    return 0;
  }
  return goalAchievingProbabiliy;
};
