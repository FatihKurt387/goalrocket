export const getCalculatedDateDifferance = (date1: Date, date2: Date) => {
  const duration = (date1.getTime() - date2.getTime()) / (1000 * 3600 * 24); //
  return duration;
};
