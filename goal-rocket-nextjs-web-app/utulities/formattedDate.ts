export const formattedDate = (stringDate: Date) => {
  const date = stringDate;
  const newDate =
    (date.getMonth() > 8 ? date.getMonth() + 1 : "0" + (date.getMonth() + 1)) +
    "/" +
    (date.getDate() > 9 ? date?.getDate() : "0" + date?.getDate()) +
    "/" +
    date.getFullYear();
  return new Date(newDate);
};
