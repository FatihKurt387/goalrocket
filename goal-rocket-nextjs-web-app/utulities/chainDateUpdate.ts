import {
  IdenticalUpdateArgTypes,
  UpdateMilestoneArgType,
} from "../context/MilestonesContext";
import { Timestamp } from "../firebase/firebase";
import { ProjectType } from "../types/projectType";
import { calculateGoalAchievingProbability } from "./calculateGoalAchievingProbability";
import { calculateProgressOfTime } from "./calculateProgressOfTime";
import { getCalculatedDateDifferance } from "./getDateDifferences";

interface ChainStartDateUpdateI {
  newStartDateForMilestone: Timestamp;
  newEndDateForMilestone: Timestamp;
  oldEndDate: Timestamp;
  oldStartDate: Timestamp;
  progress: number;
  id: string;
  sizeOfMilestones: number;
  activeMilestonePath: string;
  isThatFirstMilestoneOfProject: boolean;
  updateMilestoneDate: ({
    pathRef,
    id,
    newEndDate,
    newStartDate,
    oldStartDate,
    oldEndDate,
    currentProgress,
    sizeOfMilestones,
  }: UpdateMilestoneArgType) => Promise<{
    startDate: Timestamp;
    endDate: Timestamp;
    totalRelativeProgress: number;
  }>;
  updateMilestoneIdentical: ({
    goal,
    pathRef,
    description,
    hasProjects,
    duration,
    progress,
    relativeProgress,
    goalAchievingProbability,
  }: IdenticalUpdateArgTypes) => Promise<boolean | undefined>;
  activeProjectData: ProjectType | null;
  updateProjectDate: ({
    pathRef,
    startDate,
    endDate,
    progressOfProject,
  }: {
    pathRef: string;
    startDate: Timestamp;
    endDate: Timestamp;
    progressOfProject: number;
  }) => Promise<void>;
}

export const chainStartDateUpdate = async ({
  newStartDateForMilestone,
  newEndDateForMilestone,
  oldEndDate,
  oldStartDate,
  progress,
  id,
  sizeOfMilestones,
  activeMilestonePath,
  isThatFirstMilestoneOfProject,
  updateMilestoneDate,
  updateMilestoneIdentical,
  activeProjectData,
  updateProjectDate,
}: ChainStartDateUpdateI) => {
  try {
    const newDuration = getCalculatedDateDifferance(
      newEndDateForMilestone.toDate(),
      newStartDateForMilestone.toDate()
    );
    const progressOfTime = calculateProgressOfTime(
      newStartDateForMilestone,
      newEndDateForMilestone
    );
    const goalAchievingProbability = calculateGoalAchievingProbability(
      progress,
      progressOfTime
    );
    await updateMilestoneDate({
      pathRef: activeMilestonePath,
      id: id,
      newStartDate: newStartDateForMilestone,
      sizeOfMilestones,
      newEndDate: newEndDateForMilestone,
      oldEndDate,
      oldStartDate,
      currentProgress: progress,
    }).then(async ({ endDate, totalRelativeProgress }) => {
      const activeProjectPath = activeMilestonePath
        .split("/")
        .slice(0, activeMilestonePath.split("/").length - 2)
        .join("/");
      await updateMilestoneIdentical({
        pathRef: activeMilestonePath,
        duration: newDuration,
      });
      await updateProjectDate({
        pathRef: activeProjectPath,
        endDate: endDate,
        startDate: isThatFirstMilestoneOfProject
          ? newStartDateForMilestone
          : activeProjectData!.startDate,
        progressOfProject: totalRelativeProgress,
      });
    });

    return {
      endDate: newEndDateForMilestone,
      startDate: newStartDateForMilestone,
      duration: newDuration,
      goalAchievingProbability: goalAchievingProbability,
      progressOfTime: progressOfTime,
    };
  } catch (e) {
    console.log(oldEndDate.seconds, oldStartDate.seconds);
  }
};
