import { Timestamp } from "../firebase/firebase";

export type ProjectType = {
  goal: string;
  goalAchievingProbability: number;
  id: string;
  isTopLevel: boolean;
  collectionPaths?: string[] | null;
  description: string;
  user: string;
  typeofproject: string;
  startDate: Timestamp;
  plannedEndDate: Timestamp | null;
  endDate: Timestamp;
  timeDifference: number;
  progressOfTime: number;
  progressOfProject: number;
  level: number;
  topLevelProjectId: string | null;
  parent: string | null;
};

export type ProjectIdenticalType = {
  goal: string;
  goalAchievingProbability: number;
  id: string;
  isTopLevel: boolean;
  collectionPaths?: string[] | null;
  description: string;
  level: number;
  duration: number;
};
