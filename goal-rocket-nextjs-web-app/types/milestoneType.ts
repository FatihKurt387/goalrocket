import { Timestamp } from "../firebase/firebase";

export type MilestoneIdenticalType = {
  goalAchievingProbability: number;
  id: string;
  level: number;
  parent: string;
  topLevelProjectId: string;
  hasProjects: boolean;
};
export type MilestoneType = {
  goalAchievingProbability: number;
  id: string;
  level: number;
  parent: string | null;
  topLevelProjectId: string;
  hasProjects: boolean;
  progress: number;
  progressOfTime: number;
  relativeProgress: number;
  startDate: Timestamp;
  endDate: Timestamp;
  description: string;
  goal: string;
  duration: number;
};
