export type SubProjectTypeIdentical = {
  goal: string;
  goalAchievingProbability: number;
  id: string;
  isTopLevel: boolean;
  parent: string;
  level: number;
  topLevelProjectId: string;
};
export type SubProjectType = {
  goal: string;
  goalAchievingProbability: number;
  id: string;
  isTopLevel: boolean;
  parent: string;
  level: number;
  topLevelProjectId: string;
  user: string;
  typeofproject: string;
  startDate: string;
  dueDate: string;
  description: string;
  progressOfTime: number;
  progressOfProject: number;
};
