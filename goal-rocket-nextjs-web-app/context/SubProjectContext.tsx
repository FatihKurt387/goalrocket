import React, {
  createContext,
  useCallback,
  useContext,
  useEffect,
  useState,
} from "react";
import { firestore } from "../firebase/firebase";
import { ProjectIdenticalType } from "../types/projectType";
import {
  SubProjectType,
  SubProjectTypeIdentical,
} from "../types/subProjectType";
import {
  getMilestoneInitialState,
  getProjectInitialState,
} from "../utulities/constantInit";
import { decidePathLevel } from "../utulities/decitePathLevel";

interface SubProjectProps {}
export type SubProjectOverViewType = SubProjectTypeIdentical[] | undefined;

interface SubProjectContextI {
  allSubProjectsOverViewData: SubProjectType[] | null;
  updateSubProjectIdentical: ({
    goal,
    pathRef,
    description,
  }: {
    goal?: string | undefined;
    pathRef: string;
    description?: string | undefined;
  }) => Promise<boolean>;
  loadAllSubProjectsOverview: () => Promise<{
    data: SubProjectType[];
  }>;
  useGetSubProject: (
    subProjectParentPath: string,
    subProjectId: string
  ) => Promise<SubProjectType>;
  isAllSubProjectsOverviewLoading: boolean;
  createSubProject: (
    subProjectRefPath: string,
    subProjectsCredential: ProjectIdenticalType[]
  ) => Promise<boolean>;
  deleteSubProject: (id: string) => Promise<boolean>;
}

const SubProjectContext = createContext<SubProjectContextI | null>(null);

export const useSubProjectContext = () => {
  const context = useContext(SubProjectContext);
  if (!context) {
    throw new Error(
      "useSubProjectContext must be used within the useSubProjectContext.Provider"
    );
  }
  return context;
};
export const SubProjects: React.FC<SubProjectProps> = ({ children }) => {
  const [isAllSubProjectsOverviewLoading, setIsAllSubProjectsOverviewLoading] =
    useState<boolean>(true);
  const [allSubProjectsOverViewData, setAllSubProjectsOverview] = useState<
    SubProjectType[] | null
  >(null);

  const loadAllSubProjectsOverview = useCallback(async () => {
    setIsAllSubProjectsOverviewLoading(true);
    const subProjectRef = firestore
      .collectionGroup("projects")
      .orderBy("level", "asc");
    const getQueries = await subProjectRef.get();
    const docs = getQueries.docs.map((doc) => doc.data());
    const data = docs as SubProjectType[];
    setAllSubProjectsOverview(data);
    setIsAllSubProjectsOverviewLoading(false);
    return { data };
  }, []);
  const useGetSubProject = useCallback(
    async (subProjectParentPath: string, subProjectId: string) => {
      const subProjectRef = firestore
        .collection(subProjectParentPath)
        .doc(subProjectId);
      const doc = await subProjectRef.get();
      const data = doc.data() as SubProjectType;
      return data;
    },
    []
  );

  const createSubProject = useCallback(
    async (
      subProjectRefPath: string,
      subProjectsCredential: ProjectIdenticalType[]
    ) => {
      try {
        const batch = firestore.batch();
        const levelOfSubProject = decidePathLevel(
          subProjectRefPath.substring(1)
        );
        const projectRef = firestore.collection(subProjectRefPath).doc();
        const initialProjectState = getProjectInitialState(
          projectRef.id,
          null,
          levelOfSubProject,
          null,
          subProjectRefPath.substring(1).split("/")[
            subProjectRefPath.substring(1).split("/").length - 2
          ],
          subProjectRefPath.substring(1).split("/")[1],
          21,
          "S",
          false,
          ""
        );
        batch.set(projectRef, initialProjectState);
        subProjectsCredential.forEach((subProject) => {
          const milestonRef = firestore
            .collection(`${subProjectRefPath}/${projectRef.id}/milestones`)
            .doc();
          const initialMilestoneState = getMilestoneInitialState(
            milestonRef.id,
            subProjectRefPath.substring(1).split("/")[1],
            projectRef.id,
            levelOfSubProject,
            subProject.duration,
            subProject.goal
          );

          batch.set(milestonRef, initialMilestoneState);
        });

        await batch.commit();

        return true;
      } catch (e) {
        return false;
      }
    },
    []
  );
  useEffect(() => {
    loadAllSubProjectsOverview();
  }, [loadAllSubProjectsOverview]);
  const updateSubProjectIdentical = useCallback(
    async ({
      goal,
      pathRef,
      description,
    }: {
      goal?: string;
      pathRef: string;
      description?: string;
    }) => {
      const id = pathRef.split("/")[pathRef.split("/").length - 1];
      const parentPath = pathRef
        .split("/")
        .slice(0, pathRef.split("/").length - 1)
        .join("/");
      const subProjectRef = firestore.collection(parentPath).doc(id);
      if (goal && description) {
        subProjectRef.update({ goal: goal, description: description });
      } else if (goal) {
        subProjectRef.update({ goal: goal });
      } else if (description) {
        subProjectRef.update({ description: description });
      } else {
        return false;
      }

      return true;
    },
    []
  );

  const deleteSubProject = useCallback(async (id: string) => {
    const batch = firestore.batch();
    const actualSubProjectRef = await firestore
      .collectionGroup("projects")
      .limit(1)
      .where("id", "==", id)
      .get();
    const milestonesRef = await firestore
      .collectionGroup("milestones")
      .where("parent", "==", id)
      .get();
    milestonesRef.forEach((milestone) => {
      batch.delete(milestone.ref);
    });

    actualSubProjectRef.forEach((subProject) => {
      batch.delete(subProject.ref);
    });

    try {
      await batch.commit();
    } catch (e) {
      console.log("e", e);
    }
    return true;
  }, []);
  return (
    <SubProjectContext.Provider
      value={{
        loadAllSubProjectsOverview,
        updateSubProjectIdentical,
        isAllSubProjectsOverviewLoading,
        allSubProjectsOverViewData,
        createSubProject,
        useGetSubProject,
        deleteSubProject,
      }}
    >
      {children}
    </SubProjectContext.Provider>
  );
};
