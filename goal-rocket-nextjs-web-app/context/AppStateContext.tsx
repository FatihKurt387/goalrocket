import React, {
  createContext,
  Dispatch,
  SetStateAction,
  useContext,
  useState,
} from "react";
import { createTimeStamp } from "../firebase/firebase";
import { PositionDataType } from "../hooks/useProjectPositioning";
import { TreeViewDataTypes } from "../hooks/useRelatedOverview";
import { MilestoneType } from "../types/milestoneType";
import { ProjectType } from "../types/projectType";
import { SubProjectType } from "../types/subProjectType";
import { SubProjectOverViewType } from "./SubProjectContext";

interface AppStateProps {}
interface AppStateContextI {
  isTreeView: boolean;
  setIsTreeView: Dispatch<SetStateAction<boolean>>;
  isMilestoneEditing: boolean;
  isMilestoneTab: boolean;
  isProjectTab: boolean;
  setIsMilestoneEditing: Dispatch<SetStateAction<boolean>>;
  setIsMilestoneTab: Dispatch<SetStateAction<boolean>>;
  setIsProjectTab: Dispatch<SetStateAction<boolean>>;
  activeOnEditMilestoneData: MilestoneType & { path: string };
  setActiveOnEditMilestoneData: Dispatch<
    React.SetStateAction<MilestoneType & { path: string }>
  >;
  activeProjectData: ProjectType | null;
  setActiveProjectData: Dispatch<React.SetStateAction<ProjectType | null>>;
  setActiveProjectMilestonesData: Dispatch<
    React.SetStateAction<MilestoneType[] | null>
  >;
  activeProjectMilestonesData: MilestoneType[] | null;
  setLinearProgressBarPositionsData: Dispatch<
    React.SetStateAction<PositionDataType | null>
  >;
  linearProgressBarPositionsData: PositionDataType | null;
  isCreatingSubProjectScreenOpened: boolean;
  setIsCreatingSubProjectScreenOpened: React.Dispatch<
    React.SetStateAction<boolean>
  >;
  activeProjectSubProjectsData: SubProjectType[] | null;
  setActiveProjectSubProjectData: React.Dispatch<
    React.SetStateAction<SubProjectType[] | null>
  >;
  setProjectTreeViewData: React.Dispatch<
    React.SetStateAction<TreeViewDataTypes[] | undefined>
  >;
  projectTreeViewData?: TreeViewDataTypes[];
}

const AppStateContext = createContext<AppStateContextI | null>(null);

export const useAppStateContext = () => {
  const context = useContext(AppStateContext);
  if (!context) {
    throw new Error(
      "useMilestoneProject must be used within the useMilestoneContext.Provider"
    );
  }
  return context;
};
export const AppState: React.FC<AppStateProps> = ({ children }) => {
  const [isTreeView, setIsTreeView] = useState(false);
  const [
    isCreatingSubProjectScreenOpened,
    setIsCreatingSubProjectScreenOpened,
  ] = useState(false);

  const [activeProjectData, setActiveProjectData] =
    useState<ProjectType | null>(null);

  const [activeProjectSubProjectsData, setActiveProjectSubProjectData] =
    useState<SubProjectType[] | null>(null);
  const [activeProjectMilestonesData, setActiveProjectMilestonesData] =
    useState<MilestoneType[] | null>(null);

  const [activeOnEditMilestoneData, setActiveOnEditMilestoneData] = useState<
    MilestoneType & { path: string }
  >({
    id: "",
    goal: "",
    duration: 0,
    startDate: createTimeStamp.now(),
    endDate: createTimeStamp.now(),
    progress: 0,
    relativeProgress: 0,
    description: "",
    goalAchievingProbability: 0,
    hasProjects: false,
    level: 0,
    parent: "",
    progressOfTime: 0,
    topLevelProjectId: "",
    path: "",
  });
  const [linearProgressBarPositionsData, setLinearProgressBarPositionsData] =
    useState<PositionDataType | null>(null);
  const [projectTreeViewData, setProjectTreeViewData] = useState<
    TreeViewDataTypes[] | undefined
  >();
  const [isMilestoneEditing, setIsMilestoneEditing] = useState(false);
  const [isMilestoneTab, setIsMilestoneTab] = useState(false);
  const [isProjectTab, setIsProjectTab] = useState(true);
  // const checkIsMilestoneEditing = useCallback(() => {
  //   if (isMilestoneEditing) return;
  //   setActiveOnEditMilestoneData({
  //     id: "",
  //     goal: "",
  //     duration: 0,
  //     startDate: "",
  //     endDate: "",
  //     progress: 0,
  //     description: "",
  //     relativeProgress: 0,
  //   });
  // }, [isMilestoneEditing]);
  // useEffect(() => {
  //   checkIsMilestoneEditing();
  // }, [checkIsMilestoneEditing]);

  return (
    <AppStateContext.Provider
      value={{
        isTreeView,
        isMilestoneEditing,
        isMilestoneTab,
        isProjectTab,
        activeOnEditMilestoneData,
        setIsMilestoneTab,
        setIsProjectTab,
        setIsTreeView,
        setIsMilestoneEditing,
        setActiveOnEditMilestoneData,
        setActiveProjectData,
        activeProjectData,
        setLinearProgressBarPositionsData,
        linearProgressBarPositionsData,
        setActiveProjectMilestonesData,
        activeProjectMilestonesData,
        isCreatingSubProjectScreenOpened,
        setIsCreatingSubProjectScreenOpened,
        activeProjectSubProjectsData,
        setActiveProjectSubProjectData,
        setProjectTreeViewData,
        projectTreeViewData,
      }}
    >
      {children}
    </AppStateContext.Provider>
  );
};
