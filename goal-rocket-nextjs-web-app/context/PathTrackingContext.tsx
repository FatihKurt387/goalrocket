import React, { useState, createContext, useContext, useEffect } from "react";
// import { useLocation } from "react-router";
import { useRouter } from "next/router";
interface PathTrackingContextProps {}
interface CreatePathTrackingContextI {
  specificCurrentPath: string;
  setSpecificCurrentPath: React.Dispatch<React.SetStateAction<string>>;
}
const PathTrackingContext = createContext<CreatePathTrackingContextI | null>(
  null
);

export const usePathTrackingContext = () => {
  const context = useContext(PathTrackingContext);
  if (!context) {
    throw new Error(
      "use must be used within the usePathTrackingContext.Provider"
    );
  }
  return context;
};
export const PathTracking: React.FC<PathTrackingContextProps> = ({
  children,
}) => {
  const [specificCurrentPath, setSpecificCurrentPath] = useState<string>("");

  const location = useRouter().asPath;
  const history = useRouter();
  useEffect(() => {
    setSpecificCurrentPath(location);
  }, [location]);
  // useEffect(() => {
  //   const reload = history.events.on("routeChangeComplete", () =>
  //     history.reload(window.location.pathname)
  //   );
  //   return () => {
  //     history.events.off("routeChangeComplete", () =>
  //       history.reload(window.location.pathname)
  //     );
  //   };
  // }, [history]);
  return (
    <PathTrackingContext.Provider
      value={{ setSpecificCurrentPath, specificCurrentPath }}
    >
      {children}
    </PathTrackingContext.Provider>
  );
};
