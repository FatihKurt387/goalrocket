import React, {
  createContext,
  useCallback,
  useContext,
  useEffect,
  useState,
} from "react";

import { Timestamp, firestore, createTimeStamp } from "../firebase/firebase";
import { applyAtomicUpdate } from "../hooks/useAtomicUpdate";
import { MilestoneIdenticalType, MilestoneType } from "../types/milestoneType";
import { calculateGoalAchievingProbability } from "../utulities/calculateGoalAchievingProbability";
import { calculateProgressOfTime } from "../utulities/calculateProgressOfTime";
import { getMilestoneInitialState } from "../utulities/constantInit";
import { decidePathLevel } from "../utulities/decitePathLevel";
import { getCalculatedDateDifferance } from "../utulities/getDateDifferences";
import { getIdFromPath } from "../utulities/getIdFromPath";
import { getTopLevelProjectIdFromPath } from "../utulities/getTopLevelProjectIdFromPath";

interface MilestonesProjectProps {}
export type MilestoneOverViewType = MilestoneIdenticalType[] | undefined;
export type MilestoneTypeForOnEditTab = {
  id: string;
  goal: string;
  duration: number;
  startDate: Timestamp;
  endDate: Timestamp;
  progress: number;
  relativeProgress: number;
  description: string;
  path: string;
};

export type UpdateMilestoneArgType = {
  pathRef: string;
  id: string;
  sizeOfMilestones: number;
  newEndDate: Timestamp;
  newStartDate: Timestamp;
  oldStartDate: Timestamp;
  oldEndDate: Timestamp;
  currentProgress: number;
};

export type IdenticalUpdateArgTypes = {
  goal?: string;
  pathRef: string;
  description?: string;
  hasProjects?: boolean;
  duration?: number;
  progress?: number;
  relativeProgress?: number;
  goalAchievingProbability?: number;
};

interface MilestonesProjectContextI {
  updateMilestoneIdentical: ({
    goal,
    pathRef,
    description,
    hasProjects,
    duration,
    progress,
    relativeProgress,
    goalAchievingProbability,
  }: IdenticalUpdateArgTypes) => Promise<boolean | undefined>;
  loadAllMilestonesOverview: () => Promise<{
    data: MilestoneType[];
  }>;
  isAllMilestonesOverviewLoading: boolean;
  allMilestonesOverViewData: MilestoneType[] | null;
  createMilestone: (
    parentPathRef: string,
    lastOneEndDate: Timestamp
  ) => Promise<MilestoneType>;
  getMilestones: (parentPath: string) => Promise<{
    milestoneData: MilestoneType[] | null;
  }>;
  deleteMilestone: (
    milestoneParentPathRef: string,
    hasProjects: boolean,
    milestoneId: string
  ) => Promise<boolean>;
  updateMilestoneDate: ({
    pathRef,
    id,
    newEndDate,
    newStartDate,
    oldStartDate,
    oldEndDate,
    currentProgress,
    sizeOfMilestones,
  }: UpdateMilestoneArgType) => Promise<{
    startDate: Timestamp;
    endDate: Timestamp;
    totalRelativeProgress: number;
  }>;
  calculateMilestoneRelativeProgress: (
    currentProgress: number,
    currentDuration: number,
    totalDuration: number
  ) => number;
}
const MilestonesContext = createContext<MilestonesProjectContextI | null>(null);

export const useMilestoneContext = () => {
  const context = useContext(MilestonesContext);
  if (!context) {
    throw new Error(
      "useMilestoneProject must be used within the useMilestoneContext.Provider"
    );
  }
  return context;
};
export const Milestones: React.FC<MilestonesProjectProps> = ({ children }) => {
  const [isAllMilestonesOverviewLoading, setIsAllMilestonesOverviewLoading] =
    useState(true);

  const [allMilestonesOverViewData, setAllMilestoneOverviewData] = useState<
    MilestoneType[] | null
  >(null);

  const loadAllMilestonesOverview = useCallback(async () => {
    const milestonesRef = firestore.collectionGroup("milestones").get();
    const docs = (await milestonesRef).docs.map((doc) => {
      return doc.data();
    });
    const data = docs as MilestoneType[];
    setAllMilestoneOverviewData(data);
    setIsAllMilestonesOverviewLoading(false);
    return { data };
  }, []);
  useEffect(() => {
    loadAllMilestonesOverview();
  }, [loadAllMilestonesOverview]);
  const createMilestone = useCallback(
    async (parentPathRef: string, lastOneEndDate: Timestamp) => {
      const toplevelId = parentPathRef.split("/")[1];
      const parent =
        parentPathRef.split("/")[parentPathRef.split("/").length - 2];
      const batch = firestore.batch();
      const milestoneRef = firestore.collection(parentPathRef).doc();
      const level = decidePathLevel(parentPathRef);
      const startDate = lastOneEndDate;
      const endDate = createTimeStamp.fromMillis(
        startDate.toMillis() + 7 * 86400000
      );

      const initialMilestoneState = getMilestoneInitialState(
        milestoneRef.id,
        toplevelId,
        parent,
        level,
        7,
        "",
        startDate,
        endDate
      );
      batch.set(milestoneRef, initialMilestoneState);

      await batch.commit();
      return initialMilestoneState;
    },
    []
  );
  const deleteMilestone = useCallback(
    async (
      milestoneParentPathRef: string,
      hasProjects: boolean,
      milestoneId: string
    ) => {
      const atTheSameLevelHowManyMilestoneInProject = await firestore
        .collection(milestoneParentPathRef)
        .get();
      const length = atTheSameLevelHowManyMilestoneInProject.size;
      // ToDo make validation tag for these check.
      if (length === 1) return false;
      // is there subproject Under Milestone ?
      const actualMilestoneRef = firestore
        .collection(milestoneParentPathRef)
        .doc(milestoneId);
      const subProjectsRef = await firestore
        .collection(milestoneParentPathRef + "/" + milestoneId + "/projects")
        .get();
      if (hasProjects) {
        subProjectsRef.forEach(async (subProject) => {
          const milestonesRef = await firestore
            .collectionGroup("milestones")
            .where("parent", "==", subProject.id)
            .get();
          milestonesRef.docs.forEach((milestone) => {
            if (milestone.exists) firestore.doc(milestone.ref.path).delete();
          });
          if (subProject.exists) firestore.doc(subProject.ref.path).delete();
        });
      }
      if (actualMilestoneRef) actualMilestoneRef.delete();
      return true;
    },
    []
  );
  const getMilestones = useCallback(async (parentPath: string) => {
    const milestoneRef = await firestore.collection(parentPath).get();
    const doc = milestoneRef.docs.map((milestone) => milestone.data());
    const milestoneData = doc as MilestoneType[] | null;
    return { milestoneData };
  }, []);
  const calculateMilestoneRelativeProgress = (
    currentProgress: number,
    currentDuration: number,
    totalDuration: number
  ) => {
    const relativeProgress =
      currentProgress * (currentDuration / totalDuration);
    return relativeProgress;
  };
  const updateMilestoneDate = useCallback(
    async ({
      pathRef,
      newEndDate,
      newStartDate,
      oldStartDate,
      id,
      sizeOfMilestones,
      currentProgress,
    }: {
      pathRef: string;
      id: string;
      newEndDate: Timestamp;
      newStartDate: Timestamp;
      oldStartDate: Timestamp;
      sizeOfMilestones: number;
      currentProgress: number;
    }) => {
      const pathSplitted = pathRef.split("/");
      let totalRelativeProgress = 0;
      const parentPath = pathSplitted
        .slice(0, pathSplitted.length - 1)
        .join("/");
      const parentId = pathSplitted[pathSplitted.length - 3];
      const milestoneId = id ?? pathSplitted[pathSplitted.length - 1];
      let newStartDates: Timestamp[] = [newStartDate];
      let newEndDates: Timestamp[] = [newEndDate];

      if (sizeOfMilestones > 1) {
        // if there is more than one milestones
        const batch = firestore.batch();
        const currentMilestoneRef = firestore
          .collection(parentPath)
          .doc(milestoneId);
        const isCurrentMilestoneExist = (await currentMilestoneRef.get())
          .exists;
        const otherMilestonesRef = firestore
          .collectionGroup("milestones")
          .where("parent", "==", parentId)
          .orderBy("startDate", "asc");
        if (newStartDate && newEndDate) {
          await otherMilestonesRef.get().then((onSnapshot) => {
            let index = 0;
            let currentTotalDurations = 0;
            onSnapshot.forEach((doc) => {
              return (currentTotalDurations += doc.data().duration as number);
            });
            // console.log("total durations", currentTotalDurations);
            if (isCurrentMilestoneExist) {
              const currentDuration = getCalculatedDateDifferance(
                newEndDate.toDate(),
                newStartDate.toDate()
              );
              const newProgressOfTime = calculateProgressOfTime(
                newStartDate,
                newEndDate
              );
              const newProgress = calculateMilestoneRelativeProgress(
                currentProgress,
                currentDuration,
                currentTotalDurations
              );
              const newGoalAchievingProbability =
                calculateGoalAchievingProbability(
                  currentProgress,
                  newProgressOfTime
                );
              totalRelativeProgress += newProgress;
              batch.update(currentMilestoneRef, {
                startDate: newStartDate,
                endDate: newEndDate,
                progressOfTime: newProgressOfTime,
                relativeProgress: newProgress,
                goalAchievingProbability: newGoalAchievingProbability,
              });
            }
            // console.log("contunie");
            onSnapshot.forEach(async (doc) => {
              const docStartDate = doc.data().startDate as Timestamp;
              const docDuration = doc.data().duration as number;
              const docProgress = doc.data().progress as number;

              if (
                oldStartDate.seconds >= docStartDate.seconds ||
                doc.id === id
              ) {
                if (doc.id === id) return;
                const newRelat = calculateMilestoneRelativeProgress(
                  docProgress,
                  docDuration,
                  currentTotalDurations
                );
                totalRelativeProgress += newRelat;
                batch.update(doc.ref, {
                  relativeProgress: newRelat,
                });
                return;
              }
              // 12.09.2021 -> 12.13.2021 (after changed )
              // 12.12.2021 -> 12.13.2021 (before changed)
              // 12.13.2021 -> 12.14.2021 (b.c )

              const newStartDateForMilestone = createTimeStamp.fromMillis(
                newEndDates[index].toMillis()
              );
              const newEndDataForMilestone = createTimeStamp.fromMillis(
                newStartDateForMilestone.toMillis() + docDuration * 86400000
              );
              const newRelativeProgress = calculateMilestoneRelativeProgress(
                docProgress,
                docDuration,
                currentTotalDurations
              );
              const newProgressOfTime = calculateProgressOfTime(
                newStartDateForMilestone,
                newEndDataForMilestone
              );
              const newGoalAchievingProbability =
                calculateGoalAchievingProbability(
                  docProgress,
                  newProgressOfTime
                );
              // console.log(
              //   "loop",
              //   newStartDateForMilestone.toDate(),
              //   newEndDataForMilestone.toDate()
              // );
              totalRelativeProgress += newRelativeProgress;
              batch.update(doc.ref, {
                startDate: newStartDateForMilestone,
                endDate: newEndDataForMilestone,
                progressOfTime: newProgressOfTime,
                relativeProgress: newRelativeProgress,
                goalAchievingProbability: newGoalAchievingProbability,
              });

              newStartDates.push(newStartDateForMilestone);
              newEndDates.push(newEndDataForMilestone);
              index++;
            });
          });

          await batch.commit();
        }
      } else if (sizeOfMilestones === 1) {
        console.log("here we are again", parentPath, parent);
        const level = decidePathLevel(parentPath);
        const topLevelProjectId = getTopLevelProjectIdFromPath(parentPath);
        // if there is only one milestones
        totalRelativeProgress = currentProgress;
        const batch = firestore.batch();
        const milestoneRef = firestore.collection(parentPath).doc(milestoneId);

        const newProgressOfTime = calculateProgressOfTime(
          newStartDate,
          newEndDate
        );
        const newGoalAchievingProbability = calculateGoalAchievingProbability(
          currentProgress,
          newProgressOfTime
        );
        if (newStartDate && newEndDate) {
          batch.update(milestoneRef, {
            startDate: newStartDate,
            endDate: newEndDate,
            progressOfTime: newProgressOfTime,
            goalAchievingProbability: newGoalAchievingProbability,
          });

          await batch.commit();
          await applyAtomicUpdate({
            parent: pathSplitted[pathSplitted.length - 5],
            pathRef: pathSplitted.toString(),
            credentials: { newEndDate, newStartDate },
            level,
            topLevelProjectId,
            cb: updateMilestoneDate,
          });
        }
      }
      // console.log("aa", {
      //   startDate: newStartDates[newStartDates.length - 1],
      //   endDate: newEndDates[newStartDates.length - 1],
      // });
      return {
        startDate: newStartDates[newStartDates.length - 1],
        endDate: newEndDates[newStartDates.length - 1],
        totalRelativeProgress,
      };
    },
    []
  );
  const updateMilestoneIdentical = useCallback(
    async ({
      goal,
      pathRef,
      description,
      hasProjects,
      duration,
      progress,
      relativeProgress,
      goalAchievingProbability,
    }: {
      goal?: string;
      pathRef: string;
      description?: string;
      hasProjects?: boolean;
      duration?: number;
      progress?: number;
      relativeProgress?: number;
      goalAchievingProbability?: number;
    }) => {
      const pathSplitted = pathRef.split("/");
      const id = pathSplitted[pathSplitted.length - 1];
      const parentPath = pathRef
        .split("/")
        .slice(0, pathSplitted.length - 1)
        .join("/");
      // console.log(
      //   "milesotne update",
      //   id,
      //   parentPath,
      //   pathRef,
      //   goal,
      //   description
      // );
      const milestoneRef = firestore.collection(parentPath).doc(id);
      if (goal && description) {
        milestoneRef.update({ goal, description });
      } else if (goal) {
        milestoneRef.update({ goal });
      } else if (description) {
        milestoneRef.update({ description });
      } else if (hasProjects) {
        milestoneRef.update({ hasProjects });
      } else if (duration) {
        milestoneRef.update({ duration });
      } else if (
        progress !== undefined ||
        (progress === 0 && relativeProgress && goalAchievingProbability)
      ) {
        milestoneRef.update({
          progress,
          relativeProgress,
          goalAchievingProbability,
        });
      } else {
        return false;
      }
    },
    []
  );

  return (
    <MilestonesContext.Provider
      value={{
        updateMilestoneIdentical,
        updateMilestoneDate,
        loadAllMilestonesOverview,
        allMilestonesOverViewData,
        isAllMilestonesOverviewLoading,
        createMilestone,
        deleteMilestone,
        getMilestones,
        calculateMilestoneRelativeProgress,
      }}
    >
      {children}
    </MilestonesContext.Provider>
  );
};
