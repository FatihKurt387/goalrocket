import { createTimeStamp, firestore, Timestamp } from "../firebase/firebase";
import React, {
  createContext,
  useCallback,
  useContext,
  useEffect,
  useState,
} from "react";
import { ProjectIdenticalType, ProjectType } from "../types/projectType";
import {
  getMilestoneInitialState,
  getProjectInitialState,
} from "../utulities/constantInit";
import { calculateProgressOfTime } from "../utulities/calculateProgressOfTime";
import { calculateGoalAchievingProbability } from "../utulities/calculateGoalAchievingProbability";
import { decidePathLevel } from "../utulities/decitePathLevel";
import { getIdFromPath } from "../utulities/getIdFromPath";

interface MainProjectProps {}

export type ProjectOverviewType = ProjectIdenticalType[] | undefined;

interface ProjectContextI {
  updateProjectIdentical: ({
    goal,
    pathRef,
    description,
    plannedEndDate,
  }: {
    goal?: string;
    pathRef: string;
    plannedEndDate?: Timestamp;
    description?: string;
  }) => Promise<false | undefined>;

  loadAllProjectsOverview: () => Promise<{
    projectsOverviews: ProjectType[];
  }>;
  updateProjectDate: ({
    pathRef,
    startDate,
    endDate,
    progressOfProject,
  }: {
    pathRef: string;
    startDate: Timestamp;
    endDate: Timestamp;
    progressOfProject: number;
  }) => Promise<void>;
  isAllProjectsOverviewLoading: boolean;
  activeProject: ProjectType | null;
  allProjectsOverViewData: ProjectType[] | null;
  getProject: (projectRefPath: string) => Promise<
    | {
        projectData: ProjectType | null;
      }
    | undefined
  >;
  createProject: (
    collectionPath?: string | undefined,
    milestonesCredentials?: ProjectIdenticalType[] | undefined,
    initialStartDate?: Timestamp
  ) => Promise<string>;
  isActiveProjectLoaded: boolean;
  deleteProject: (id: string) => Promise<void>;
}

const ProjectContext = createContext<ProjectContextI | null>(null);

export const useProjectContext = () => {
  const context = useContext(ProjectContext);
  if (!context) {
    throw new Error(
      "useProjectContext must be used within the useProjectContext.Provider"
    );
  }
  return context;
};

export const Projects: React.FC<MainProjectProps> = ({ children }) => {
  const [activeProject] = useState<ProjectType | null>(null);

  const [allProjectsOverViewData, setAllProjectsOverviewData] = useState<
    ProjectType[] | null
  >(null);

  const [isActiveProjectLoaded] = useState<boolean>(false);

  const [isAllProjectsOverviewLoading, setIsAllProjectsOverviewLoading] =
    useState<boolean>(true);

  const fetchAllProjectsOverview = async () => {
    const projectsRef = firestore
      .collection("projects")
      .orderBy("startDate", "asc");
    const getQueries = await projectsRef.get();
    const data = getQueries.docs.map((doc) => doc.data()) || null;
    return data as ProjectType[];
  };
  const loadAllProjectsOverview = useCallback(async () => {
    const projectsOverviews = await fetchAllProjectsOverview();
    setAllProjectsOverviewData(projectsOverviews);
    setIsAllProjectsOverviewLoading(false);
    return { projectsOverviews };
  }, []);
  useEffect(() => {
    loadAllProjectsOverview();
  }, [loadAllProjectsOverview]);

  const getProject = useCallback(async (projectRefPath: string) => {
    const mainProjectRef = firestore
      .collection("projects")
      .doc(projectRefPath.split("/")[1]);
    const doc = await mainProjectRef.get();
    const projectData = doc.data() as ProjectType | null;
    return { projectData };
  }, []);

  const createProject = useCallback(
    async (
      collectionPath?: string,
      milestonesCredentials?: ProjectIdenticalType[],
      initialStartDate?: Timestamp
    ) => {
      const batch = firestore.batch();
      const parentPath = collectionPath
        ? collectionPath + "/projects"
        : "projects";
      const parentId = collectionPath ? getIdFromPath(collectionPath) : null; // projects/projectid/milestones/milestoneid
      const topLevelProjectId = collectionPath
        ? collectionPath.split("/")[1]
        : "";
      const levelOfProject = collectionPath ? decidePathLevel(parentPath) : 0;
      const projectRef = firestore.collection(parentPath).doc();

      if (milestonesCredentials) {
        const lastEndDate: Timestamp[] = [];
        const today = createTimeStamp.now();
        milestonesCredentials.forEach(({ duration, goal }, index) => {
          const milestoneRef = firestore
            .collection(`${parentPath}/${projectRef.id}/milestones`)
            .doc();
          const milestoneStartDate =
            index === 0 ? initialStartDate ?? today : lastEndDate[index - 1];
          const initialEndDate = createTimeStamp.fromMillis(
            milestoneStartDate.toMillis() + duration * 86400000
          );
          lastEndDate.push(initialEndDate);
          const initialMilestoneState = getMilestoneInitialState(
            milestoneRef.id,
            topLevelProjectId,
            projectRef.id,
            levelOfProject,
            duration,
            goal,
            milestoneStartDate,
            initialEndDate
          );

          if (index === milestonesCredentials.length - 1) {
            const initialProjectState = getProjectInitialState(
              projectRef.id,
              null,
              levelOfProject,
              null,
              parentId,
              topLevelProjectId,
              7,
              "S",
              false,
              "",
              initialStartDate,
              undefined,
              initialEndDate
            );
            batch.set(projectRef, initialProjectState);
          }

          batch.set(milestoneRef, initialMilestoneState);
        });
      } else {
        const milestonRef = firestore
          .collection("projects")
          .doc(projectRef.id)
          .collection("milestones")
          .doc();
        const initialProjectState = getProjectInitialState(
          projectRef.id,
          milestonRef.id,
          levelOfProject,
          null,
          parentId,
          projectRef.id
        );
        const initialMilestoneState = getMilestoneInitialState(
          milestonRef.id,
          projectRef.id,
          projectRef.id
        );
        batch
          .set(projectRef, initialProjectState)
          .set(milestonRef, initialMilestoneState);
      }
      await batch.commit();
      return projectRef.id;
    },
    []
  );

  const updateProjectIdentical = useCallback(
    async ({
      goal,
      pathRef,
      description,
      plannedEndDate,
    }: {
      goal?: string;
      pathRef: string;
      description?: string;
      plannedEndDate?: Timestamp;
      goalAchievingProbability?: number;
    }) => {
      const batch = firestore.batch();
      const id = pathRef.split("/")[1]; // projects/<:id>
      const project = firestore.collection("projects").doc(id);
      if (goal) {
        batch.update(project, { goal });
      } else if (description) {
        batch.update(project, { description });
      } else if (plannedEndDate) {
        batch.update(project, { plannedEndDate });
      } else {
        return false;
      }
      await batch.commit();
    },
    []
  );
  const updateProjectDate = async ({
    pathRef,
    startDate,
    endDate,
    progressOfProject,
  }: {
    pathRef: string;
    startDate: Timestamp;
    endDate: Timestamp;
    progressOfProject: number;
  }) => {
    const batch = firestore.batch();
    const id = getIdFromPath(pathRef);
    const parentPathRef = pathRef
      .split("/")
      .splice(0, pathRef.split("/").length - 1)
      .join("/");
    // projects/<:id>
    const project = firestore.collection(parentPathRef).doc(id);
    const newProgressOfTime = calculateProgressOfTime(startDate, endDate);
    const newGoalAchievingProbability = calculateGoalAchievingProbability(
      progressOfProject,
      newProgressOfTime
    );
    batch.update(project, {
      startDate,
      endDate,
      progressOfTime: newProgressOfTime,
      progressOfProject,
      goalAchievingProbability: newGoalAchievingProbability,
    });
    await batch.commit();
  };

  const deleteProject = useCallback(async (id: string) => {
    const batch = firestore.batch();
    const project = firestore.collection("projects").doc(id);

    const milestonesQueries = await firestore
      .collectionGroup("milestones")
      .where("topLevelProjectId", "==", id)
      .get();

    const subProjectsQueries = await firestore
      .collectionGroup("projects")
      .where("topLevelProjectId", "==", id)
      .get();

    milestonesQueries.forEach((document) => batch.delete(document.ref));
    subProjectsQueries.forEach((project) => batch.delete(project.ref));
    batch.delete(project);

    await batch.commit();
  }, []);

  return (
    <ProjectContext.Provider
      value={{
        updateProjectIdentical,
        loadAllProjectsOverview,
        isAllProjectsOverviewLoading,
        allProjectsOverViewData,
        activeProject,
        getProject,
        createProject,
        isActiveProjectLoaded,
        deleteProject,
        updateProjectDate,
      }}
    >
      {children}
    </ProjectContext.Provider>
  );
};
